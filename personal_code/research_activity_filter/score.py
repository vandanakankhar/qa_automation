from account import User_details, User_details
import requests
from body import TEST_CASES
out_filename = "research_activity.csv"
import json
import csv
import os.path

def auth_login(account):
    #QA_client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X
    #Staging_client_id=8mw9DF9xJSq9Kf6GXkQjjAb11rfOt6P7AoD3hUTR----32
    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())

    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[29:]
    # print(codes)
    # print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        QA_link
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    return AUTH_TOKEN

def score(body):
    get_auth_token(User_details)
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    url = 'url'
    req = requests.post(url,data=json.dumps(body), headers=Header)

    #print("========================\n\n\n", url, Header, req.json())
    #print("status : ", req.status_code)
    # print("Response Body : ",req)
    #pprint.pprint(json.dumps(req.json()))
    # clinicaltrials=0
    # publications=0
    # congresses=0
    # guidelines=0
    # societies=0
    list_dict_staging = {}
    list_staging = []
    inno_data = req.json().get('data',[])
    for data in inno_data :
        # clinicaltrials=0
        # publications=0
        # congresses=0
        # guidelines=0
        # societies=0
        score_data_staging = data.get('scores',[])
        #for author_data in score_data_staging:

        for score_pro in score_data_staging:
            author_id_staging = score_pro.get('author_id','')
            list_staging.append(author_id_staging)
            score_profile = score_pro.get('score_profile','')
            clinicaltrials=0
            publications=0
            congresses=0
            guidelines=0
            societies=0
            advocacy=0
            hospitals=0
            HTA=0
            regulatory_bodies=0
            for asset_cls in score_profile:
                asset_class_staging=asset_cls.get('asset_class','')
                if asset_class_staging == "clinicaltrials":
                    clinicaltrials= asset_cls.get('doc_count','')
                elif asset_class_staging == "guidelines":
                    guidelines=asset_cls.get('doc_count','')
                elif asset_class_staging == "congresses":
                    congresses=asset_cls.get('doc_count','')
                elif asset_class_staging == "publications":
                    publications=asset_cls.get('doc_count','')
                elif asset_class_staging == "societies":
                    societies = asset_cls.get('doc_count','')
                elif asset_class_staging == "pags":
                    advocacy = asset_cls.get('doc_count','')
                elif asset_class_staging == "hospitals":
                    hospitals = asset_cls.get('doc_count','')
                elif asset_class_staging == "htabodies":
                    HTA = asset_cls.get('doc_count','')
                elif asset_class_staging == "regulatorybodiessss":
                    regulatory_bodies = asset_cls.get('doc_count','')

            DOC_Count_staging=dict_staging(author_id_staging,publications,clinicaltrials,societies,congresses,guidelines,regulatory_bodies,HTA,hospitals,advocacy)
            list_dict_staging[author_id_staging] = DOC_Count_staging
    #print(json.dumps(list_dict_staging,indent = 4))


    return list_dict_staging,list_staging

def dict_staging(author_id_staging,publications,clinicaltrials,societies,congresses,guidelines,regulatory_bodies,HTA,hospitals,advocacy):

    DOC_Count={
                "publications":publications,
                "clinicaltrials":clinicaltrials,
                "societies":societies,
                "congresses":congresses,
                "guidelines":guidelines,
                "regulatory_bodies":regulatory_bodies,
                "HTA":HTA,
                "hospitals":hospitals,
                "advocacy":advocacy
            }

    return DOC_Count
def research_activity(list_staging):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    filters={"TA":"Neurology","kolType":"allkols","Indications":{"values":["Multiple Sclerosis"],"isAnd":False}}

    params = {
            'filters': json.dumps(filters),
            'group': 'org',
            'dataType': 'Public'
            }
    list_dict_activity={}
    count = 0
    for author_id in list_staging:
        count=count +1
        #import ipdb; ipdb.set_trace()
        print(count,"===========================================================================")
        url = 'url/api/v2/'+'kols/profile/'+ author_id + '/research_activity'
        res = requests.get(url,params,headers=Header)
        #.json()
        print("status : ", res.status_code)
        #import ipdb; ipdb.set_trace()
        #print(json.dumps(res))

        inno_data = res.json().get('data',[])
        #print(json.dumps(inno_data))
        #import ipdb; ipdb.set_trace()
        clinicaltrials=0
        publications=0
        congresses=0
        guidelines=0
        societies=0
        advocacy=0
        hospitals=0
        HTA=0
        regulatory_bodies=0
        for score_pro in inno_data:
            #print(json.dumps(score_pro))
            asset_class = score_pro.get('name','')
            if asset_class == "Clinical Trials":
                clinicaltrials= score_pro.get('count','')
            elif asset_class == "Guidelines":
                guidelines=score_pro.get('count','')
            elif asset_class == "Congresses":
                congresses=score_pro.get('count','')
            elif asset_class == "Publications":
                publications=score_pro.get('count','')
            elif asset_class == "Societies":
                societies = score_pro.get('count','')
            elif asset_class == "Advocacy":
                advocacy = score_pro.get('count','')
            elif asset_class == "Top Hospital KOLs":
                hospitals = score_pro.get('count','')
            elif asset_class == "HTA":
                HTA = score_pro.get('count','')
            elif asset_class == "Regulatory bodies":
                regulatory_bodies = score_pro.get('count','')

        DOC_Count_activity=dict_activity(author_id,publications,clinicaltrials,societies,congresses,guidelines,regulatory_bodies,HTA,hospitals,advocacy)
        list_dict_activity[author_id] = DOC_Count_activity
        #import ipdb; ipdb.set_trace()
        #print(json.dumps(list_dict_activity))
    return list_dict_activity
def dict_activity(author_id,publications,clinicaltrials,societies,congresses,guidelines,regulatory_bodies,HTA,hospitals,advocacy):
    DOC_activity={
                "publications":publications,
                "clinicaltrials":clinicaltrials,
                "societies":societies,
                "congresses":congresses,
                "guidelines":guidelines,
                "regulatory_bodies":regulatory_bodies,
                "HTA":HTA,
                "hospitals":hospitals,
                "advocacy":advocacy
            }

    return DOC_activity

def compare(author_id,staging_meta, production_meta):

    if staging_meta:
        publication_staging=staging_meta.get("publications")
        CT_staging=staging_meta.get("clinicaltrials")
        societies_staging=staging_meta.get("societies")
        congresses_staging=staging_meta.get("congresses")
        guidelines_staging=staging_meta.get("guidelines")
        regulatory_bodies_staging=staging_meta.get("regulatory_bodies")
        hospitals_staging=staging_meta.get("hospitals")
        HTA_staging=staging_meta.get("HTA")
        advocacy_staging=staging_meta.get("advocacy")
        #print("Score advocacy", advocacy_staging)
        #total_count_staging= publication_staging+CT_staging+societies_staging+congresses_staging+guidelines_staging+regulatory_bodies_staging+hospitals_staging+HTA_staging+advocacy_staging

    if production_meta:
        publication_producation = production_meta.get("publications")
        CT_producation = production_meta.get("clinicaltrials")
        societies_producation = production_meta.get("societies")
        congresses_producation = production_meta.get("congresses")
        guidelines_producation = production_meta.get("guidelines")
        regulatory_bodies_producation=production_meta.get("regulatory_bodies")
        hospitals_producation=production_meta.get("hospitals")
        HTA_producation=production_meta.get("HTA")
        advocacy_producation=production_meta.get("advocacy")
        #("research activity advocacy", advocacy_producation)
        #total_count_producation = publication_producation+CT_producation+societies_producation+congresses_producation+guidelines_producation+regulatory_bodies+hospitals_producation+HTA_producation+advocacy_producation
    if publication_staging == publication_producation :
        doc_diff_flag = ''
    else :
        doc_diff_flag = "Publications"
    print(doc_diff_flag)
    if congresses_staging == congresses_producation :
        congress_diff_flag = ''
    else :
        congress_diff_flag = "Congress"
    # ids = ''.join(author_id)
    print(congress_diff_flag)
    if CT_staging == CT_producation :
        CT_diff_flag = ''
    else :
        CT_diff_flag  = "Clinical Trials"
        # ids = ''.join(author_id)
    print(CT_diff_flag)
    if societies_staging == societies_producation :
        societies_diff_flag = ''
    else :
        societies_diff_flag  = "Societies"
        # ids = ''.join(author_id)
    print(societies_diff_flag)
    if guidelines_staging == guidelines_producation:
        guidelines_diff_flag = ''
    else:
        guidelines_diff_flag = "Guidelines"
    print(guidelines_diff_flag)
    if regulatory_bodies_staging == regulatory_bodies_producation:
        regulatory_bodies_diff_flag= ''
    else:
        regulatory_bodies_diff_flag = "Regulatory Bodies"
    print(regulatory_bodies_diff_flag)

    if hospitals_staging == hospitals_producation:
        hospitals_diff_flag = ''
    else:
        hospitals_diff_flag = "Hospital"
    print(hospitals_diff_flag)

    if HTA_staging == HTA_producation:
        HTA_diff_flag = ''
    else:
        HTA_diff_flag = 'HTA'
    print(HTA_diff_flag)

    if advocacy_staging == advocacy_producation:
        advocacy_diff_flag = ''
    else:
        advocacy_diff_flag = "Advocacy"
    print(advocacy_diff_flag)

    write_csv(author_id,publication_staging,publication_producation,doc_diff_flag,CT_staging,CT_producation,CT_diff_flag,congresses_staging,congresses_producation,congress_diff_flag,societies_staging,societies_producation,societies_diff_flag,regulatory_bodies_staging,regulatory_bodies_producation,regulatory_bodies_diff_flag,hospitals_staging,hospitals_producation,hospitals_diff_flag,HTA_staging,HTA_producation,HTA_diff_flag,advocacy_staging,advocacy_producation,advocacy_diff_flag,guidelines_staging,guidelines_producation,guidelines_diff_flag)

def write_csv(author_id,publication_staging,publication_producation,doc_diff_flag,CT_staging,CT_producation,CT_diff_flag,congresses_staging,congresses_producation,congress_diff_flag,societies_staging,societies_producation,societies_diff_flag,regulatory_bodies_staging,regulatory_bodies_producation,regulatory_bodies_diff_flag,hospitals_staging,hospitals_producation,hospitals_diff_flag,HTA_staging,HTA_producation,HTA_diff_flag,advocacy_staging,advocacy_producation,advocacy_diff_flag,guidelines_staging,guidelines_producation,guidelines_diff_flag):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author id','publication score','Research Activity','Publication Flag','CT Score','CT Acticity','CT Flag','Congress score','Congress Activity','Congress Flag','Socities Score','Socities Activity','Socities Flag','Regulatory Score','Regulatory Activity','Regulatory Flag','Hospital score','Hospital Activity','Hospital Flag','HTA score','HTA Activity','HTA Flag','Advocacy score','Advocacy Activity','Advocacy Flag','Guidelines','Guidelines Activity','Flag']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author id':author_id,'publication score':publication_staging,'Research Activity':publication_producation,'Publication Flag':doc_diff_flag,'CT Score':CT_staging,'CT Acticity':CT_producation,'CT Flag':CT_diff_flag,'Congress score':congresses_staging,'Congress Activity':congresses_producation,'Congress Flag':congress_diff_flag,'Socities Score':societies_staging,'Socities Activity':societies_producation,'Socities Flag':societies_diff_flag,'Regulatory Score':regulatory_bodies_staging,'Regulatory Activity':regulatory_bodies_producation,'Regulatory Flag':regulatory_bodies_diff_flag,'Hospital score':hospitals_staging,'Hospital Activity':hospitals_producation,'Hospital Flag':hospitals_diff_flag,'HTA score':HTA_staging,'HTA Activity':HTA_producation,'HTA Flag':HTA_diff_flag,'Advocacy score':advocacy_staging,'Advocacy Activity':advocacy_producation,'Advocacy Flag':advocacy_diff_flag,'Guidelines':guidelines_staging,'Guidelines Activity':guidelines_producation,'Flag':guidelines_diff_flag})

if __name__ == '__main__':

    list_staging,list_dict_staging=score(TEST_CASES["Oncology"])
    list_dict_activity=research_activity(list_dict_staging)
    print(json.dumps(list_dict_activity))


    # print("====================================================================")
    # print(list_dict_activity)

    for author_id_staging in list_dict_staging :
        scoreObj = list_staging.get(author_id_staging)
        researchObj = list_dict_activity.get(author_id_staging)
        compare(author_id_staging,scoreObj,researchObj)
