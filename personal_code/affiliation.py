from __future__ import print_function


import json
import pprint
import time
#import db
import requests
from pymongo import DESCENDING
from db import get_current_affiliation,get_history_affiliation,get_current_affiliation_Merck,get_History_affiliation_Merck #importget_current_affiliation,get_history_affiliations,get_current_affiliation_Merck,get_history_affiliation_Merck
from accounts import User_details, User_details
from test_cases_affiliation import TEST_ORGANIZATIONS,TEST_DESIGNATION,TEST_COUNTRY,TEST_CASES_Affiliation
#from test_cases import (TEST_ADDRESSES, TEST_CASES, TEST_EMAILS, TEST_PHONES)
# from test_cases_merck import (TEST_ADDRESSES_MERCK, TEST_CASES_MERCK,
#                          TEST_EMAILS_MERCK, TEST_PHONES_MERCK)


#---------------------------------------------------------
#                     1.  LOGIN LOGIC
#---------------------------------------------------------
AUTH_TOKEN = ""
KOL_id = ID
KOL_ID_2 = "60aa0668880a4e43b200ae63456fb16e"

BASE_API = "url/api/v2/kols/profile/"


def auth_login(account):

    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    # print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    # print(dict)
    code = dict['responseData']['callback_url']
    codes = code[29:]
    # print(codes)
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }

    req = requests.get(
        'url/api/v1/auth/callback', params=payload)
    accesstoken_dict = req.json()
    # print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    # print("Granted access token:", accesstoken[10:])
    return accesstoken
# to make Auth token generic


def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(account)

#---------------------------------------------------------
#          2.Environment for Preconditions
#---------------------------------------------------------
# Create Environment for preconditions


def run_condition(body, condition_type):
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }

    body = json.dumps(body)
    # print(body)
    req = requests.put(BASE_API + KOL_ID + '/affiliations',
                       params=params, data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)
    #print(condition_type, "Response Body : ", json.dumps(req.json()))

    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"
#--------------------------------------------------------------------
def get_data():
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }

    #body = json.dumps(body)
    # print(body)
    req = requests.get(BASE_API + KOL_ID + '/affiliations',
                       params=params,headers=Header)
    print("status : ", req.status_code)
    #print("Response Body : ", json.dumps(req.json()))
    application_id_dict = req.json()
    application_id = ''


    if AUTH_TOKEN_Merck:
        data=application_id_dict.get('data','')
        orgData=data.get('orgData', '')
        affiliations = orgData.get('affiliations',[])
        for affiliation in affiliations:
            application_id = affiliation.get('application_id')
            #print(application_id)

    else:
        data=application_id_dict.get('data','')
        innoData=data.get('innoData','')
        affiliations=innoData.get('affiliations',[])
        for affiliation in affiliations:
            application_id = affiliation.get('application_id')
            #print(application_id)
    return application_id
#-------------------------------------------------------------------
def excute_test_case(body, condition_type):
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as',

    }

    body = json.dumps(body)
    # print body

    #print(body)
    req = requests.put(BASE_API + KOL_ID + '/affiliations',
                       params=params, data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)


#---------------------------------------------------------
#          3.Main Test Case
#---------------------------------------------------------

def perform_check(test_cases_affiliation,key,account):
    current_list = check_current_collection()
    expected_current_collection_list = test_cases_affiliation.get(key)

    affiliation = ''
    test_affiliation = ''
    for affiliations in current_list:
        affiliation = affiliations.get('affiliation')

    for test_affiliations in expected_current_collection_list:
        test_affiliation = test_affiliations.get('affiliation')


    flag_check_affiliation = False
    # if both have something
    if test_affiliation and affiliation:
        if affiliation in test_affiliation:
            #print(check_type, "Email check:", "Pass")
            flag_check_affiliation = True
        else:
            #print(check_type, " Email check:", "Fail")
            flag_check_affiliation = False

    # Both are empty
    elif not test_affiliation and not affiliation:
        #print(check_type, " Email check:", "Pass")
        flag_check_affiliation = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_affiliation = False

    if flag_check_affiliation:
        # #pprint.pprint(current_emails)
        print("For affiliation,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For affiliation,Test case is Fail ")

    #---------------------Country---------------------

    var_name=''
    for response in current_list :
        country_l = response.get('extracted_terms' , {}).get('country' , [0])
        for country in country_l :
                var_name = country
    for test_country in expected_current_collection_list:
        test_countries = test_country.get('country')


        flag_check_country = False
        if var_name == test_countries:
            flag_check_country = True

        # # Both are empty
        elif not test_countries and not countries:
        #     #print(check_type, " Email check:", "Pass")
             flag_check_country = True
        # # One of them is empty
        else:
        #     #print(check_type, "Email check:", "Fail")
             flag_check_country = False
    else:
        flag_check_country = False


    if flag_check_country:
        print("For Country,Test case is Pass")
    else:
        # pprint.pprint(current_emails)
        print("For Country,Test case is Fail")
#---------------------------------------------------------------
    designations = ''
    Test_designations = ''
    for designation in current_list:
        designations = designation.get('designation')

    for Test_designation in expected_current_collection_list:
        Test_designations = Test_designation.get('designation')


    flag_check_designation = False
    # if both have something
    # if Test_designations and designations:
    #     for designation in designations:
            #print(check_type, "Checking email:", email['email'])
    if designations == Test_designations:
        #print(check_type, "Email check:", "Pass")
        flag_check_designation = True
    else:
        #print(check_type, " Email check:", "Fail")
        flag_check_designation = False

    # # Both are empty
    # elif not Test_designations and not designations:
    #     #print(check_type, " Email check:", "Pass")
    #     flag_check_designation = True
    # # One of them is empty
    # else:
    #     #print(check_type, "Email check:", "Fail")
    #     flag_check_designation = False

    if flag_check_designation:
        # #pprint.pprint(current_emails)
        print("For Designation,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For Designation,Test case is Fail ")

    if flag_check_affiliation and flag_check_country and flag_check_designation:
        print(" Current Collection Check:", "Pass")

    else:
        print("Current Collection Check:", "Fail")
#-----------------------------------------------------------------------
def perform_check_history(test_cases_affiliation,key,account):
    history_data = check_history_collection()
    expected_current_collection_list = test_cases_affiliation.get(key)
    affiliation = history_data[0].get('affiliation')
    # for affiliations in history_data:
    #     affiliation = affiliations.get('affiliation')
    for test_affiliations in expected_current_collection_list:
        test_affiliation = test_affiliations.get('affiliation')
    flag_check_affiliation = False
    if test_affiliation == affiliation:
        flag_check_affiliation = True
    # if test_affiliation and affiliation:
    #     if affiliation == test_affiliation:
    #         flag_check_affiliation = True
    #     else:
    #         flag_check_affiliation = False
    # # Both are empty
    # elif not test_affiliation and not affiliation:
    #     #print(check_type, " Email check:", "Pass")
    #     flag_check_affiliation = True
    # # One of them is empty
    # else:
    #     flag_check_affiliation = False
    if flag_check_affiliation:
        print("For History affiliation,Test case is Pass ")
    else:
        print("For History affiliation,Test case is Fail ")
    #---------------------Country---------------------
    #for country in history_data:
    recent_doc_countries = history_data[0].get('extracted_terms').get('country')
    country = recent_doc_countries[0] if len(recent_doc_countries)>0 else None
    for test_country in expected_current_collection_list:
        test_countries = test_country.get('country')
    test_country = test_countries if test_countries else None
    flag_check_country = False
    if country == test_country:
        flag_check_country = True
    # if country and test_country:
    #     #for country in countries:
    #         #print(check_type, "Checking email:", email['email'])
    #     if test_country == country:
    #         #print(check_type, "Email check:", "Pass")
    #
    #     else:
    #         #print(check_type, " Email check:", "Fail")
    #         flag_check_country = False
    #
    # # Both are empty
    # elif not test_countries and not countries:
    #     #print(check_type, " Email check:", "Pass")
    #     flag_check_country = True
    # # One of them is empty
    # else:
    #     #print(check_type, "Email check:", "Fail")
    #     flag_check_country = False
    if flag_check_country:
        print("For History Country,Test case is Pass ")
    else:
        print("For History Country,Test case is Fail ")
#---------------------------------------------------------------
    designation = history_data[0].get('designation')
    #for designation in history_data:
    #     designations = designation.get('designation')
    for Test_designation in expected_current_collection_list:
        Test_designations = Test_designation.get('designation')
    flag_check_designation = False
    if Test_designations == designation:
        flag_check_designation = True
    # if both have something
    # if Test_designations and designations:
    #     for designation in designations:
    #         #print(check_type, "Checking email:", email['email'])
    #         if designations in Test_designations:
    #             #print(check_type, "Email check:", "Pass")
    #             flag_check_designation = True
    #         else:
    #             #print(check_type, " Email check:", "Fail")
    #             flag_check_designation = False
    #
    # # Both are empty
    # elif not Test_designations and not designations:
    #     #print(check_type, " Email check:", "Pass")
    #     flag_check_designation = True
    # # One of them is empty
    # else:
    #     #print(check_type, "Email check:", "Fail")
    #     flag_check_designation = False
    if flag_check_designation:
        print("For History,Test case is Pass ")
    else:
        print("For History,Test case is Fail ")

    if flag_check_affiliation and flag_check_country and flag_check_designation:
        print(" History Collection Check:", "Pass")

    else:
        print(" History Collection Check:", "Fail")
#---------------------------------------------------------
#         Merck Perform check
#---------------------------------------------------------
def perform_check_Merck(test_cases_affiliation, key):
    current_list = check_current_collection_merck()
    expected_current_collection_list = test_cases_affiliation.get(key)

    affiliation = ''
    test_affiliation = ''
    for affiliations in current_list:
        affiliation = affiliations.get('affiliation')

    for test_affiliations in expected_current_collection_list:
        test_affiliation = test_affiliations.get('affiliation')

    flag_check_affiliation = False
    # if both have something
    if test_affiliation and affiliation:
        if affiliation == test_affiliation:
            #print(check_type, "Email check:", "Pass")
            flag_check_affiliation = True
        else:
            #print(check_type, " Email check:", "Fail")
            flag_check_affiliation = False

    # Both are empty
    elif not test_affiliation and not affiliation:
        #print(check_type, " Email check:", "Pass")
        flag_check_affiliation = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_affiliation = False

    if flag_check_affiliation:
        # #pprint.pprint(current_emails)
        print("For affiliation,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For affiliation,Test case is Fail ")

    #---------------------Country---------------------
    var_name = ''
    for response in current_list :
        country_l = response.get('extracted_term' , {}).get('country' , [])
        for country in country_l :
                countries = country
    #for country in current_list:
    #    countries = country.get('country')
    for test_country in expected_current_collection_list:
        test_countries = test_country.get('country')
    countries = ''
    test_countries = ''
    flag_check_country = False
    # if both have something
    if test_countries and countries:
        #or country in countries:
            #print(check_type, "Checking email:", email['email'])
        if countries == test_countries:
                #print(check_type, "Email check:", "Pass")
            flag_check_country = True
        else:
                #print(check_type, " Email check:", "Fail")
            flag_check_country = False

    # Both are empty
    elif not test_countries and not countries:
        #print(check_type, " Email check:", "Pass")
        flag_check_country = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_country = False

    if flag_check_country:
        # #pprint.pprint(current_emails)
        print("For Country,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For Country,Test case is Fail ")
#---------------------------------------------------------------
    for designation in current_list:
        designations = designation.get('designation')

    for Test_designation in expected_current_collection_list:
        Test_designations = Test_designation.get('designation')

    flag_check_designation = True
    # if both have something
    designations = ''
    Test_designations = ''
    if Test_designations and designations:
        for designation in designations:
            #print(check_type, "Checking email:", email['email'])
            if designations in Test_designations:
                #print(check_type, "Email check:", "Pass")
                flag_check_designation = True
            else:
                #print(check_type, " Email check:", "Fail")
                flag_check_designation = False

    # Both are empty
    elif not Test_designations and not designations:
        #print(check_type, " Email check:", "Pass")
        flag_check_designation = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_designation = False

    if flag_check_designation:
        # #pprint.pprint(current_emails)
        print("For Designation,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For Designation,Test case is Fail ")

    if flag_check_affiliation and flag_check_country and flag_check_designation:
        print(" Merck Current Collection Check:", "Pass")

    else:
        print("Merck Current Collection Check:", "Fail")
#-----------------------------------------------------------------
# Merck-History-collection
#-----------------------------------------------------------------
def perform_check_history_merck(test_cases_affiliation,key):
    history_data = check_history_collection_merck()
    expected_current_collection_list = test_cases_affiliation.get(key)

    for affiliations in history_data:
        affiliation = affiliations.get('affiliation')

    for test_affiliations in expected_current_collection_list:
        test_affiliation = test_affiliations.get('affiliation')
    #print(affiliation)
    #print(test_affiliation)
    print(affiliation==test_affiliation)
    flag_check_affiliation = False
    # if both have something
    if test_affiliation and affiliation:
        if affiliation == test_affiliation:
            #print(check_type, "Email check:", "Pass")
            flag_check_affiliation = True
        else:
            #print(check_type, " Email check:", "Fail")
            flag_check_affiliation = False

    # Both are empty
    elif not test_affiliation and not affiliation:
        #print(check_type, " Email check:", "Pass")
        flag_check_affiliation = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_affiliation = False

    if flag_check_affiliation:
        # #pprint.pprint(current_emails)
        print("For History affiliation,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For History affiliation,Test case is Fail ")

    #---------------------Country---------------------


    #for country in history_data:
    #    countries = country.get('country')

    countries = ''

    for response in history_data :
        country_l = response.get('extracted_terms' , {}).get('country' , [])
        countries = country_l[0]

    for test_country in expected_current_collection_list:
        test_countries = test_country.get('country')

    flag_check_country = False
    # if both have something
    if test_countries and countries:
        for country in countries:
            for test_country in test_countries:
                if country == test_country:
                    flag_check_country = True
                    break

    # Both are empty
    elif not test_countries and not countries:
        #print(check_type, " Email check:", "Pass")
        flag_check_country = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_country = False

    if flag_check_country:
        # #pprint.pprint(current_emails)
        print("For History Country,Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For History Country,Test case is Fail ")
#---------------------------------------------------------------
    for designation in history_data:
        designations = designation.get('designation')

    for Test_designation in expected_current_collection_list:
        Test_designations = Test_designation.get('designation')

    flag_check_designation = False
    # if both have something
    if Test_designations and designations:
        #for designation in designations:
            #print(check_type, "Checking email:", email['email'])
            if designations == Test_designations:
                #print(check_type, "Email check:", "Pass")
                flag_check_designation = True
            else:
                #print(check_type, " Email check:", "Fail")
                flag_check_designation = False

    # Both are empty
    elif not Test_designations and not designations:
        #print(check_type, " Email check:", "Pass")
        flag_check_designation = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_designation = False

    if flag_check_designation:
        # #pprint.pprint(current_emails)
        print("For History,Designation Test case is Pass ")

    else:
        # pprint.pprint(current_emails)
        print("For History,Test case is Fail ")

    if flag_check_affiliation and flag_check_country and flag_check_designation:
        print("Merck History Collection Check:", "Pass")

    else:
        print(" Merck History Collection Check:", "Fail")



#---------------------------------------------------------
#                    6.DB collections
#---------------------------------------------------------
# To check History collection

def check_current_collection():

    collection = get_current_affiliation()
    find_current = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING)
    current_data = list(find_current)
    #print(current_data)

    return current_data


def check_history_collection():

    collection = get_history_affiliation()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING)
    history_data = list(find_last_history)
    #print(history_data)
    return history_data

    #perform_check(history_data, test_case, 'test_case_history', "History")


def check_current_collection_merck():

    collection = get_current_affiliation_Merck()
    find_current = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING)
    current_data = list(find_current)
    #print(current_data)

    return current_data

def check_history_collection_merck():

    collection = get_History_affiliation_Merck()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING)
    history_data = list(find_last_history)
    #print(history_data)
    return history_data


#---------------------------------------------------------
#                     Final Step
#---------------------------------------------------------


def run_test_cases():

    get_auth_token(User_details)

    for test_case in TEST_CASES_Affiliation:
        print("-" * 20)
        print("Executing",test_case['test_case_name'])
        print("-" * 20)
        print(test_case['test_case_descripation'])
        print("-" * 20)
        #print(association_data['precondition'])
        print("-" * 20)

        # 1. Generate Environment
        run_condition(test_case['test_case_senario'], 'Scenario')
        time.sleep(5)
        #Get application
        get_data()
        time.sleep(5)
        # 2. Execute Test case.
        if test_case['test_case_name'] =='Edit_Affiliation':
            test_case['test_case_senario']["affiliations"][0]["application_id"] = get_data()
        excute_test_case(test_case['test_case_execute'], 'Test Case')
        print("-" * 20)
        time.sleep(5)

        check_current_collection()
        #check curent collection
        print("-" * 20)
        perform_check(test_case['test_case_execute'],'affiliations',User_details)
        print("-" * 20)
        #run_condition(test_case['test_case_execute'], 'Test Case')
        check_history_collection()
        print("-" * 20)
        #excute History collection
        perform_check_history(test_case['test_case_senario'],'affiliations',User_details)
        print("-" * 20)
        #perform_check(test_case['test_case_senario'],'affiliations',User_details)


        '''
    get_auth_token(User_details)
    for test_case in TEST_CASES_Affiliation:
        print("-" * 20)
        print("Executing",test_case['test_case_name'])
        print("-" * 20)
        print(test_case['test_case_descripation'])
        print("-" * 20)
        #print(association_data['precondition'])
        print("-" * 20)

        # 1. Generate Environment
        run_condition(test_case['test_case_senario'], 'Scenario')
        print("-" * 20)
        #Get application
        get_data()
        time.sleep(5)
        # 2. Execute Test case.
        if test_case['test_case_name'] =='Edit_Affiliation':
            test_case['test_case_senario']["affiliations"][0]["application_id"] = get_data()
        excute_test_case(test_case['test_case_execute'], 'Test Case')
        print("-" * 20)
        time.sleep(5)
        check_current_collection_merck()
        #check curent collection
        perform_check_Merck(test_case['test_case_execute'],'affiliations')
        time.sleep(5)
        print("-" * 20)
        #run_condition(test_case['test_case_execute'], 'Test Case')
        check_history_collection_merck()
        #excute History collection
        time.sleep(5)
        perform_check_history_merck(test_case['test_case_senario'],'affiliations')
        print("-" * 20)
        print("-" * 20)


        '''

#---------------------------------------------------------


if __name__ == '__main__':
    run_test_cases()
