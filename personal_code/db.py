import urllib

import pymongo

import config


def get_collections_history():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_NAME
    db = conn[database_name]
    collection_name = config.DB_COLLECTION
    collection = db[collection_name]
    return collection


def get_collections_history_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_Merck_History
    db = conn[database_name]
    collection_name = config.DB_Merck_COllection
    collection = db[collection_name]
    return collection


def get_current_association_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_Merck_Current_assocition
    db = conn[database_name]
    collection_name = config.DB_Merck_COllection_association
    collection = db[collection_name]
    return collection


def get_history_association_Merck():
    #associations_data_merck_history
    #collection name--------associations_data_history

    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_Merck_History_assocition
    db = conn[database_name]
    collection_name = config.DB_Merck_History_COllection_association
    collection = db[collection_name]
    return collection


def get_current_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION
    collection = db[collection_name]
    return collection

def get_history_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION_History
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION_History
    collection = db[collection_name]
    return collection

def get_current_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION
    collection = db[collection_name]
    return collection

def get_history_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION_History
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION_History
    collection = db[collection_name]
    return collection

def get_current_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION
    collection = db[collection_name]
    return collection

def get_history_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION_History
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION_History
    collection = db[collection_name]
    return collection

def get_current_affiliation():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION
    collection = db[collection_name]
    return collection

def get_history_affiliation():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_INNO_AFFILIATION_History
    db = conn[database_name]
    collection_name = config.DB_INNO_COLLECTION_AFFILIATION_History
    collection = db[collection_name]
    return collection

def get_current_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_Merck_AFFILIATION
    db = conn[database_name]
    collection_name = config.DB_Merck_COLLECTION_AFFILIATION
    collection = db[collection_name]
    return collection

def get_History_affiliation_Merck():
    uri = "mongodb://" + config.DB_USERNAME + ":" + \
        config.DB_PASSWORD + "@" + config.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = config.DB_Merck_AFFILIATION_History
    db = conn[database_name]
    collection_name = config.DB_Merck_COLLECTION_AFFILIATION_History
    collection = db[collection_name]
    return collection
