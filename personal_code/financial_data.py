from accounts import User_details, User_details
import requests
import json
import time
import csv
import os.path
from elasticsearch import Elasticsearch
global_client = elastic_Search_ip)
out_filename ="personal_info.csv"
out_filename1="research_activity.csv"
out_filename_2="authon_name.csv"
out_filename_3="affiliation.csv"
out_filename_4="connection.csv"
import pprint
import re
KOL_ID_1=['06cfd47fe042491a90f438aff9f54806',
'74d84a14401f49469df9e190b407fe99',
'a7eed96c47f543c69fed3974063c8b1b',
'22da99cc94644119b3a17d4046bf907f',
'14c28c36e69645db8f099d42bc0e45f6',
'e46b39c1165b40a0960ceaff846a2abf',
'3a996c0e86c146259a17afddf0665c80',
'eb8bf6c0aa1d4fcb9db60479b10bfb3f',
'ee26a6ce5a794a6dbfe73d345946154d',
'a519ce8b4d7941158d1eac52df27e2cf',
'48edb20263224ac3a8b76c6f60858774',
'73962b9a31b24f07b4f0871a8c8a375d',
'fe7e0e7396d44c7495947d874ce3624c',
'b97688c070bc4e0ea68abeef0df2607e',
'4a9c0d7c812a4e74b07f572ddb80133e',
'6151563076924b42992c8dcce432ea07',
'd3a36c3bc4f246b3b419226630656c6d',
'21233cc4ca7545828c116199e094df28',
'86f4206121f84f42873707455a79cf7b',
'7d302c52b93e4244ad1c932d2dcf4b5d',
'e73ea34671d148e394caeceadec8899f',
'e94a28fa973a4cbabe50f827989734ae',
'b418dc01dca34f2dab9fdd0c2f378787',
'bf68ea315f6144dab2577ade61ef27b6',
'f53d12f86e254ed09420ca1fe19e0ec1',
'a788d56a4a7641f48a615a4844af347e',
'7890542e054d4b2e8bcbb86abd2dce25',
'45e9528ca0ab47d294f6dd0e5bc2d547',
'0b44d30be86d4efdac83b242ee5e49d7',
'2dc047c29639417cbb0ddb3fc62727fa',
'142124e3e7554a6ba389af6f86c3d2ba']
# def auth_login(account):
#     r = requests.post('Url_of_application', json=account)
#     #print(r.status_code)
#     #pprint.pprint(r.json())
#     dict = r.json()
#     #print(dict)
#     code = dict['responseData']['callback_url']
#     #print(code)
#     codes = code[34:]
#     #print(codes)
#     #print("Granted login codes :", codes)
#     return codes
# def auth_callback_api(account):
#     global accesstoken
#     payload = {
#         "code": auth_login(account),
#         "app":"name_of_app"
#     }
#     req = requests.get(
#         'application_callback_url', params=payload)
#     #print(r.status_code)
#     accesstoken_dict = req.json()
#     #print(accesstoken_dict)
#     # print(accesstoken_dict['accessToken'])
#     accesstoken = accesstoken_dict['accessToken']
#     #print(accesstoken)
#     #print("Granted access for staging")
#     return accesstoken
# # to make Auth token generic
# def get_auth_token(account):
#     global AUTH_TOKEN
#     AUTH_TOKEN = auth_callback_api(account)
#     return AUTH_TOKEN

Application's_url
pags = ''
AUTH_TOKEN ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzA1MjYyNzMsInNlc3Npb25fa2V5IjoiNjBjYzgzZmQtZGE4MS00YWM1LWEyMmUtM2I1YzM0ZmNiMDdiIiwiZXhwIjoxNTMwNjEyNjczfQ.ibXW3SCqITiZxVk9ioesNPxcP21vafTuaGNZvQjYeBg"

# def get_input(as_class):
#     get_auth_token(User_details)
#     url = 'url'
#     payload = {"from":0,"limit":10,"TA":"Financial","asset_class":as_class,"filters":{"kol_info":[{"name":"Designation","value":25,"showDate":False,"startDate":0,"endDate":0},{"name":"Network","value":50,"showDate":False,"startDate":0,"endDate":0},{"name":"Affiliation","value":25,"showDate":False,"startDate":0,"endDate":0}],"showScore":True,"kolType":"financial"}}
#     headers = {
#         'Authorization': 'Bearer ' + AUTH_TOKEN,
#         'content-type': 'application/json'
#     }
#     r = requests.post(url, data=json.dumps(payload), headers=headers)
#     r = r.json()
#     for i in r['data'][0]['scores']:
#         # count+= 1
#         # print(count)
#         # if count <= 15:
#             #continue
#         #author_id=i['author_id']
#         #print(i['author_id'],as_class)
#         KOL_ID_1.append(i['author_id'])
#         # if count == 20:
#         #     break
def author_name(KOL_ID_1):
    #difference=list(list_staging)
    #difference_str=",".join(difference)
    author_nm=''
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    for KOL_ID in KOL_ID_1:
        params = {
             'author_ids': KOL_ID
        }
        #import ipdb; ipdb.set_trace()
        url = 'url/api/v2/kols/list/'
        req = requests.get(url,params,headers=Header)
        inno_data = req.json().get('data',[])
        for data in inno_data :
            author_nm = data.get('nm','')
            print(author_nm)
            write_name(KOL_ID,author_nm)

    return author_nm
def write_name(KOL_ID,author_nm):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename_2, 'a') as csvfile:
           feild_names= ['Author ID','Author Name']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Author Name':author_nm,})

def research_activity():

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/research_activity'
        res = requests.get(url, params, headers=Header)
        print("status : ", res.status_code)
        inno_data = res.json().get('data',[])
        dict = {"KOL_ID":KOL_ID,"asset_classes":[]}
        pharma_watch = []
        list1 = []
        flag = False

        for data in inno_data :
            score_data_staging = data.get('name',[])
            docs = data.get('docs',[])
            asset_classes=dict["asset_classes"].append(score_data_staging)
            for doc in docs :
                watch = doc.get('key_company_name','')
                pharma = watch.replace(" ","").lower()
                if pharma :
                    list1.append(pharma)
                if watch :
                    pharma_watch.append(watch)
                if len(list1)>1:
                    for i in range(0,len(list1[1:])):
                        if list1[i]==list1[i+1]:
                            flag = True
                        elif list1[0]==list1[i+1]:
                            flag = True



        write_research(KOL_ID,dict['asset_classes'],pharma_watch,flag)
        #print(dict)
    #===========================================================================
def es_query(author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body

def personal_info():

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/personal_info'
        print(url)
        #print(json.dumps(params))
        #dict = {"KOL_ID":KOL_ID,"email":[],"phone":[],"addresse":[]}
        res = requests.get(url, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))
        pho = ''
        phone = []
        addr = ''
        telephone = []
        addresse=''
        email_1=[]

        duplicate_flag = False


        data = res.json().get('data',[])
        inno_data=data.get('innoData',[])
        print(inno_data)
        emails=inno_data.get('emails',[])
        for email in emails:
            mail = email.get('email')
            email_1.append(mail)
            #dict["email"].append(email_1)

        print(email_1,KOL_ID)

        phones=inno_data.get('phone_numbers',[])
        for pho in phones:
            te = pho.get('phone')
            if te :
                ph = te.encode('utf-8')
            telephone.append(ph)

            phon= re.sub(r'[^\w]', ' ',pho.get('phone'))
            tel = filter(lambda x: x.isdigit(),phon)
            phone.append(tel)
        if len(phone)>1 :
            for i in range(0,len(phone[1:])):
                if phone[i]==phone[i+1]:
                    duplicate_flag = True
                elif phone[0]==phone[i+1]:
                    duplicate_flag = True

        print(telephone , KOL_ID)
            #dict["phone"].append(phone)
        addresses=inno_data.get('addresses',[])
        for addr in addresses:
            addresse=addr.get('address')
            print(addresse,KOL_ID)
            #dict["addresse"].append(addresse)
        # if email or phone or addresse:
        #     print(dict)
        #==========================author_name=====================================
        author_nm=''
        Header = {
            'Authorization': 'Bearer ' + AUTH_TOKEN,
            'content-type': 'application/json'
        }

        params = {
             'author_ids': KOL_ID
        }
        #import ipdb; ipdb.set_trace()
        url = 'url/api/v2/kols/list/'
        req = requests.get(url,params,headers=Header)
        inno_data = req.json().get('data',[])

        for data in inno_data :
            author_nm = data.get('nm','')
            print(author_nm)
            write_name(KOL_ID,author_nm)
    #=============================affiliations===================================
        list_affiliation=[]
        Header = {
            'Authorization': 'Bearer ' + AUTH_TOKEN,
            'content-type': 'application/json'
        }
        url = BASE_API + 'kols/profile/' + KOL_ID + '/affiliations'
            #print(url)
            #print(json.dumps(params))
        #dict = {"KOL_ID":KOL_ID,"email":[],"phone":[],"addresse":[]}
        res = requests.get(url, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))
        data = res.json().get('data',[])
        inno_data = data.get('innoData',[])
        affiliations = inno_data.get('affiliations')
        count = 0
        for aff in affiliations:
            affiliation=aff.get('affiliation')
            #write_affiliation(KOL_ID,affiliation)
            #sprint(affiliation,KOL_ID)
            count+=1
            if count == 1:
                break
        print(KOL_ID,author_nm,email_1,telephone,duplicate_flag,addresse,affiliation)
        write_csv(KOL_ID,author_nm,email_1,telephone,duplicate_flag,addresse,affiliation)

def write_csv(KOL_ID,author_nm,email_1,telephone,duplicate_flag,addresse,affiliation) :
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author ID','Author Name','Emails','Phones','Duplicate_no','Addresses','Affiliation']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Author Name':author_nm,'Emails':email_1, 'Phones':telephone,'Duplicate_no':duplicate_flag,'Addresses':addresse,'Affiliation':affiliation})
# def affiliation():
#     list_affiliation=[]
#     Header = {
#         'Authorization': 'Bearer ' + AUTH_TOKEN,
#         'content-type': 'application/json'
#     }
#
#     for KOL_ID in KOL_ID_1:
#         url = BASE_API + 'kols/profile/' + KOL_ID + '/affiliations'
#             #print(url)
#             #print(json.dumps(params))
#
#         #dict = {"KOL_ID":KOL_ID,"email":[],"phone":[],"addresse":[]}
#         res = requests.get(url, headers=Header)
#         print("status : ", res.status_code)
#         #print(json.dumps(res.json()))
#
#         data = res.json().get('data',[])
#         inno_data = data.get('innoData',[])
#         affiliations = inno_data.get('affiliations')
#         count = 0
#         for aff in affiliations:
#             affiliation=aff.get('affiliation')
#             write_affiliation(KOL_ID,affiliation)
#             print(affiliation,KOL_ID)
#             count+=1
#             if count == 1:
#                 break
def connections():
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/connections'
        time.sleep(5)
        res = requests.get(url, params, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))
        data = res.json().get('data',[])
        inno_data = data.get('connections',[])


        pharma_Connections=inno_data.get('pharmaConnections',[])
        finConnections=inno_data.get('finConnections',[])
        author_pharma = {}
        author_finance = {}

        for connection in pharma_Connections :
                body=es_query(connection)
                response =global_client.search(index="authors_alias", body = body, request_timeout=100)
                for doc in response['hits']['hits']:
                    author=doc.get('_source',{}).get('name','')
                    #author_pharma.append(author+" @@ "+connection)
                    author_pharma[connection] = author
        for financial_connection in finConnections :
            body=es_query(financial_connection)
            response =global_client.search(index="authors_alias", body = body, request_timeout=100)
            for doc in response['hits']['hits']:
                author=doc.get('_source',{}).get('name','')
                #author_pharma.append(author+" @@ "+financial_connection)
                author_finance[financial_connection] = author

        print(author_pharma)

        csv_connections(KOL_ID,author_pharma,author_finance)

def csv_connections(KOL_ID,author_pharma,author_finance):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename_4, 'a') as csvfile:
           feild_names= ['Author ID','Pharma Connections','Financial Connections']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Pharma Connections':author_pharma,'Financial Connections':author_finance})

#============================================
# def write_affiliation(KOL_ID,affiliation):
#        file_exists = os.path.isfile(out_filename)
#        with open(out_filename_3, 'a') as csvfile:
#            feild_names= ['Author ID','affiliation']
#            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
#            if not file_exists:
#                writer.writeheader()
#            writer.writerow({'Author ID':KOL_ID,'affiliation':affiliation,})
def write_research(KOL_ID,asset_classes,pharma_watch,flag) :
       file_exists = os.path.isfile(out_filename)
       with open(out_filename1, 'a') as csvfile:
           feild_names= ['Author ID','Asset Class','Pharma Watch','Duplicate pharma watch']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Asset Class':asset_classes,'Pharma Watch':pharma_watch,'Duplicate pharma watch':flag})

if __name__ == '__main__':
    author_name(KOL_ID_1)
    #personal_info()
    research_activity()
    # affiliation()
    #connections()
