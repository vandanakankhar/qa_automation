 #from account import User_details, User_details
import requests
import json
import time
import csv
import os.path
from elasticsearch import Elasticsearch
global_client = Elasticsearch(['elastic_Search_username_password'])
out_filename ="personal_info.csv"
out_filename1="research_activity.csv"
out_filename_2="authon_name.csv"
out_filename_3="affiliation.csv"
out_filename_4="connection.csv"
import pprint
import re
KOL_ID_1=[]
Application's_url
pags = ''
AUTH_TOKEN =""

def author_name(KOL_ID_1):
    #difference=list(list_staging)
    #difference_str=",".join(difference)
    author_nm=''
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    for KOL_ID in KOL_ID_1:
        params = {
             'author_ids': KOL_ID
        }
        #import ipdb; ipdb.set_trace()
        url = 'url/api/v1/kols/list/'
        req = requests.get(url,params,headers=Header)
        inno_data = req.json().get('data',[])
        for data in inno_data :
            author_nm = data.get('nm','')
            print(author_nm)
            write_name(KOL_ID,author_nm)

    return author_nm
def write_name(KOL_ID,author_nm) :
       file_exists = os.path.isfile(out_filename)
       with open(out_filename_2, 'a') as csvfile:
           feild_names= ['Author ID','Author Name']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Author Name':author_nm,})

def research_activity():

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/research_activity'
        res = requests.get(url, params, headers=Header)
        print("status : ", res.status_code)
        inno_data = res.json().get('data',[])

        dict = {"KOL_ID":KOL_ID,"asset_classes":[],"Count":[]}
        pharma_watch = []
        list1 = []
        flag = False

        for data in inno_data :
            score_data_staging = data.get('name',[])
            docs = data.get('docs',[])
            asset_classes=dict["asset_classes"].append(score_data_staging)
            count=data.get('count')
            counts=dict["Count"].append(count)
            for doc in docs :
                watch = doc.get('key_company_name','') or doc.get('key_pharma_companies','')
                pharma = watch.replace(" ","").lower()
                if pharma :
                    list1.append(pharma)
                if watch :
                    pharma_watch.append(watch)
                if len(list1)>1:
                    for i in range(0,len(list1[1:])):
                        if list1[i]==list1[i+1]:
                            flag = True
                        elif list1[0]==list1[i+1]:
                            flag = True



        write_research(KOL_ID,dict['Count'],dict['asset_classes'],pharma_watch,flag)
        print(dict)
    #===========================================================================
def es_query(author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body
def personal_info():
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/personal_info'
            #print(url)
            #print(json.dumps(params))
        #dict = {"KOL_ID":KOL_ID,"email":[],"phone":[],"addresse":[]}
        res = requests.get(url, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))
        pho = ''
        phone = []
        addr = ''
        telephone = []
        addresse=''
        email_1=[]
        duplicate_flag = False


        data = res.json().get('data',[])
        inno_data=data.get('innoData',[])
        #print(inno_data)
        emails=inno_data.get('emails',[])
        for email in emails:
            mail = email.get('email')
            email_1.append(mail)
            #dict["email"].append(email_1)

        print(email_1,KOL_ID)

        phones=inno_data.get('phone_numbers',[])
        for pho in phones:
            te = pho.get('phone')
            if te :
                ph = te.encode('utf-8')
            telephone.append(ph)
            phon= re.sub(r'[^\w]', ' ',pho.get('phone'))
            tel = filter(lambda x: x.isdigit(),phon)
            phone.append(tel)
        if len(phone)>1 :
            for i in range(0,len(phone[1:])):
                if phone[i]==phone[i+1]:
                    duplicate_flag = True
                elif phone[0]==phone[i+1]:
                    duplicate_flag = True

        print(telephone , KOL_ID)
            #dict["phone"].append(phone)
        addresses=inno_data.get('addresses',[])
        for addr in addresses:
            addresse=addr.get('address')
            print(addresse,KOL_ID)
            #dict["addresse"].append(addresse)
        # if email or phone or addresse:
        #     print(dict)
        write_csv(KOL_ID,email_1,telephone,duplicate_flag,addresse)
def affiliation():
    list_affiliation=[]
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }

    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/affiliations'
            #print(url)
            #print(json.dumps(params))

        #dict = {"KOL_ID":KOL_ID,"email":[],"phone":[],"addresse":[]}
        res = requests.get(url, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))

        data = res.json().get('data',[])
        inno_data = data.get('innoData',[])
        affiliations = inno_data.get('affiliations')
        count = 0
        for aff in affiliations:
            affiliation=aff.get('affiliation')
            write_affiliation(KOL_ID,affiliation)
            print(affiliation,KOL_ID)
            count+=1
            if count == 1:
                break
def connections():
    #import ipdb; ipdb.set_trace()
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/connections'
        #time.sleep(5)
        res = requests.get(url, params, headers=Header)
        print("status : ", res.status_code)
        #print(json.dumps(res.json()))
        data = res.json().get('data',[])
        inno_data = data.get('connections',[])

        pharma_Connections=inno_data.get('pharmaConnections',[])
        finConnections=inno_data.get('finConnections',[])
        author_pharma = []
        author_finance = []
        for connection in pharma_Connections :
                body=es_query(connection)
                response =global_client.search(index="authors_alias", body = body, request_timeout=100)
                for doc in response['hits']['hits']:
                    author=doc.get('_source',{}).get('name','')
                    author_pharma.append(author+" @@ "+connection)
        for financial_connection in finConnections :
            #import ipdb; ipdb.set_trace()
            body=es_query(financial_connection)
            response =global_client.search(index="authors_alias", body = body, request_timeout=100)
            for doc in response['hits']['hits']:
                author=doc.get('_source',{}).get('name','')
                author_pharma.append(author+" @@ "+financial_connection)
        #import ipdb; ipdb.set_trace()
        print(author_pharma)

        csv_connections(KOL_ID,author_pharma,author_finance)
        print(author_finance)


def csv_connections(KOL_ID,pharma_Connections,finConnections):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename_4, 'a') as csvfile:
           feild_names= ['Author ID','Pharma Connections','Financial Connections']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Pharma Connections':pharma_Connections,'Financial Connections':finConnections})

#============================================


def write_affiliation(KOL_ID,affiliation):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename_3, 'a') as csvfile:
           feild_names= ['Author ID','affiliation']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'affiliation':affiliation,})






def write_research(KOL_ID,count,asset_classes,pharma_watch,flag) :
       file_exists = os.path.isfile(out_filename)
       with open(out_filename1, 'a') as csvfile:
           feild_names= ['Author ID','Count','Asset Class','Pharma Watch','Duplicate pharma watch']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Count':count,'Asset Class':asset_classes,'Pharma Watch':pharma_watch,'Duplicate pharma watch':flag})


def write_csv(KOL_ID,email,phone,duplicate_flag,addresse) :
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author ID','Emails','Phones','Duplicate_no','addresses']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':KOL_ID,'Emails':email, 'Phones':phone,'Duplicate_no':duplicate_flag,'addresses':addresse,})


if __name__ == '__main__':
    #author_name(KOL_ID_1)
    #personal_info()
    research_activity()
    #affiliation()
    #connections()
