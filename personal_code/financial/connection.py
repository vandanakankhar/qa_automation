#from accounts import User_details, User_details
import requests
import json
import time
import csv
import os.path
from elasticsearch import Elasticsearch
global_client = elastic_Search_ip)
out_filename ="connection.csv"

import re
KOL_ID_1=[]

AUTH_TOKEN=""

Application's_url

def connections():
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10,
        "group":"org"
    }
    count = 0
    #res = []
    for KOL_ID in KOL_ID_1:
        url = BASE_API + 'kols/profile/' + KOL_ID + '/connections'
        res = requests.get(url, params=params, headers=Header)
        print(res.url)
        print("=======================================connection-API===================")
        print("status : ", res.status_code)
        print("=======================================connection-API===================")
        print(json.dumps(res.json()))
        #print("================================================================")
        data = res.json().get('data',[])
        inno_data = data.get('connections',[])
        #print(inno_data)
        pharma_Connections=inno_data.get('pharmaConnections',[])
        finConnections=inno_data.get('finConnections',[])
        #=======================================================================
        author_nm=''
        Header = {
            'Authorization': 'Bearer ' + AUTH_TOKEN,
            'content-type': 'application/json'
        }

        params = {
             'author_ids': KOL_ID
        }

        url = 'url/api/v2/kols/list/'
        req = requests.get(url,params,headers=Header)
        print("=======================================connection-API---2===================")
        print(req)
        print("=========================================connection-API---2===")
        #print(json.dumps(req.json()))
        inno_data = req.json().get('data',[])
        #print(json.dumps(inno_data))
        for data in inno_data :
            author_name = data.get('nm')
            print(author_name)


        author_name_fun(author_name,KOL_ID,pharma_Connections,finConnections)
        # res.append({KOL_ID,pharma_Connections,finConnections})
        # return res

def author_name_fun(author_name,KOL_ID,pharma_Connections,finConnections):
        #import ipdb; ipdb.set_trace()
        l_pharma = len(finConnections)
        l_fin = len(pharma_Connections)
        #final_list=pharma_Connections.extend(finConnections)
        pharma_author=[]
        pharma_author_id=[]
        count = 0
        for author_id in pharma_Connections:
            author_nm=''
            Header = {
                'Authorization': 'Bearer ' + AUTH_TOKEN,
                'content-type': 'application/json'
            }
            params = {
                 'author_ids': author_id,
            }

            url = 'url/api/v2/kols/list/'
            req = requests.get(url,params,headers=Header)
            count=0+1
            print(count)
            print("=======================================list-API===================")
            print(req)
            print("=======================================list-API===================")
            #print(json.dumps(req.json()))
            inno_data = req.json().get('data',[])
            #print(inno_data)
            for data in inno_data :
                pharma_a = data.get('nm')
                pharma_id = data.get('kolkey')
                pharma_author.append(pharma_a)
                pharma_author_id.append(pharma_id)
            if count == len(pharma_Connections):
                break
        #================================================================
        fin_author=[]
        fin_author_id=[]
        for author_id in finConnections:
            author_nm=''
            Header = {
                'Authorization': 'Bearer ' + AUTH_TOKEN,
                'content-type': 'application/json'
            }

            params = {
                 'author_ids': author_id
            }

            url = 'url/api/v2/kols/list/'
            req = requests.get(url,params,headers=Header)
            print("=======================================connection-API---2===================")
            print(req)
            print("=========================================connection-API---2===")
            #print(json.dumps(req.json()))
            inno_data = req.json().get('data',[])
            #print(inno_data)

            for data in inno_data :
                fin_a = data.get('nm')
                fin_author.append(fin_a)
                fin_id = data.get('kolkey')
                fin_author_id.append(fin_id)

        #========================================================================


        write_csv(author_name,KOL_ID,pharma_Connections,l_pharma,pharma_author,pharma_author_id,finConnections,l_fin,fin_author,fin_author_id)

def write_csv(author_name,KOL_ID,pharma_Connections,l_pharma,pharma_author,pharma_author_id,finConnections,l_fin,fin_author,fin_author_id):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author Name','Author','Pharma Connections','Length of pharma','pharma author','pharma author id','fin Connections','Length of fin','fin author','fin author id']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author Name':author_name,'Author':KOL_ID,'Pharma Connections':pharma_Connections,'Length of pharma':l_pharma,'pharma author':pharma_author,'pharma author id':pharma_author_id,'fin Connections':finConnections,'Length of fin':l_fin,'fin author':fin_author,'fin author id':fin_author_id})

# def write_csv(KOL_ID,author_nm,email_1,telephone,duplicate_flag,addresse,affiliation) :
#        file_exists = os.path.isfile(out_filename)
#        with open(out_filename, 'a') as csvfile:
#            feild_names= ['Author ID','Author Name','Emails','Phones','Duplicate_no','Addresses','Affiliation']
#            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
#            if not file_exists:
#                writer.writeheader()
#            writer.writerow({'Author ID':KOL_ID,'Author Name':author_nm,'Emails':email_1, 'Phones':telephone,'Duplicate_no':duplicate_flag,'Addresses':addresse,'Affiliation':affiliation})

if __name__ == '__main__':

    connections()
