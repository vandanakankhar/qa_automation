import pymongo
import db_config_fa
import json
import requests
import urllib
import csv
import re
import os.path
import re
import sys
import pprint

out_filename = "fa_validation.csv"
#db.regulatory_bodies_data.findOne({$and:[{"body_name":"Food and Drug Administration (FDA)"},{"classification.TA":"Dermatology"}]})
AUTH_TOKEN=""

def mongo_setup():

    uri = "mongodb://" + db_config_fa.DB_USERNAME + ":" + \
        db_config_fa.DB_PASSWORD + "@" + db_config_fa.DB_HOST + ""
    conn = pymongo.MongoClient(uri)
    database_name = db_config_fa.DB_NAME
    db = conn[database_name]
    collection_name = db_config_fa.DB_COLLECTION
    collection = db[collection_name]
    return collection

def check_mongo():

    author_dict={}

    collection = mongo_setup()

    find_data_cursor = collection.find()
    for find_data in find_data_cursor:
        # del find_data['_id']
        # print(json.dumps(find_data))
        if find_data.get('authors'):
            author_ids=find_data.get('authors')
            for nm in author_ids:
                kol_name=nm.get('kol_name')
                print("***********",kol_name)
                KOL_ID = nm.get('author_id', None)
                author_dict[kol_name] = KOL_ID


    return author_dict
#========================personal_info========================================
def personal_info(KOL_ID):

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    BASE_API = 'url'
    #response = {}

    if KOL_ID:
        url = BASE_API + KOL_ID +'/personal_info'
        req = requests.get(url,headers=Header)
        print(req.status_code,KOL_ID)
        #print(json.dumps(req.json()))
        if req.status_code == 200:
            innoData = req.json()['data'].get('innoData', None)

            current_email = innoData.get('emails')
            list_email = []
            for em in current_email:
                email=em.get('email',[])
                if email:
                    list_email.append(email)
            #print(list_email)
            current_phone= innoData.get('phone_numbers')
            list_phone=[]
            for pho in current_phone:

                phone_number=pho.get('phone')
                if phone_number:
                    phone_number=re.sub(r'[^\w]', ' ', phone_number)
                    phone="".join(filter(lambda x: x.isdigit(),phone_number))
                    list_phone.append(phone)
            #print(list_phone)

            current_address= innoData.get('addresses')
            list_address=[]
            for addr in current_address:
                address=addr.get('address')
                if address:
                    list_address.append(address)
            #print(list_address)
            return list_email,list_phone,list_address
        else:
            return [], [], []
#=================profile=======================================================
def profile(KOL_ID):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }

    BASE_API = 'url'

    if KOL_ID:
        url = BASE_API + KOL_ID + '/basic_info'
        req = requests.get(url,headers=Header)
        print(req)
        print(req.status_code)
        if req.status_code == 200:
            innoAffi = req.json()
            #print(innoAffi)
            Name = innoAffi.get('nm')
            desig=innoAffi.get('designation')
            # designation = ''
            # if desig:
            #     designation=re.sub(r'[^a-z]', '', desig.lower())
            affi=innoAffi.get('aff')
            kol_author_id = innoAffi.get('kolkey')
            country=innoAffi.get('macnty')
            kol_edu=innoAffi.get('education')
            t=innoAffi.get('title')


            return Name,desig,affi,kol_author_id,country,kol_edu,t
        else:
            return ' ','','','','','',''

def research_activity(KOL_ID):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }

    BASE_API = 'url'
    if KOL_ID:
        url = BASE_API + KOL_ID + '/research_activity'
        req = requests.get(url,headers=Header)
        #print(req)
        print(req.status_code)

        #print(json.dumps(req.json()))
        if req.status_code == 200:
            innoData = req.json()['data']
            for data in innoData:
                #kol_edu=innoAffi.get('education')
                if data.get('name') == 'Pharma Watch':
                    #import ipdb; ipdb.set_trace()
                    key_company_name=data.get('docs')
                    company_list=[]
                    for company in key_company_name:
                        company_name=company.get('key_company_name')
                        company_list.append(company_name)

                    print(company_list)


            return company_list
        else:
            return ' ','','','','','',''

def read_csv(input_file):

    author_dict = check_mongo()
    print(author_dict.keys())
    # list_ids=check_mongo()
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0
        for line in reader  :
            count = count + 1
            print(count)
            # if count <= 219:
            #     continue
            kol_name = line.get('Name of Analyst')
            print(kol_name)
            kol_designation = line.get('Designation').split('@@@')

            kol_email = line.get('Email Address').split('@@@')

            kol_phone_list = []
            phone_str = line.get('Phone/Fax').split('@@@')
            for phone_number in phone_str:
                phone_number=re.sub(r'[^\w]', ' ', phone_number)
                kol_phone="".join(filter(lambda x: x.isdigit(),phone_number))
                kol_phone_list.append(kol_phone)
            kol_a = line.get('Address').split('@@@')
            for k_a in kol_a:
                kol_add = re.sub(r'[^\w]',' ',k_a)
                kol_address=re.sub(r'[^a-z]', '', kol_add.lower())
            print(kol_address)

            kol_affiliation = line.get('Name of Firm').split('@@@')
            kol_country = line.get('Firm Country')
            kol_company_name = line.get('Key Companies that the Analyst is monitoring').split('@@@')
            #Financial_T=line.get('')


            print(author_dict.get(kol_name))
            if author_dict.get(kol_name,None):

                list_email,list_phone,list_address=personal_info(author_dict[kol_name])

                Name,desig,affi,kol_author_id,country,kol_edu,t=profile(author_dict[kol_name])
                company_name=research_activity(author_dict[kol_name])

                if list_email or kol_email:
                    email_result=compare_email(list_email,kol_email)
                #list_phone = []
                if list_phone or kol_phone_list:
                    phone_result=compare_phone(list_phone,kol_phone_list)
                #list_address = []

                if list_address or kol_address:
                    address_result=compare_address(list_address,kol_address)

                else:
                    address_result = ''

                if country or kol_country:
                    country_result=compare_country(country,kol_country)

                if affi or kol_affiliation:
                    affiliation_result=compare_affiliation(affi,kol_affiliation)

                if desig or kol_designation:
                    designation_result=compare_designation(desig,kol_designation)
                if company_name or kol_company_name:
                    company_result=compare_company(company_name,kol_company_name)

                write_csv(kol_author_id,Name,list_email,kol_email,email_result,list_phone,kol_phone_list,phone_result,list_address,kol_a,address_result,country,kol_country,country_result,desig,kol_designation,designation_result,affi,kol_affiliation,affiliation_result,company_name,kol_company_name,company_result)

def compare_email(list_email,kol_email):



    count = 0
    if kol_email:

        for ema in kol_email:
            for emai in list_email:
                if ema == emai:
                    count+=1
        if count == len(kol_email) or ema == 'NA':
            status_flag = 'Matching'
        else:
            status_flag = 'Not Matching'
        return status_flag

def compare_phone(list_phone,kol_phone_list):
    count = 0
    for pho in kol_phone_list:
        for ph in list_phone:
            if pho in ph:
                count+=1

    if count == len(kol_phone_list) or pho == '':
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag

def compare_address(list_address,kol_address):

    count = 0
    if list_address:
        for a in list_address:
            kol_addr = re.sub(r'[^\w]',' ',a)
            kol_ad=re.sub(r'[^a-z]', '', kol_addr.lower())
            print(kol_address,"=====", kol_ad, "===========================", a)
            if kol_address in kol_ad or kol_address == 'na':
                status_flag = 'Matching'
            else:
                status_flag = 'Not Matching'
    else:
        if kol_address == 'na':
            status_flag = 'Matching'
        else:
            status_flag = 'Not Matching'
    return status_flag


def compare_company(company_list,key_company_name):
    count = 0
    if key_company_name:
        for com in key_company_name:
            t_kol = re.sub(r'[^\w=]',' ',com)
            company=re.sub(r'[^a-z]', '', t_kol.lower())
            for emai in company_list:
                em = re.sub(r'[^\w=]',' ',emai)
                emai=re.sub(r'[^a-z]', '', em.lower())
                if company == emai:
                    count+=1

        if count == len(key_company_name) or company == 'na':
            status_flag = 'Matching'
        else:
            status_flag = 'Not Matching'
        return status_flag

def compare_country(country,kol_country):
    status_flag = 'Matching'

    if country and kol_country:
        if country.lower() == kol_country.lower():
            status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag
def compare_designation(desig,kol_designation):
    designation = ''
    if desig:
        designatio=re.sub(r'[^a-z]', '', desig.lower())
        designation=re.sub(r'[^a-z]', '', designatio.lower())
    if kol_designation:
        for a in kol_designation:
            kol_addr = re.sub(r'[^\w]',' ',a)
            kol_ad=re.sub(r'[^a-z]', '', kol_addr.lower())
            status_flag = 'Not Matching'
            if designation == kol_ad or kol_ad == 'na':
                status_flag = 'Matching'
                break

    return status_flag

def compare_affiliation(affi,kol_affiliation):
    affiliation = ''
    if affi:
        aff=re.sub(r'[^a-z]', '', affi.lower())
        affiliation=re.sub(r'[^a-z]', '', aff.lower())
    if kol_affiliation:
        for aff in kol_affiliation:
            kol_aff = re.sub(r'[^\w]',' ',aff)
            kol_affi=re.sub(r'[^a-z]', '', kol_aff.lower())
            status_flag = 'Not Matching'
            if kol_affi == affiliation or kol_affi == 'na':
                status_flag = 'Matching'
                break

    return status_flag
    # status_flag = 'Matching'
    # affiliation = ''
    # if affi:
    #     affiliation=re.sub(r'[^a-z]', '', affi.lower())
    #
    # if affiliation in kol_affiliation:
    #     status_flag = 'Matching'
    # else:
    #     status_flag = 'Not Matching'
    # return status_flag
    #
def write_csv(kol_author_id,Name,list_email,kol_email,email_result,list_phone,kol_phone_list,phone_result,list_address,kol_a,address_result,country,kol_country,country_result,desig,kol_designation,designation_result,affi,kol_affiliation,affiliation_result,company,kol_company_name,company_result):
        file_exists=os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            fieldnames = ['Author ID','Kol_name', 'Portal_Email', 'Email', 'Email Flag',\
                            'Portal Phone', 'Phone','Phone Flag','Portal Address','Address','Address flag','Portal Country','Country','Country Flag','Portal Designation','Designation','De Flag','Portal Firm Name','Firm Name','Firm Flag','Portal Company','Company','Company flag']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author ID':kol_author_id,'Kol_name':Name, 'Portal_Email':list_email, 'Email':kol_email,
                            'Email Flag':email_result,'Portal Phone':list_phone, 'Phone':kol_phone_list,'Phone Flag':phone_result,'Portal Address':list_address,'Address':kol_a,'Address flag': address_result,'Portal Country':country,'Country':kol_country,'Country Flag':country_result,'Portal Designation':desig,'Designation':kol_designation,'De Flag': designation_result,'Portal Firm Name':affi,'Firm Name':kol_affiliation,'Firm Flag':affiliation_result,'Portal Company':company,'Company':kol_company_name,'Company flag':company_result})

if __name__ == '__main__':
    read_csv("Merck_FA.csv")



    # kol_title,kol_education,kol_name,kol_designation,kol_affiliation,kol_email,kol_phone,kol_country,kol_address=read_csv("Dermatology_Instance_Regulatory_Body.csv")
    # list_ids=check_mongo()
    # list_email,list_phone,list_address=personal_info(list_ids)
    # Name,designation,affiliation,kol_author_id,country,education,title=profile(list_ids)
    #
    # write_csv(kol_author_id,author,kol_name,list_email,kol_email,list_phone,kol_phone,list_address,kol_address,country,kol_country,designation,kol_designation,affiliation,kol_affiliation,education,kol_education,title,kol_title)
    # def compare_email(list_email,kol_email):
    #
    #     for em in list_email:
    #         email = em.lower()
    #         print(email)
    #     kol_email= kol_email.lower()
    #     print(kol_email)
    #
    #
    #         if(email==kol_email):
    #             print("email Matched")
    #         else:
    #             print("email not matched")
