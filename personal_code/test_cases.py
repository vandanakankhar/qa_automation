import random
TEST_EMAILS = [

    'email1111'+ str(random.randint(1, 100)) +'@application.com',
    'email112'+ str(random.randint(1, 100)) +'@application.com',
    'email11113'+ str(random.randint(1, 100)) +'@application.com',
    'email1114'+ str(random.randint(1, 100)) +'@application.com'


]
TEST_PHONES = [

    '001012334111'+ str(random.randint(1, 100)) +'',
    '1234567'+ str(random.randint(1, 100)) +'',
    '03303333'+ str(random.randint(1, 100)) +'',
    '04404444'+ str(random.randint(1, 100)) +'',
]
TEST_ADDRESSES = [
    #
       '' + str(random.randint(1, 100)) + 'FIRST Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'SECOND Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'THIRD Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'FOURTH Merck_ADDRESS,Pune, Manhattan',
]
TEST_CASES = [
    {
        'test_case_name': 'inno_add_kol_1',
        'test_case_descripation': 'To verify Data added by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be blank',

        'test_case_senario': {
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
            
        },

        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[0],
                    "timestamp": None,
                    "isNew": False

                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS[0]],
            "phone_numbers": [TEST_PHONES[0]],
            "addresses": [TEST_ADDRESSES[0]],
        }
    },
    {
        'test_case_name': 'inno_add_kol_2',
        'test_case_descripation': 'To verify Data ADDED by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be Present.',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[0],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[0],
                    "timestamp": None,
                    "isNew": False
                },
                {
                    "source": [],
                    "email": TEST_EMAILS[1],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[0],
                    "source": [],
                    "isNew": False
                },
                {
                    "phone": TEST_PHONES[1],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[0],
                    "source": [],
                    "isNew": False
                },
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[1],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [TEST_EMAILS[0]],
            "phone_numbers": [TEST_PHONES[0]],
            "addresses": [TEST_ADDRESSES[0]],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS[0], TEST_EMAILS[1]],
            "phone_numbers": [TEST_PHONES[0], TEST_PHONES[1]],
            "addresses": [TEST_ADDRESSES[0], TEST_ADDRESSES[1]],
        }
    },
    {
        'test_case_name': 'inno_delete_kol_3',
        'test_case_descripation': 'To verify Data Deleted by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be Present',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[2],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[2],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[2],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
            "history": True
        },
        'test_case_history':{
            "emails": [TEST_EMAILS[2]],
            "phone_numbers": [TEST_PHONES[2]],
            "addresses": [TEST_ADDRESSES[2]],
        },
        'test_case_current_collection':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        }
    },
    {
        'test_case_name': 'inno_edit_kol_4',
        'test_case_descripation': 'To verify Data Edited by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be blank',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[0],
                    "timestamp": None,
                    "isNew": False,
                    'id':0
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[0],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[0],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS[1],
                    "timestamp": None,
                    "isNew": False,
                    'id':0
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES[1],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES[1],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS[1]],
            "phone_numbers": [TEST_PHONES[1]],
            "addresses": [TEST_ADDRESSES[1]],
        }
    }
]
