import requests
from accounts import User_details, User_details
import json
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
import time
# global_client = elastic_Search_ip)
# global_production = elastisearch_ip
import re
import sys
import pprint
KOL_ID="60aa0668880a4e43b200ae63456fb16e"
list_staging=[]
list_pro=[]
Application's_url
pags = ''
#AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mjc1ODMyMTIsInNlc3Npb25fa2V5IjoiZTQwNDhhYjMtMWMzOC00ZTYzLWE4M2YtZTE5Y2FjYzFlMzc5IiwiZXhwIjoxNTI3NjY5NjEyfQ.AzJx7t3tipVjSmaDrt5N2AX83Cu0-aDjtlLud3qbUiU"
filename="output.csv"
list_filename=filename.split('.')
now = time.strftime('%d-%m-%Y %H:%M:%S')
out_filename=list_filename[0]+'.'+list_filename[1]
print(out_filename)
def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    print(r.status_code)

    # pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):

    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get('application_callback_url', params=payload)
    print(req.status_code)

    accesstoken_dict = req.json()
    # print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)

def read_csv():
    get_auth_token(User_details)
    input_file = "Demo.csv"
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0

        for line in reader  :
            TA=line.get('TA')
            as_class=line.get('Asset class')
            print(as_class)
            list_indications=line.get('Indication')
            #country=line.get('Country')
            filter=line.get('Filter')
            filter_values = line.get('Filter Name')
            test_name = line.get('Test Name')
            count=count + 1
            # if count<=531:
            #     continue

            filter_operation(TA,as_class,list_indications,filter,filter_values,test_name)


def filter_operation(TA,as_class,list_indications,filter,filter_values,test_name):

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    url = 'url'

    payload ={"from":0,"limit":10,"TA":TA,"asset_class":as_class,"filters":{"kol_info":[{"name":as_class,"value":100,"showDate":False,"startDate":0,"endDate":0}],"showScore":False,"kolType":"allkols","Indications":{"values":[list_indications],"isAnd":False},filter:{"values":[filter_values],"isAnd":False},"Test Name":{"values":[test_name],"isAnd":False}}}

    req = requests.post(url,data=json.dumps(payload),headers=Header)
    #import ipdb; ipdb.set_trace()
    print("=========================================================================")
    print(json.dumps(payload))
    print("=========================================================================")
    print("status : ", req.status_code)
    print("Fetched Top 200 KOLs : ",req)
    print("=========================================================================")
    #print(Header)
    print("=========================================================================")
    print(json.dumps(req.json()))
    #import ipdb; ipdb.set_trace()
    print("=======================================================================")
    #print(payload)
    print("=========================================================================")
    print(req.json())
    inno_data = req.json().get('data',[])
    author_id=[]
    for sc in inno_data:
        score = sc.get('scores')
        for author in score:
            author_ids=author.get('author_id')
            author_id.append(author_ids)
        list_author_id=len(author_id)

        print(filter_values,"===================================",list_author_id)

        csv_write(TA,list_indications,as_class,filter,filter_values,test_name,list_author_id,author_id)

def csv_write(TA,list_indications,as_class,filter,filter_values,test_name,list_author_id,author_id):
    file_exists = os.path.isfile(out_filename)
    with open(out_filename, 'a') as csvfile:
        feild_names= ['TA','Indication','Asset Class','Filter','Filter Values','Test Name','Count','Authors']
        writer = csv.DictWriter(csvfile, fieldnames=feild_names)
        if not file_exists:
            writer.writeheader()
        writer.writerow({'TA':TA,'Indication':list_indications,'Asset Class':as_class,'Filter':filter,'Filter Values':filter_values,'Test Name':test_name,'Count':list_author_id,'Authors':author_id})

if __name__ == '__main__':
    #import ipdb; ipdb.set_trace()
    read_csv()
