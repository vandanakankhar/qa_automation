
import csv
import re

from elasticsearch import Elasticsearch


#----------------------------------------------------------#

def build_query(author_id):
    #netsed mapping
    es_query = {
        "sort": {
            "created_at": "desc"
        },
        "query": {
            "bool": {
                "must": [{
                    "nested": {
                        "path": "authors",
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "terms": {
                                            "authors.author_id": author_id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
                ]
            }
        },
        "_source": [
            "abstract",
            "public_title",
            "clinical_id",
            "source_url",
            "created_at"
        ]
    }
    return es_query


synonyms = ['Decreased Sperm Motility [PE],Azoospermia', 'Laboratory finding of Oligozoospermia', 'Congenital absence of germinal epithelium of testes', 'Teratozoospermia,inability to impregnate', 'Oligospermia',
            'Male sterility,Subfertility', ' Male', 'Male infertility']


def get_string_id(string):
    return re.sub(r'[^a-z0-9]', '', string)

def synonyms_match(docs):
    source = docs.get('_source', {})
    public_title = source.get('public_title', '')
    abstract = source.get('abstract', '')
    text = get_string_id(public_title+abstract)

    flag = True
    for i in synonyms:
        i_id = get_string_id(i)
        if i_id in text:
            print("Synonyms Matched", True)
            flag = False
            break
        # else:
            # print("No Synonyms Matched", False)

    if flag:
        print("no match")
    else:
        print("match")
# if synonyms in abstract:
 #       print("Abstract",True)
 #   else:
 #       print("Abstract",False)


def csv_reader(input_file):
    index_name = "clinical_trials_v5"
    es_client =IP
    with open(input_file, 'r')as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            author_id = line.get('author_id')
            body = build_query([author_id])
            response = es_client.search(
                index=index_name, body=body, request_timeout=60)
            if len(response['hits']['hits']) > 0:
                pass
                # print(response['hits']['hits'][0]['_source'][0]['clinical_id'])
            else:
                print("No Data")

            for docs in response['hits']['hits']:
                result = synonyms_match(docs)


if __name__ == "__main__":
    csv_reader("author_id.csv")
