from __future__ import print_function

import json
import os
import pprint
import sys
import time
import urllib
from collections import OrderedDict

import requests
from pymongo import DESCENDING, MongoClient

from db import get_collections_history

TEST_CASE_NUMBER = "inno_add_kol_2"
test_case_results = OrderedDict()
test_case_results['test_case_name'] = TEST_CASE_NUMBER
KOL_id = ID
test_case_results['kol_used'] = KOL_id
Email = "email1@innoaddkol.com"
Email_2 = "email2@innoaddkol.com"
Phone = "11111111111"
Phone_2 = "2222222222"
Address = "FIRST ADDRESS,New York, Manhattan"
Address_2="SECOND ADDRESS,Pune,India"
ACCESS_EMAIL = "riyaz.bhanvadia@application.com"
AUTH_TOKEN = ""
enviroment_email = []
enviroment_phone = []
enviroment_addres = []

senario_body = {
    "emails": [{
        "source": [],
        "email": Email,
        "timestamp": None,
        "isNew": False
    }],
    "phone_numbers": [{
        "phone": Phone,
        "source": [],

        "isNew": False
    }],
    "addresses": [ {
            "timestamp": None,
            "address": Address,
            "source": [],
            "isNew": False
        }],
    "history": True
}
test_case_body = {
    "emails": [
        {
            "source": [],
            "email": Email_2,
            "timestamp": None,
            "isNew": False
        },

    ],
    "phone_numbers": [{
        "phone": Phone_2,
        "source": [],

        "isNew": False
    }],
    "addresses": [{
            "timestamp": None,
            "address": Address_2,
            "source": [],
            "isNew": False
        }],
    "history": True
}


def get_auth_token():
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api()


def auth_login():

    body = {
        "email": ACCESS_EMAIL,
        "password": "Welcome@3"
    }

    r = requests.post('Url_of_application', json=body)
    # print(r.status_code)
    # pprint.pprint(r.json())
    dict = r.json()
    code = dict['responseData']['callback_url']
    codes = code[34:]
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api():
    global accesstoken
    payload = {
        "code": auth_login(),
        "app":"name_of_app"
    }
    req = requests.get(
        URL, params=payload)
    accesstoken_dict = req.json()
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    # print("Granted access token:", accesstoken[10:])
    return accesstoken


def execute_test_case_add_kol_detail(body):

    print('-' * 3, "Adding second Email, second phone, second address", '-' * 3)
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_id,
        'dc': 'as'
    }

    body = json.dumps(body)

    req = requests.put('url/api/v1/kols/60aa0668880a4e43b200ae63456fb16e',
                       params=params, data=body, headers=Header)
    print("--Added successfully--- Put status : ", req.status_code)


def create_environment_test_case_add_kol_detail(body):

    print('-' * 3, "Email,phone and address are present in History", '-' * 3)
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_id,
        'dc': 'as'
    }

    body = json.dumps(body)

    req = requests.put('url/api/v1/kols/60aa0668880a4e43b200ae63456fb16e',
                       params=params, data=body, headers=Header)
    print("Scenario status : ", req.status_code)
    test_case_results['create_environment'] = "success"


def check_scinario_in_current_collection():
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    req = requests.get(
        'url/api/v1/kols/60aa0668880a4e43b200ae63456fb16e', headers=Header)
   #print(req.status_code, req.text)
    print("---Checking data in current collection---:")
    if req.status_code == 200:
        innoData = req.json()['data'].get('innoData', None)
        if innoData:
            innoAssociations = innoData['innoAssociations']
            #print("Checking:", "Last Updated")
            lastUpdated = innoAssociations.get('lastUpdatedBy')
            print("Last updated by:", lastUpdated['email'])
            if lastUpdated and lastUpdated['email'] == ACCESS_EMAIL:
                print("Last Updated check:", "Pass")
                test_case_results['last_updated_by'] = ACCESS_EMAIL
            else:
                print("Last Updated check:", "Fail")
#####################Add Emails############################################################
            emails = innoAssociations.get('emails')
            for number, email in enumerate(emails):
                print("Checking email:", email['email'])
                if email['email'] == Email_2:
                    print("Current Email check:", number + 1, "Pass")
                    test_case_results['current_email_check_' +
                                      str(number)] = "pass"
                else:
                    print("Current Email check:", number + 1, "Fail")
                    test_case_results['current_email_check_' +
                                      str(number)] = "fail"
######################Add Phones##########################################################
            phones = innoAssociations.get('phone_numbers')
            for number, phone in enumerate(phones):
                print("Checking Phone Number:", phone['phone'])
                if phone['phone'] == Phone_2:
                    print("Current Phone Number check:", number + 1, "Pass")
                    test_case_results['current_phone_check' +
                                      str(number)] = "pass"

                else:
                    print("Current Phone Number check:", "Fail")
                    test_case_results['current_phone_check' +
                                      str(number)] = "fail"
########################Add Addresses####################################################
            address = innoAssociations.get('addresses')
            for address in address:
                print("Checking Address:", address['address'])
                if address['address']== Address_2:
                    print("Current address check:", "Pass")
                    test_case_results['current_address_check'] = "pass"

                else:
                    print("Current address check:", "Fail")
                    test_case_results['current_address_check'] = "fail"

        else:
            print("GET test case failed:", "No data in current collection.")
    else:
        print(req.status_code, req.text)


def check_history_collection_in_mongo():
    print('-' * 10, 'Checking History', '-' * 10)
    collection = get_collections_history()
    find_last_history = collection.find({'author_id': KOL_id}).sort(
        'timestamp', DESCENDING).limit(1)
    history_data = find_last_history[0]

    emails = history_data.get('emails')
    for number, email in enumerate(emails):
        print("Checking history email:", email['email'])
        if email['email'] == Email:
            print("History Email check:", number + 1, "Pass")
            test_case_results['history_email_check_' +
                              str(number + 1)] = "pass"
        else:
            print("History Email check:", number + 1, "Fail")
            test_case_results['history_email_check_' +
                              str(number + 1)] = "fail"
    phones = history_data.get('phone_numbers')
    for number, phone in enumerate(phones):
        print("Checking history for phone:", phone['phone'])

        if phone['phone'] == Phone:
            print("history phone:", number + 1, "Pass")
            test_case_results['history_phone_check_' +
                              str(number + 1)] = "pass"

        else:
            print("history phone:", number + 1, "Fail")
            test_case_results['history_phone_check_' +
                              str(number + 1)] = "fail"


    address = history_data.get('addresses')
    for address in address:
        print("checking history address:", address['address'])
        if address['address']==Address:
            print("History address check",str(number+1),"pass")
        test_case_results['history_address_check'+ str(number+ 1)] = "pass"

    else:
        print("history address:", "Fail")
        test_case_results['history_address_check'] = "fail"


def make_json_document():
    with open('' + TEST_CASE_NUMBER + '.json', 'w') as outfile:
        json.dump(test_case_results, outfile)


def first_test_case_scenario():
    # First scenario for KOL with PUT.

    get_auth_token()
    print("Login Completed!!!!")
    create_environment_test_case_add_kol_detail(senario_body)
    execute_test_case_add_kol_detail(test_case_body)
    # To check responce for KOL with GET.
    # time.sleep(1)
    check_scinario_in_current_collection()
    check_history_collection_in_mongo()
    make_json_document()


if __name__ == "__main__":
    first_test_case_scenario()
