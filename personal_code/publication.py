#import ipdb; ipdb.set_trace()
import csv
import re
import os.path
out_filename = 'output.csv'


from elasticsearch import Elasticsearch


#----------------------------------------------------------#

def build_query(author_id):

 es_query={

    "sort": {
        "created_at": "desc"
    },
    "query": {
        "bool": {
            "must": [
                {
                    "terms": {
                        "authors.author_id": author_id
                    }
                }
            ]
        }
    },
    "_source": [
        "article_title",
        "journal_title",
        "created_at",
        "source_url",
        "abstract",
        "publication_id",
        "impact_factor",
        "authors"
    ]
}
 return es_query
   
# synonyms=['Oligospermia']
def get_string_id(string):
    return re.sub(r'[^a-z0-9]', '', string)

def synonym_match_ct(docs,author_id):
    affiliation=''
    source = docs.get('_source', {})

    authors = source.get('authors',[])
    for author in authors :
        es_author_id=author.get('author_id')
        if es_author_id == author_id:
            es_author_name = author.get('author_name')

            es_affiliation_list = author.get('author_affiliation',[])
            for es_affiliation in es_affiliation_list :
                if es_affiliation :
                  affiliation = es_affiliation
                else :
                    affiliation = 'No affiliation'
                

            
    public_title = source.get('public_title', '')
    abstract = source.get('abstract', '')
    publication_id = source.get('publication_id','')
    article_title=source.get('article_title','')
    
    text = get_string_id(public_title + abstract)#Data from ES
    

    flag = True
    synonym_list = csv_reader_synonym('synonyms.csv')
    for synonym in synonym_list:
        i_id = get_string_id(synonym)
        #import ipdb; ipdb.set_trace() 
        #text = 'prashant p patil'
        if i_id in text:
            print("-------Synonym Matched------", True)
            flag = False
            break
        # else:
            # print("No Synonyms Matched", False)

    if flag:
        print("No Match "+author_id+'  '+publication_id+'  '+es_author_name+'  '+affiliation)
        write_csv(author_id,publication_id,es_author_name,affiliation)
        
    else:
        print("--------Match-----------")
    
def write_csv(author_id,publication_id,name,affiliation):
       file_exists=os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author_Name','Author_id','publication_id','affiliation']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author_Name':name,'Author_id':author_id, 'publication_id':publication_id,\
                               'affiliation':affiliation})
# if synonyms in abstract:
 #       print("Abstract",True)
 #   else:
 #       print("Abstract",False)


def csv_reader_synonym(file_input):
    with open(file_input, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        list_synonym=[]#New list to store synonyms from csv
        #import ipdb; ipdb.set_trace()
        for line in reader:
            synonym = line.get('Synonyms')
            #print("vandanana......." + synonyms)
            list_synonym.append(synonym)
        return list_synonym


def csv_reader(input_file):
    index_name = "publications_v6"
    es_client =IP
    #declare array here fo synonmys
    with open(input_file, 'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0
        for line in reader:
            author_id = line.get('author_id')
            body = build_query([author_id])  # Write query
            response = es_client.search(
                index=index_name, body=body, request_timeout=60)  # pass query response in response
            if len(response['hits']['hits']) > 0:
                # pass
                for docs in response['hits']['hits']:
                    result = synonym_match_ct(docs,author_id)  # pass response in method
                # print(response['hits']['hits'][0]['_source'][0]['clinical_id'])
            else:
                print("No Data for author")
                #print(author_id)
                f.write("This is %s\r\n" % author_id)
            count=count+1
            #print(count)


f = open("publications.txt", "w+")


if __name__ == "__main__":
    csv_reader("author_id.csv")
#write_csv("output.csv")
    




