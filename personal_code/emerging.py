import requests
import json
import pprint
import csv
import os.path
import pprint
out_filename ="top20_Neurology.csv"
out_filename_1 ="top20_Oncology.csv"
out_filename_2="top20_Cardiology.csv"
out_filename_3 ="top20_Dermatology.csv"
out_filename_4 ="top20_Reproductive_Health.csv"
out_filename_5 ="top20_Metabolic_Diseases.csv"
out_filename_6 ="top20_Endocrinology.csv"
from accounts import User_details
global count
count = 0
AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjMzNDI3NzEsInNlc3Npb25fa2V5IjoiZjY5MTA0YTctZmIxYS00N2U0LThiZmItZjFjNmRiODlmMmFkIiwiZXhwIjoxNTIzNDI5MTcxfQ.mPXKago95QOLc7e185ujXQ8mdOAchvRLTivatNthIh4"
# def auth_login(account):
#     r = requests.post('Url_of_application', json=account)
#     #print(r.status_code)
#     #pprint.pprint(r.json())
#     dict = r.json()
#     #print(dict)
#     code = dict['responseData']['callback_url']
#     #print(code)
#     codes = code[34:]
#     #print(codes)
#     #print("Granted login codes :", codes)
#     return codes
# def auth_callback_api(account):
#     global accesstoken
#     payload = {
#         "code": auth_login(account),
#         "app":"name_of_app"
#     }
#     req = requests.get(
#         'application_callback_url', params=payload)
#     #print(r.status_code)
#     accesstoken_dict = req.json()
#     #print(accesstoken_dict)
#     # print(accesstoken_dict['accessToken'])
#     accesstoken = accesstoken_dict['accessToken']
#     #print(accesstoken)
#     #print("Granted access for staging")
#     return accesstoken
# # to make Auth token generic
# def get_auth_token(account):
#     global AUTH_TOKEN
#     AUTH_TOKEN = auth_callback_api(account)
#
#     return AUTH_TOKEN

def get_input(count, as_class, TA, list_indications, country=[]):
    #get_auth_token(User_details)
    print(as_class, TA, list_indications, country)

    url = 'https://facelift.kplexus.net/api/v2/kols/score/'
    payload = {}
    if as_class == "Top KOLs":
        payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [{"name": "Publications", "value": 15, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Clinical Trials", "value": 13, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Congresses", "value": 12, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Guidelines", "value": 10, "showDate": True, "startDate": 0, "endDate": 0}, {
            "name": "Regulatory Bodies", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "HTA", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Societies", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Advocacy", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Top Hospital KOLs", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": True, "kolType": "allkols"}}
    else:
        payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [
            {"name": as_class, "value": 100, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": False, "kolType": "allkols", "Countries": {"values": country, "isAnd": False}}}

    headers = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    #print(json.dumps(payload))
    r = requests.post(url, data=json.dumps(payload), headers=headers)

    r = r.json()
    #pprint.pprint(r)
    for i in r['data'][0]['scores']:

        count+= 1
        print(count)
        #if count <= 15:
            #continue
        #author_id=i['author_id']
        print(i['author_id'],list_indications,country,as_class)
        #writer.writerow({'AUTHOR_ID' : i['author_id'], "TA" : TA, "INDICATION" : list_indications, "COUNTRY" : country, "RESOUCES" : as_class})

        write_csv(i['author_id'],TA,list_indications,country,as_class)
        if count == 200:
            break
def write_csv(author_id,TA, list_indications, country, as_class) :
    if TA == "Neurology":
        file_exists = os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})

    elif TA == "Oncology":
        file_exists = os.path.isfile(out_filename_1)
        with open(out_filename_1, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Cardiology":
        file_exists = os.path.isfile(out_filename_2)
        with open(out_filename_2, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})

    elif TA == "Dermatology":
        file_exists = os.path.isfile(out_filename_3)
        with open(out_filename_3, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Reproductive Health":
        file_exists = os.path.isfile(out_filename_4)
        with open(out_filename_4, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Metabolic Diseases":
        file_exists = os.path.isfile(out_filename_5)
        with open(out_filename_5, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Endocrinology":
        file_exists = os.path.isfile(out_filename_6)
        with open(out_filename_6, 'a') as csvfile:
            feild_names= ['AUTHOR_ID','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'AUTHOR_ID':author_id,'TA':TA,'INDICATION':list_indications,              'COUNTRY':country,'RESOUCES':as_class})
if __name__ == '__main__':

    TA= ["Neurology"]
    #TA = ["Neurology","Oncology","Cardiology","Dermatology","Reproductive Health","Metabolic Diseases","Endocrinology"]
    #,"Alzheimer","Parkinson"
    INDICATION_DCT = {"Neurology": ["Multiple Sclerosis"], "Oncology": ["Breast cancer","Acute Lymphoblastic Leukemia"],"Cardiology": ["Myocardial Infarction"],"Dermatology": ["Dermatitis", "Psoriasis"],"Reproductive Health": ["Male Infertility","Female Infertility","Ovarian Disease","Assisted Reproduction"],"Metabolic Diseases":["Diabetes"],"Endocrinology": ["Thyroid Disorders", "Obesity"]}

    LIST_ASSET_CLASS = ["Top KOLs","Publications", "Clinical Trials", "Congresses","Societies", "Top Hospital KOLs"]
    #Top Kols, Ct, Publication, congresses, society and hospitals
    #"Italy","Canada","United Kingdom","Spain","France",

    COUNTRY_DCT ={
                "Multiple Sclerosis": ["Germany","United States of America"],
                "Alzheimer":["Germany"],
                "Parkinon":["Germany"],
                "Breast cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Acute Lymphoblastic Leukemia":["Germany","United States of America"],
                "Dermatitis":["France","Germany","United States Of America"],
                "Psoriasis":["France","Germany","United States Of America"],
                "Myocardial Infarction":["Germany","Denmark","Switzerland","France","United States of America","United Kingdom","France","Spain","Japan","Australia"],
                "Male Infertility":["Switzerland","Spain","Germany","Italy"],
                "Female Infertility":["Switzerland","Spain","Germany","Italy"],
                "Ovarian Disease":["Switzerland","Spain","Germany","Italy"],
                "Assisted Reproduction":["Switzerland","United States of America","Germany"],
                "Diabetes":["United States Of America"],
                "Thyroid Disorders":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Obesity":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"]}

    count = 0
    for ta_name in TA:
        for resource in LIST_ASSET_CLASS:
            for indication in INDICATION_DCT[ta_name]:
                for country in COUNTRY_DCT[indication]:
                    get_input(count, resource, ta_name, [indication],[country])
                    print(count,ta_name,[indication],resource,[country])

# if __name__ == '__main__':
#     country(TA,INDICATION_DCT,LIST_ASSET_CLASS)
#
#
#


    # LIST_ASSET_CLASS = ["Top KOLs", "Publications"]
    # # list_country=["Costa Rica", "Chile", "Panama", "Ecuador", "Mexico", "Peru", "Argentina", "Brazil", "Columbia", ]
    # TA = ["Neurology"]
    # INDICATION_DCT = {"Neurology": ["Multiple Sclerosis","Alzheimer","Parkinson"]
    #                     ""}
    # COUNTRY_DCT = {"Neurology": ["Panama", "Germany"]}
    # author_ids = set()

    # for as_class in list_asset_class:
    #     list_authors_as_class=get_input(as_class,"Neurology",["Multiple Sclerosis"])
    #     for ids in list_authors_as_class:
    #         print author_ids
    #         author_ids.add(ids)

    # for ta_name in INDICATION_DCT:
    #     for resource in LIST_ASSET_CLASS:
    #         for indication in INDICATION_DCT[ta_name]:
    #             get_input(count, resource, ta_name, [indication])
    #             count = 0



    # for as_class in list_asset_class:
    #     for countries in list_country:
    #         list_authors_as_class = get_input(
    #             as_class, "Endocrinology", ["Obesity"], [countries])
    #         for ids in list_authors_as_class:
    #             author_ids.add(ids)
