from __future__ import print_function


import json
import pprint
import time

import requests
from pymongo import DESCENDING

from accounts import User_details, User_details
from db import get_collections_history, get_collections_history_Merck
from test_cases import (TEST_ADDRESSES, TEST_CASES, TEST_EMAILS, TEST_PHONES)
from test_cases_merck import (TEST_ADDRESSES_MERCK, TEST_CASES_MERCK,
                              TEST_EMAILS_MERCK, TEST_PHONES_MERCK)

#---------------------------------------------------------
#                     1.  LOGIN LOGIC
#---------------------------------------------------------
AUTH_TOKEN = ""
KOL_id = ID
KOL_ID_2 = "60aa0668880a4e43b200ae63456fb16e"
f = open("output.txt", "w+")


def auth_login(account):

    r = requests.post('Url_of_application', json=account)
    # print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    code = dict['responseData']['callback_url']
    codes = code[34:]
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        URL, params=payload)
    accesstoken_dict = req.json()
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    # print("Granted access token:", accesstoken[10:])
    return accesstoken
# to make Auth token generic


def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(account)

#---------------------------------------------------------
#          2.Environment for Preconditions
#---------------------------------------------------------
# Create Environment for preconditions


def run_condition(body, condition_type):
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }

    body = json.dumps(body)

    req = requests.put('url/api/v2/kols/' + KOL_ID + '/affiliations',
                       params=params, data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"

#---------------------------------------------------------
#          3.Main Test Case
#---------------------------------------------------------


def perform_check(collection, test_case, key, check_type):
    expected_current_collection = test_case.get(key)
    current_emails = collection.get('emails')
    flag_check_email = True
    # if both have something
    if expected_current_collection.get('emails') and current_emails:
        for email in current_emails:
            #print(check_type, "Checking email:", email['email'])
            if email['email'] in expected_current_collection['emails']:
                #print(check_type, "Email check:", "Pass")
                flag_check_email = True
            else:
                #print(check_type, " Email check:", "Fail")
                flag_check_email = False

    # Both are empty
    elif not expected_current_collection['emails'] and not current_emails:
        #print(check_type, " Email check:", "Pass")
        flag_check_email = True
    # One of them is empty
    else:
        #print(check_type, "Email check:", "Fail")
        flag_check_email = False

    if flag_check_email:
        # #pprint.pprint(current_emails)
        print("For Email,Test case is Pass ")
        f.write("For Email,Test case is Pass")
    else:
        # pprint.pprint(current_emails)
        print("For Email,Test case is Fail ")
        f.write("For Email,Test case is Fail")

    # Phone
    current_phones = collection.get('phone_numbers')
    # if both have something
    flag_check_phone = True

    if expected_current_collection['phone_numbers'] and current_phones:
        for phone in current_phones:
            #print(check_type, "Checking phone:", phone['phone'])
            if phone['phone'] in expected_current_collection['phone_numbers']:
                #print(check_type, " Phone check:", "Pass")
                flag_check_phone = True

            else:
                #print(check_type, " Phone check:", "Fail")
                flag_check_phone = False

    # Both are empty
    elif not expected_current_collection['phone_numbers'] and not current_phones:
        #print(check_type, " Phone check:", "Pass")
        flag_check_phone = True

    # One of them is empty
    else:
        #print(check_type, " Phone check:", "Fail")
        flag_check_phone = False
    if flag_check_phone:
        # #pprint.pprint(current_phones)
        print("For Phone,Test case is Pass")
        f.write("For Phone,Test case is Pass")
    else:
        # pprint.pprint(current_phones)
        print("For Phone,Test case is Fail")
        f.write("For Phone,Test case is Fail")
    flag_check_address = True

    # Address
    current_addresses = collection.get('addresses')
    # if both have something

    if expected_current_collection['addresses'] and current_addresses:
        for address in current_addresses:
            #print(check_type, "Checking address:", address['address'])
            if address['address'] in expected_current_collection['addresses']:
                #print(check_type, " address check:", "Pass")
                flag_check_address = True

            else:
                #print(check_type, " address check:", "Fail")
                flag_check_address = False

    # Both are empty
    elif not expected_current_collection['addresses'] and not current_addresses:
        #print(check_type, " address check:", "Pass")
        flag_check_address = True

    # One of them is empty
    else:
        #print(check_type, " address check:", "Fail")
        flag_check_address = False

    if flag_check_address:
        # #pprint.pprint(current_addresses)
        print("For Address,Test case is pass")
        f.write("For Address,Test case is Pass")
    else:
        # pprint.pprint(current_addresses)
        print("For Address,Test case is Fail")
        f.write("For Address,Test case is Fail")

    if flag_check_email and flag_check_address and flag_check_phone:
        print(check_type, " Collection Check:", "Pass")
        f.write("For Collection,Test case is Pass")

    else:
        print(check_type, " Collection Check:", "Fail")
        f.write("For Collection,Test case is Fail")
#---------------------------------------------------------
#         4.Get Data from Current collection
#---------------------------------------------------------
# To get data from current collection


def check_execution_status_current_collection(account, test_case):
    expected_current_collection = test_case.get('test_case_current_collection')
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    req = requests.get(
        'url/api/v1/kols/' + KOL_ID + '', headers=Header)
   #print(req.status_code, req.text)
    print("Current collection Status:")
    if req.status_code == 200:
        innoData = req.json()['data'].get('innoData', None)
        if innoData:
            innoAssociations = innoData['innoAssociations']
            #print("Checking:", "Last Updated by")
            lastUpdated = innoAssociations.get('lastUpdatedBy')
            print("Last updated by:", lastUpdated['email'])
            if lastUpdated and lastUpdated['email'] == account['email']:
                print("Last Updated check:", "Pass")
                f.write("Last Updated check Pass")
                #test_case_results['last_updated_by'] = ACCESS_EMAIL
            else:
                print("Last Updated check:", "Fail")
                f.write("Last Updated check Fail")

            perform_check(innoAssociations, test_case,
                          'test_case_current_collection', "Current")

        else:
            print("GET test case failed:", "No data in current collection.")
    else:
        print(req.status_code, req.text)

#---------------------------------------------------------
#         5.Check another Client Collection
#---------------------------------------------------------


def check_execution_status_on_client_current_collection(account, test_case, client):
    expected_current_collection = test_case.get('test_case_current_collection')
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    req = requests.get(
        'url/api/v1/kols/' + KOL_ID + '', headers=Header)
   #print(req.status_code, req.text)
    print("Checking data in " + client + " current collection with test cases:")
    # #pprint.pprint(req.json())
    if req.status_code == 200:
        innoData = req.json()['data'].get('orgData', None)
        if innoData:
            innoAssociations = innoData['orgAssociations']
            #print("Checking:", "Last Updated by")
            lastUpdated = innoAssociations.get('lastUpdatedBy')
            # pprint.pprint(lastUpdated)
            print("Last updated by:", lastUpdated['email'])
            if lastUpdated and lastUpdated['email'] == account['email']:
                print("Last Updated check:", "Pass")
                #test_case_results['last_updated_by'] = ACCESS_EMAIL
                pass
            else:
                print("Last Updated check:", "Fail")

            perform_check(innoAssociations, test_case,
                          'test_case_current_collection', client + " current")

        else:
            print("GET test case failed:", "No data in current collection.")
    else:
        print(req.status_code, req.text)
#---------------------------------------------------------
#       6.Create Environment for Merck
#---------------------------------------------------------


def run_condition_Merck(body, condition_type):
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)
    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }

    body = json.dumps(body)

    req = requests.put('url/api/v1/kols/' + KOL_ID + '',
                       params=params, data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"
#---------------------------------------------------------
#                    6.DB collections
#---------------------------------------------------------
# To check History collection


def check_history_collection(test_case):

    collection = get_collections_history()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'timestamp', DESCENDING).limit(1)
    history_data = find_last_history[0]

    perform_check(history_data, test_case, 'test_case_history', "History")
# To check History collection


def check_history_collection_client(test_case):

    collection = get_collections_history_Merck()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'timestamp', DESCENDING).limit(1)
    history_data = find_last_history[0]

    perform_check(history_data, test_case, 'test_case_history', "History")


#-------------------------------------------------------
'''
def write_csv(self,test_case,test_case_name,test_case_descripation):
       file_exists=os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['test_case','test_case_name','test_case_descripation','precondition','expected_current_collection','expected_history_collection','latest_time_stamp']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'name':name,'author_id':author_id, 'affiliation':affiliation,\
                               'country':country,'publication_updated':updated_publication,\
                                'updated_pmid':updated_pmid,'latest_time_stamp':latest_time_stamp})
'''


#---------------------------------------------------------
#                     Final Step
#---------------------------------------------------------


def run_test_cases():

    get_auth_token(User_details)

    for test_case in TEST_CASES[0:1]:
        print("-" * 20)
        print("Executing", test_case['test_case_name'])
        #f.write("This is %s\r\n" % test_case['test_case_name'])

        print("-" * 20)
        #print(test_case['test_case_descripation'])
        #f.write("This is %s\r\n" % test_case['test_case_descripation'])

        print("-" * 20)
        print(test_case['precondition'])
        #f.write("This is %s\r\n" % test_case['precondition'])

        print("-" * 20)
        #print("Precondition Data", test_case['test_case_senario'])
        #f.write("This is %s\r\n" % test_case['test_case_history'])

        print("-" * 20)
        #print("Current collection Data",
              test_case['test_case_current_collection'])
        #f.write("This is %s\r\n" % test_case['test_case_current_collection'])

        print("-" * 20)
        #print("History collection Data", test_case['test_case_history'])
        #f.write("This is %s\r\n" % test_case['test_case_history'])

        print("-" * 20)

        # 1. Generate Environment
        run_condition(test_case['test_case_senario'], 'Scenario')
        # 2. Execute Test case.
        time.sleep(5)
        run_condition(test_case['test_case_execute'], 'Test Case')
        # 3. Chechk In Current
        time.sleep(5)
        check_execution_status_current_collection(
            User_details, test_case)
        print("-" * 20)
        # 4. Chech in history
        check_history_collection(test_case)
        print("-" * 20)
        get_auth_token(User_details)
        check_execution_status_on_client_current_collection(
            User_details, test_case, 'Merck')
        print("-" * 20)
        get_auth_token(User_details)
        print("-" * 20)
    '''
    get_auth_token(User_details)
    for test_cases_merck in TEST_CASES_MERCK[0:1]:
        print("-" * 20)
        print("Executing",test_cases_merck['test_case_name'])
        print("-" * 20)
        print(test_cases_merck['test_case_descripation'])
        print("-" * 20)
        print(test_cases_merck['precondition'])
        print("-" * 20)
        print("-" * 20)
        run_condition(
            test_cases_merck['test_case_senario'], 'Scenario')
        time.sleep(5)
        run_condition(
            test_cases_merck['test_case_execute'], 'Test Case')
        time.sleep(5)

        check_execution_status_on_client_current_collection(
            User_details, test_cases_merck, 'Merck')
        time.sleep(5)
        check_history_collection_client(test_cases_merck)
        get_auth_token(User_details)
        print("-" * 20)        
        check_execution_status_current_collection(
            User_details, test_cases_merck)
        get_auth_token(User_details)
        
        print("-" * 20)
        '''


#---------------------------------------------------------


if __name__ == '__main__':
    run_test_cases()
