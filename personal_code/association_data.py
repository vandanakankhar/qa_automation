import random
BUSINESS_UNIT = [

    'GMA'+ str(random.randint(1, 100)) +'',
    'GMP'+ str(random.randint(1, 100)) +'',
    'GHL'+ str(random.randint(1, 100)) +'',
    'GYT'+ str(random.randint(1, 100)) +''
   

]
ASSOCIATION_NAME = [

    'CONSULTLING ACTIVITY'+ str(random.randint(1, 100)) ,''+
    'ADVISORY BOARDS'+ str(random.randint(1, 100)) +'',
    'SPEAKING ENGAGEMENT'+ str(random.randint(1, 100)) +'',
    'MEDICAL EDUCATION EVENT'+ str(random.randint(1, 100)) +'',
]
ASSOCIATION_TYPE = [
    #
       '' + str(random.randint(1, 100)) + 'FIRST Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'SECOND Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'THIRD Merck_ADDRESS,Pune, Manhattan',
       '' + str(random.randint(1, 100)) + 'FOURTH Merck_ADDRESS,Pune, Manhattan',
]



TEST_CASES_ASSOCIATION = [
    {
        'test_case_name': 'Add_Association_1',
        'test_case_descripation': 'Add association',
        'precondition': 'One association is already present',

        'test_case_senario': {
                "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
        },
        'test_case_execute': {
           "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[1],
                        "activity_type": BUSINESS_UNIT[1],
                        "association": BUSINESS_UNIT[1],
                        "id":1
                    }
                ]
                },
        'test_case_history':{
             "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
        }
    },
    {


        'test_case_name': 'Edit_Association_2',
        'test_case_descripation': 'Edit Association',
        'precondition': 'One association is present',

        'test_case_senario': {
                "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
        },
        'test_case_execute': {
           "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
                },
        'test_case_history':{
             "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
        },
        'test_case_current_collection':{
            "BU": [BUSINESS_UNIT[0]],
            "association": [ASSOCIATION_NAME[0]],
            "activity_type": [ASSOCIATION_TYPE[0]],
        }
    },
    {


        'test_case_name': 'Delete_Association_2',
        'test_case_descripation': 'Edit Association',
        'precondition': 'One association is present',

        'test_case_senario': {
                "company_associations": [
                    ]
        },
        'test_case_execute': {
           "company_associations": [
                    {
                        "BU": BUSINESS_UNIT[0],
                        "activity_type": BUSINESS_UNIT[0],
                        "association": BUSINESS_UNIT[0],
                        "id":1
                    }
                ]
                },
        'test_case_history':{
             "company_associations": [

                ]
        },
        'test_case_current_collection':{
            "BU": [BUSINESS_UNIT[0]],
            "association": [ASSOCIATION_NAME[0]],
            "activity_type": [ASSOCIATION_TYPE[0]],
        }
    }

]
