from accounts import User_details, User_details
import requests
from body import TEST_CASES
from body_pro import TEST_CASES_pro
import json
import csv
import os.path
import time
import pprint
from elasticsearch import Elasticsearch
out_filename ="Top200_kol_result.csv"
out_filename1 = "Meta_info.csv"
global_client = Elastic_Search_usernam_password
global_production = Elastic_Search_usernam_password
#global_production = Elasticsearch(['elastic_Search_username_password'])

list_staging=[]
list_pro=[]
Application's_url
pags = ''

def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        'application_callback_url', params=payload)
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
#============================================================================
def auth_login_production(account):

    r = requests.post('url', json=account)

    dict = r.json()

    code = dict['responseData']['callback_url']

    codes = code[26:]
    return codes
def auth_callback_api_production(account):
    global accesstoken
    payload = {
        "code": auth_login_production(account),
        "app":"name_of_app"
    }
    req = requests.get(
        url, params=payload)

    accesstoken_dict = req.json()
    accesstoken = accesstoken_dict['accessToken']
    print("Granted access for Production:")
    return accesstoken
# to make Auth token generic
def get_auth_token_production(account):
    global AUTH_TOKEN_producation
    AUTH_TOKEN_producation = auth_callback_api_production(account)

def staging_score(body):
    get_auth_token(User_details)
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    url = 'url'
    req = requests.post(url,data=json.dumps(body), headers=Header)

    #print("========================\n\n\n", url, Header, req.json())
    print("status : ", req.status_code)
    #print("Response Body : ",req)
    list_dict_staging = []
    inno_data = req.json().get('data',[])
    for data in inno_data :

        score_data_staging = data.get('scores',[])
        #for author_data in score_data_staging:
        for score_pro in score_data_staging:
            author_id_staging = score_pro.get('author_id','')
            list_staging.append(author_id_staging)




def run_test_cases():
    staging_score(TEST_CASES["Oncology"])


if __name__ == '__main__':
    run_test_cases()
