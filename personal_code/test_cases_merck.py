import random
TEST_EMAILS_MERCK = [
    'email5611' + str(random.randint(1, 100)) + '@Merck11.com',
    'email1111' + str(random.randint(1, 100)) + '@Merck11.com',
    'email112' + str(random.randint(1, 100)) + '@Merck22.com',
    'email11113' + str(random.randint(1, 100)) + '@Merck33.com',

]
TEST_PHONES_MERCK = [
   
    '001012334111' + str(random.randint(1, 100)) + '',
    '1234567' + str(random.randint(1, 100)) + '',
    '03303333' + str(random.randint(1, 100)) + '',
    '04404444' + str(random.randint(1, 100)) + '',
]
TEST_ADDRESSES_MERCK = [
    '' + str(random.randint(1, 100)) + 'FIRST Merck_ADDRESS,Pune, Manhattan',
    '' + str(random.randint(1, 100)) + 'SECOND Merck_ADDRESS,Pune, Manhattan',
    '' + str(random.randint(1, 100)) + 'THIRD Merck_ADDRESS,Pune, Manhattan',
    '' + str(random.randint(1, 100)) + 'FOURTH Merck_ADDRESS,Pune, Manhattan',
]

TEST_CASES_MERCK = [
    {
        'test_case_name': 'inno_add_kol_1',
        'test_case_descripation': 'To verify Data added by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be blank',

        'test_case_senario': {
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
            "history": True
        },
        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[0],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS_MERCK[0]],
            "phone_numbers": [TEST_PHONES_MERCK[0]],
            "addresses": [TEST_ADDRESSES_MERCK[0]],
        }
    },
    {
        'test_case_name': 'merck_add_kol_2',
        'test_case_descripation': 'To verify Data ADDED by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be Present.',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[0],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[0],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[0],
                    "timestamp": None,
                    "isNew": False
                },
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[1],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[0],
                    "source": [],
                    "isNew": False
                },
                {
                    "phone": TEST_PHONES_MERCK[1],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[0],
                    "source": [],
                    "isNew": False
                },
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[1],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [TEST_EMAILS_MERCK[0]],
            "phone_numbers": [TEST_PHONES_MERCK[0]],
            "addresses": [TEST_ADDRESSES_MERCK[0]],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS_MERCK[0], TEST_EMAILS_MERCK[1]],
            "phone_numbers": [TEST_PHONES_MERCK[0], TEST_PHONES_MERCK[1]],
            "addresses": [TEST_ADDRESSES_MERCK[0], TEST_ADDRESSES_MERCK[1]],
        }
    },
    {
        'test_case_name': 'merck_delete_kol_3',
        'test_case_descripation': 'To verify Data Deleted by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be Present',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[2],
                    "timestamp": None,
                    "isNew": False
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[2],
                    "source": [],
                    "isNew": False
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[2],
                    "source": [],
                    "isNew": False
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
            "history": True
        },
        'test_case_history':{
            "emails": [TEST_EMAILS_MERCK[2]],
            "phone_numbers": [TEST_PHONES_MERCK[2]],
            "addresses": [TEST_ADDRESSES_MERCK[2]],
        },
        'test_case_current_collection':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        }
    },
    {
        'test_case_name': 'merck_edit_kol_4',
        'test_case_descripation': 'To verify Data Edited by Super Admin should be stored in Respective collection and respective History.',
        'precondition': 'Email,phone,address should be blank',
        'test_case_senario': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[0],
                    "timestamp": None,
                    "isNew": False,
                    'id':0
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[0],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[0],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "history": True
        },
        'test_case_execute': {
            "emails": [
                {
                    "source": [],
                    "email": TEST_EMAILS_MERCK[1],
                    "timestamp": None,
                    "isNew": False,
                    'id':0
                }
            ],
            "phone_numbers": [
                {
                    "phone": TEST_PHONES_MERCK[1],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "addresses": [
                {
                    "timestamp": None,
                    "address": TEST_ADDRESSES_MERCK[1],
                    "source": [],
                    "isNew": False,
                    'id':0
                }
            ],
            "history": True
        },
        'test_case_history':{
            "emails": [],
            "phone_numbers": [],
            "addresses": [],
        },
        'test_case_current_collection':{
            "emails": [TEST_EMAILS_MERCK[1]],
            "phone_numbers": [TEST_PHONES_MERCK[1]],
            "addresses": [TEST_ADDRESSES_MERCK[1]],
        }
    }
]
