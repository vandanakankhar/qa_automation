import requests
import csv
import pprint
import os.path
from elasticsearch import Elasticsearch
global_client = Elasticsearch(["10.240.0.161:9200"])
out_filename = "output_kol_information.csv"
import sys
class kol_information():
    def es_query(self,author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body
    def publications_query(self ,author_id):
        body = {
                  "sort": {
                    "created_at": "desc"
                  },
                  "query" :{
                    "match":{
                      "authors.author_id": author_id
                    }
                  }
                }
        return body
    def congress_query(self , author_id):
        body_congress = {
                     "sort": {
                       "created_at": "desc"
                     },
                     "query": {
                       "nested": {
                         "path": "authors",
                         "query": {
                           "bool": {
                             "must": [
                               {
                                 "terms": {
                                   "authors.author_id": [
                                     author_id
                                   ]
                                 }
                               }
                             ]
                           }
                         }
                       }
                     }
                    }
        return body_congress
    def write_result(self,name,author_id,affiliation, country ,TA,congress_title ,congress_name,congress_abstract,publication_title ,publication_journal,publication_abstract) : #TA):
        file_exists=os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            fieldnames = ['Name', 'affiliation in Author corpus', 'country in Author corpus','author_id' , 'TA','Congress Title',\
                                   'Congress Name','Congress abstract','Publication Title','Publication Journal','Publication Abstract']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Name': name, 'affiliation in Author corpus': affiliation, 'country in Author corpus':country, \
                                      'author_id':author_id, 'TA':TA ,'Congress Title':"||".join(congress_title) ,'Congress Name':"||".join(congress_name) ,\
                                          'Congress abstract':"||".join(congress_abstract) ,'Publication Title':"||".join(publication_title),\
                                                 'Publication Journal':"||".join(publication_journal) , 'Publication Abstract':"||".join(publication_abstract)}) #, 'TA': TA})
    def get_kol_info(self,input_file):
        author_ids = []
        with open(input_file,'r')as csvfile:
            reader = csv.DictReader(csvfile)
            for line in reader :
                author_id = line.get("Author_id").encode('utf-8')
                #TA = line.get("TA").encode('utf-8')
                TA = 'Metabolic_Diseases'
                author_name = ''
                body = self.es_query(author_id)
                body_publication = self.publications_query(author_id)
                body_congress = self.congress_query(author_id)
                response = global_client.search(index="authors_alias", body = body, request_timeout=10)
                response_publication = global_client.search(index = "index_name" , body = body_publication,request_timeout=10)
                response_congresses = global_client.search(index ="index_name" ,body = body_congress ,request_timeout=10)
                count = 0
                # if response['hits']['hits']:
                #     break
                for doc in response['hits']['hits']:
                    author_country=''
                    author_name=doc.get('_source',{}).get('name','').encode('utf-8')
                    author_affiliation=doc.get('_source',{}).get('current_affiliation',{}).get('affiliation','').encode('utf-8')
                    for country in doc.get('_source',{}).get('current_affiliation',{}).get('extracted_terms',{}).get('country',[]):
                        author_country=country.encode('utf-8')
                article_title = []
                journal_title = []
                publication_abstract = []
                # if response_publication['hits']['hits']:
                #     break
                for docs in response_publication['hits']['hits']:
                    count += 1
                    if count == 5:
                        break
                    article = docs.get('_source',{}).get('article_title','')
                    journal = docs.get('_source',{}).get('journal_title','')
                    abstract = docs.get('_source',{}).get('abstract','')
                    if article :
                        article_title.append(article.encode('utf-8'))
                    if journal :
                        journal_title.append(journal.encode('utf-8'))
                    if abstract :
                        publication_abstract.append(abstract.encode('utf-8'))
                congress_title = []
                congress_abstract = []
                congress_name = []
                # if response_congresses['hits']['hits']:
                #     break
                for docs in response_congresses['hits']['hits']:
                    #import ipdb; ipdb.set_trace()
                    count += 1
                    if count == 5:
                        break
                    title = docs.get('_source',{}).get('title','')
                    abstract = docs.get('_source',{}).get('abstract','')
                    name = docs.get('_source',{}).get('congress_name','')
                    if title :
                        congress_title.append(title.encode('utf-8'))
                    if abstract :
                        congress_abstract.append(abstract.encode('utf-8'))
                    if name :
                        congress_name.append(name.encode('utf-8'))
                #import ipdb; ipdb.set_trace()
                self.write_result(author_name,author_id,author_affiliation,author_country,TA,congress_title,congress_name,congress_abstract,\
                                      article_title,journal_title,publication_abstract)#TA)
if __name__=='__main__':
    #TA = sys.argv[1]
    #print("kol_information script has started and the TA is :"+TA)
    obj=kol_information()
    #obj.get_kol_info("top20_"+TA+".csv")
    obj.get_kol_info("Metabolic_Diseases_author_ids.csv")
