from accounts import User_details, User_details
import requests
from body import TEST_CASES
from body_pro import TEST_CASES_pro
import json
import csv
import os.path
import pprint


AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MTkxMDI2NzAsInNlc3Npb25fa2V5IjoiZWJjZjM4N2YtNjc5OS00YmE4LTg0Y2YtMDA2MmFmYjA4ZGI3IiwiZXhwIjoxNTE5MTg5MDcwfQ.6mpDP1EFz5RyrR2UsIh0kyOScChaXiUNILSRp8xDQ7E"
AUTH_TOKEN_producation="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MTkxMDI2MTgsInNlc3Npb25fa2V5IjoiYjQ2NGQwMjMtZGMwYi00ODA4LWJkMzctYTYyZWNkMTYzZWE3IiwiZXhwIjoxNTE5MTg5MDE4fQ.T_xaKzAf6iWO7nUlLrPPh0spCPeqzzp2x8XzhS_SNcI"
KOL_ID="60aa0668880a4e43b200ae63456fb16e"
list_staging=[]
list_pro=[]
Application's_url

def auth_login(account):

    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    #print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    codes = code[29:]
    #print(codes)
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }

    req = requests.get(
        'url/api/v1/auth/callback', params=payload)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
#print(accesstoken)
    print("Granted access token:", accesstoken[10:])
    return accesstoken
# to make Auth token generic


def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(account)

def staging_data(body):
    #print(json.dumps(body))
    #print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }

    url = 'url/api/v1/kols/score/'
    req = requests.post(url,data=json.dumps(body), headers=Header)
    print("status : ", req.status_code)
    #print("Response Body : ",req)
    #pprint.pprint(json.dumps(req.json()))
    inno_data = req.json().get('data',[])
    for data in inno_data :
        score_data_staging = data.get('scores',[])
        # for author_data in score_data_staging:
        #     author_id = author_data.get('author_id','')
        #     list_staging.append(author_id)
        #     #compare_score(staging_asset_class,staging_doc_count)

        #print(list_staging)
    # print(BASE_API + 'kols/score/')
    return score_data_staging,list_staging
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"

def producation_data(body_pro):

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
        'content-type': 'application/json'
    }

    url='url/api/v1/kols/score/'
    req = requests.post(url,data=json.dumps(body_pro), headers=Header)
    #print(url)
    print("status : ", req.status_code)

    #print(json.dumps(req.json()))
    inno_data = req.json().get('data',[])
    for data in inno_data :
        score_data_producation = data.get('scores',[])
        # for author_data in score_data_producation:
        #     author_id = author_data.get('author_id','')
        #     list_pro.append(author_id)

        #print(list_pro)

    # print(BASE_API + 'kols/score/')

    # print("Response Body : ",req)
    return list_pro,score_data_producation
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"
def compare_score(score_data_producation,score_data_staging):



def perform_check(list_pro,list_staging):
    x = set(list_pro)-set(list_staging)
    y = set(list_staging)-set(list_pro)
    #len(x)
    print(len(y))
    print(y)
    print("Diff in staging and producation",y)
    write_csv(x,y)
    for count in range(0,200):
        if list_staging[count] == list_pro[count]:
            print("Matched")
        else:
            print("Not Matched",list_staging[count],list_pro[count])

    return y

def write_csv(x,y):
       file_exists=os.path.isfile("Mismatch.csv")
       with open("Mismatch.csv", 'a') as csvfile:
           feild_names= ['staging_diff','producation_diff']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'staging_diff':x,'producation_diff':y,})


def run_test_cases():
    list_staging,score_data_staging = staging_data(TEST_CASES["Neurology"])
    list_pro,score_data_producation = producation_data(TEST_CASES_pro["Neurology"])
    #perform_check(list_pro,list_staging)
    compare_score(score_data_staging,score_data_producation)


if __name__ == '__main__':
    run_test_cases()
