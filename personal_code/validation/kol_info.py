import requests
import csv
import pprint
import os.path
import json
from elasticsearch import Elasticsearch

global_client = elastic_Search_ip)


out_filename = "output_kol_information.csv"
import sys
class kol_information():
    def es_query(self,author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body
    def patents_query(self ,author_id):
        body = {
                  "sort": {
                    "created_at": "desc"
                  },
                  "query" :{
                    "match":{
                      "authors.author_id": author_id
                    }
                  }
                }
        return body
    def societies_query(self , author_id):
        body_societies = {
                     "query": {
                       "nested": {
                         "path": "authors",
                         "query": {
                           "bool": {
                             "must": [
                               {
                                 "terms": {
                                   "authors.author_id": [
                                     author_id
                                   ]
                                 }
                               }
                             ]
                           }
                         }
                       }
                     }
                    }
        return body_societies
    def write_result(self,author_id,qualification,author_affiliation,author_country,author_designation,patent_title,patent_id,country,patent_abstract,status,societies_name,source_url): #TA):
        file_exists=os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            fieldnames = ['Author id', 'Qualification', 'affiliation','Author country' , 'Designation','Patent Title','Patent Id','Patent country','Patent Abstract','status','societies Name','source_url']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author id':author_id,'Qualification':qualification, 'affiliation':author_affiliation,'Author country':author_country , 'Designation':author_designation,'Patent Title':patent_title,'Patent Id':patent_id,'Patent country':country,'Patent Abstract':patent_abstract,'status':status,'societies Name':societies_name,'source_url':source_url})

    def get_kol_info(self,input_file):
        author_ids = []

        with open(input_file,'r')as csvfile:
            reader = csv.DictReader(csvfile)
            for line in reader :


                author_id = line.get("Author_id")
                #.encode('utf-8')
                #TA = line.get("TA").encode('utf-8')
                TA = 'Metabolic_Diseases'
                author_name = ''
                body = self.es_query(author_id)

                body_patents = self.patents_query(author_id)

                body_societies = self.societies_query(author_id)

                response = global_client.search(index="authors_alias", body = body, request_timeout=10)
                #print(json.dumps(response))

                response_patents = global_client.search(index = "new_patents_alias" , body = body_patents,request_timeout=10)

                response_societies = global_client.search(index ="societies_alias" ,body = body_societies ,request_timeout=10)

                count = 0
                #print response['hits']['hits']
                #     break
                for doc in response['hits']['hits']:
                    author_country=''
                    author_name=doc.get('_source',{}).get('name','').encode('utf-8')
                    print(json.dumps(author_name))

                    author_qualification=doc.get('_source',{}).get('qualifications_info',{})
                    #.get('qualification','').encode('utf-8')
                    qualification = ''
                    for qul in author_qualification:
                        qualification=qul.get('qualification','')

                    author_affiliation=doc.get('_source',{}).get('current_affiliation',{}).get('affiliation','').encode('utf-8')
                    #print(author_affiliation)
                    for country in doc.get('_source',{}).get('current_affiliation',{}).get('extracted_terms',{}).get('country',[]):
                        author_country=country.encode('utf-8')
                    author_designation = ''
                    for designation in doc.get('_source',{}).get('current_affiliation',{}).get('extracted_terms',{}).get('designation',[]):
                        author_designation=designation.encode('uty-8')


                # if response_publication['hits']['hits']:
                #     break

                patent_title = []
                patent_id = []
                country_list=[]
                patent_abstract = []
                status=[]
                for docs in response_patents['hits']['hits']:
                    count += 1
                    if count == 5:
                        break

                    title = docs.get('_source',{}).get('title','')
                    patent = docs.get('_source',{}).get('patent_id','')
                    c = docs.get('_source',{}).get('country','')
                    abstract = docs.get('_source',{}).get('abstract','')
                    fee_status=docs.get('_source',{}).get('fee_status','')
                    if title :
                        print("========================123455", title.encode('utf-8'))
                        patent_title.append(title.encode('utf-8'))
                    if patent:
                        patent_id.append(patent.encode('utf-8'))

                    if c:
                        country_list.append(c.encode('utf-8'))
                        #.encode('utf-8')
                    if abstract:
                        patent_abstract.append(abstract.encode('utf-8'))
                    if fee_status:
                        status.append(fee_status)




                # if response_congresses['hits']['hits']:
                #     break
                societies_name = []
                source_url = []
                for docs in response_societies['hits']['hits']:
                    #import ipdb; ipdb.set_trace()

                    count += 1
                    if count == 5:
                        break
                    name = docs.get('_source',{}).get('body_name','')

                    url = docs.get('_source',{}).get('source_url','')

                    if name :
                        societies_name.append(name.encode('utf-8'))
                        #('utf-8'))
                    if url :
                        source_url.append(url.encode('utf-8'))
                        #.encode('utf-8'))

                        #.encode('utf-8'))
                #import ipdb; ipdb.set_trace()

                print(patent_title, "========================================")
                self.write_result(author_id,qualification,author_affiliation,author_country,author_designation,patent_title,patent_id,country,patent_abstract,status,societies_name,source_url)


if __name__=='__main__':
    #TA = sys.argv[1]
    #print("kol_information script has started and the TA is :"+TA)
    obj=kol_information()
    #obj.get_kol_info("top20_"+TA+".csv")
    obj.get_kol_info("connection.csv")
