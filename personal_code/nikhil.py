import requests
import json
import pprint
import csv
import os.path
import pprint
out_filename ="top20_Neurology.csv"
out_filename_1 ="top20_Oncology.csv"
out_filename_2="top20_Cardiology.csv"
out_filename_3 ="top20_Dermatology.csv"
out_filename_4 ="top20_Reproductive_Health.csv"
out_filename_5 ="top20_Metabolic_Diseases.csv"
out_filename_6 ="top20_Endocrinology.csv"
from accounts import User_details
global count
count = 0
AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjU3NzgzMTMsInNlc3Npb25fa2V5IjoiYTUxYmQxZjEtZWY1NC00OWNjLTkwMWEtNDVhZjg4OTY2OGQ4IiwiZXhwIjoxNTI1ODY0NzEzfQ.XykXv7CTMX9oMDVPGMt8Klj1SltQo75VvZXhs5dMF0M"
def get_input(count, as_class, TA, list_indications, country=[]):

    #get_auth_token(User_details)
    print(as_class, TA, list_indications, country)

    url = 'url'
    payload = {}
    if as_class == "Top KOLs":
        payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [{"name": "Publications", "value": 15, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Clinical Trials", "value": 13, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Congresses", "value": 12, "showDate": True, "startDate": 0, "endDate": 0}, {"name": "Guidelines", "value": 10, "showDate": True, "startDate": 0, "endDate": 0}, {
            "name": "Regulatory Bodies", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "HTA", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Societies", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Advocacy", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}, {"name": "Top Hospital KOLs", "value": 10, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": True, "kolType": "allkols","Countries":{"values":country,"isAnd":False}}}

    else:
        payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [
            {"name": as_class, "value": 100, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": False, "kolType": "allkols", "Countries": {"values": country, "isAnd": False}}}

    headers = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    #print(json.dumps(payload))
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    #print(r)
    r = r.json()
    #pprint.pprint(r)
    for i in r['data'][0]['scores']:

        count+= 1
        print(count)
        #if count <= 15:
            #continue
        #author_id=i['author_id']

        print(i['author_id'])
        #,list_indications,country,as_class,count)
        #writer.writerow({'AUTHOR_ID' : i['author_id'], "TA" : TA, "INDICATION" : list_indications, "COUNTRY" : country, "RESOUCES" : as_class})






if __name__ == '__main__':

    TA= ["Neurology"]
    #TA = ["Neurology","Oncology","Cardiology","Dermatology","Reproductive Health","Metabolic Diseases","Endocrinology"]
    #Breast cancer","Acute Lymphoblastic Leukemia

    INDICATION_DCT = {"Neurology": ["Multiple Sclerosis"], "Oncology": ["NSCLC"],"Cardiology": ["Myocardial Infarction"],"Dermatology": ["Dermatitis", "Psoriasis"],"Reproductive Health": ["Male Infertility","Female Infertility","Ovarian Disease","Assisted Reproduction"],"Metabolic Diseases":["Diabetes"],"Endocrinology": ["Thyroid Disorders"]}
    #, "Obesity"

    LIST_ASSET_CLASS = ["Top KOLs","Publications", "Clinical Trials", "Congresses","Societies","Top Hospital KOLs","HTA","Regulatory Bodies","Advocacy","Guidelines"]
    #Brazil","Argentina","Mexico","Colombia","Chile","Peru","Costa Rica","Ecuador","Panama"],
    #"United States of America","Canada","United Kingdom","France","Italy","Spain","Germany"
    COUNTRY_DCT ={
                "Multiple Sclerosis": ["Brazil","Argentina","Mexico","Colombia","Chile","Peru","Costa Rica","Ecuador","Panama"],
                "Alzheimer":["Germany"],
                "Parkinon":["Germany"],
                "Breast cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "NSCLC":["United States Of America","Spain","Italy","France","United Kingdom","Germany"],
                "Acute Lymphoblastic Leukemia":["Germany","United States of America"],
                "Dermatitis":["France","Germany","United States Of America"],
                "Psoriasis":["France","Germany","United States Of America"],
                "Myocardial Infarction":["Germany","Denmark","Switzerland","France","United States of America","United Kingdom","France","Spain","Japan","Australia"],
                "Male Infertility":["Switzerland","Spain","Germany","Italy"],
                "Female Infertility":["Switzerland","Spain","Germany","Italy"],
                "Ovarian Disease":["Switzerland","Spain","Germany","Italy"],
                "Assisted Reproduction":["Switzerland","United States of America","Germany"],
                "Diabetes":["United States Of America"],
                "Thyroid Disorders":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Obesity":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"]}

    count = 0
    for ta_name in TA:
        for resource in LIST_ASSET_CLASS:
            for indication in INDICATION_DCT[ta_name]:
                for country in COUNTRY_DCT[indication]:
                    get_input(count, resource, ta_name, [indication],[country])
                    print(count,ta_name,[indication],resource,[country])

# if __name__ == '__main__':
#     country(TA,INDICATION_DCT,LIST_ASSET_CLASS)
#
#
#


    # LIST_ASSET_CLASS = ["Top KOLs", "Publications"]
    # # list_country=["Costa Rica", "Chile", "Panama", "Ecuador", "Mexico", "Peru", "Argentina", "Brazil", "Columbia", ]
    # TA = ["Neurology"]
