from accounts import User_details, User_details
import requests
from body import TEST_CASES
from body_pro import TEST_CASES_pro
import json
import csv
import os.path
import time
import pprint
from elasticsearch import Elasticsearch
out_filename ="Top200_kol_result.csv"
out_filename1 = "Meta_info.csv"
global_production =  Elastic_Search_usernam_password
global_client = Elastic_Search_usernam_password
#global_production = Elasticsearch(['elastic_Search_username_password'])
#import sys



#AUTH_TOKEN_producation="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjcwNTg4ODMsInNlc3Npb25fa2V5IjoiY2I4NDc0NWQtMjcxZC00Yjk1LTg3NzgtNjRlZDYzNjIyMjJhIiwiZXhwIjoxNTI3MTQ1MjgzfQ.jykTVyU0CJm7iM3HfGSxJv5w-TqhRFrkXJUZki8g7oQ"
#KOL_ID="60aa0668880a4e43b200ae63456fb16e"
list_staging=[]
list_pro=[]
Application's_url
pags = ''


# def auth_login(account):
#     r = requests.post('Url_of_application', json=account)
#     #print(r.status_code)
#     #pprint.pprint(r.json())
#     dict = r.json()
#     #print(dict)
#     code = dict['responseData']['callback_url']
#     #print(code)
#     codes = code[34:]
#     #print(codes)
#     #print("Granted login codes :", codes)
#     return codes
# def auth_callback_api(account):
#     global accesstoken
#     payload = {
#         "code": auth_login(account),
#         "app":"name_of_app"
#     }
#     req = requests.get(
#         'application_callback_url', params=payload)
#     #print(r.status_code)
#     accesstoken_dict = req.json()
#     #print(accesstoken_dict)
#     # print(accesstoken_dict['accessToken'])
#     accesstoken = accesstoken_dict['accessToken']
#     #print(accesstoken)
#     print("Granted access for staging")
#     return accesstoken
# # to make Auth token generic
# def get_auth_token(account):
#     global AUTH_TOKEN
#     AUTH_TOKEN = auth_callback_api(account)
#============================================================================
# def auth_login_production(account):
#
#     r = requests.post('url', json=account)
#
#     dict = r.json()
#
#     code = dict['responseData']['callback_url']
#
#     codes = code[26:]
#     return codes
# def auth_callback_api_production(account):
#     global accesstoken
#     payload = {
#         "code": auth_login_production(account),
#         "app":"name_of_app"
#     }
#     req = requests.get(
#         url, params=payload)
#
#     accesstoken_dict = req.json()
#     accesstoken = accesstoken_dict['accessToken']
#     print("Granted access for Production:")
#     return accesstoken
# # to make Auth token generic
# def get_auth_token_production(account):
#     global AUTH_TOKEN_producation
#     AUTH_TOKEN_producation = auth_callback_api_production(account)
#     # global AUTH_TOKEN_producation
#     # AUTH_TOKEN_producation = auth_callback_api(account)
AUTH_TOKEN_producation="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzY3MzA4NDAsInNlc3Npb25fa2V5IjoiYzY5NDlhNzctMzU3ZS00MmY3LTlhOWEtMTdkY2UwMzhkMzhmIiwiZXhwIjoxNTM2ODE3MjQwfQ.acv20tpIcf08cJ2jX_UonnnATC363646uZ7P5SDX0q0"

AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzY3MzAyNDYsInNlc3Npb25fa2V5IjoiMmVkOTRjMmQtMTE2Mi00ZmRmLTk3MDEtNDMxNDhmMTdlOWJhIiwiZXhwIjoxNTM2ODE2NjQ2fQ.cXYR1WOgZl9aMj37bGNNsP8q_ubeR24sbEDjJ5dYupM"

def staging_data(body):
    #get_auth_token(User_details)
    #print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    #url = 'http://35.196.135.15/api/v2/kols/score/'
    #url = 'url'
    url = 'url'
    req = requests.post(url,data=json.dumps(body), headers=Header)

    #print("========================\n\n\n", url, Header, req.json())
    print("status : ", req.status_code)
    # print("Response Body : ",req)
    #pprint.pprint(json.dumps(req.json()))
    # clinicaltrials=0
    # publications=0
    # congresses=0
    # guidelines=0
    # societies=0
    list_dict_staging = []
    inno_data = req.json().get('data',[])
    for data in inno_data :
        # clinicaltrials=0
        # publications=0
        # congresses=0
        # guidelines=0
        # societies=0
        score_data_staging = data.get('scores',[])
        #for author_data in score_data_staging:
        for score_pro in score_data_staging:
            author_id_staging = score_pro.get('author_id','')
            list_staging.append(author_id_staging)
            score_profile = score_pro.get('score_profile','')
            clinicaltrials=0
            publications=0
            congresses=0
            guidelines=0
            societies=0
            for asset_cls in score_profile:
                asset_class_staging=asset_cls.get('asset_class','')
                if asset_class_staging == "clinicaltrials":
                    clinicaltrials= asset_cls.get('doc_count','')
                elif asset_class_staging == "guidelines":
                    guidelines=asset_cls.get('doc_count','')

                elif asset_class_staging == "congresses":
                    congresses=asset_cls.get('doc_count','')

                elif asset_class_staging == "publications":
                    publications=asset_cls.get('doc_count','')

                elif asset_class_staging == "societies":
                    societies = asset_cls.get('doc_count','')

            DOC_Count_staging=dict_staging(author_id_staging,publications,clinicaltrials,societies,congresses,guidelines)
            list_dict_staging.append(DOC_Count_staging)


    return list_staging,list_dict_staging


def producation_data(body_pro):
    #time.sleep(10)
    #get_auth_token_production(User_details)
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
        'content-type': 'application/json'
    }
    #import ipdb; ipdb.set_trace()
    #url='http://35.196.135.15/api/v2/kols/score/'
    url='url'
    req = requests.post(url,data=json.dumps(body_pro), headers=Header)
    #print("========================\n\n\n", url, Header, req.json())
    #print(url, req.json())

    print("status : ", req.status_code)
    list_dict_producation = []
    #print(json.dumps(req.json()))
    inno_data = req.json().get('data',[])
    for data in inno_data :
        score_data_producation = data.get('scores',[])
        #for author_data in score_data_producation:
        for score_pro in score_data_producation:
            author_id_producation = score_pro.get('author_id','')
            list_pro.append(author_id_producation)
            score_profile = score_pro.get('score_profile','')
            publications=0
            clinicaltrials=0
            congresses=0
            guidelines=0
            societies=0
            for asset_cls in score_profile:
                asset_class_producation=asset_cls.get('asset_class','')
                if asset_class_producation == "clinicaltrials":
                    clinicaltrials= asset_cls.get('doc_count','')

                elif asset_class_producation == "guidelines":
                    guidelines=asset_cls.get('doc_count','')

                elif asset_class_producation == "congresses":
                    congresses=asset_cls.get('doc_count','')

                elif asset_class_producation == "publications":
                    publications=asset_cls.get('doc_count','')

                elif asset_class_producation == "societies":
                    societies = asset_cls.get('doc_count','')

            DOC_Count_producation=dict_staging(author_id_producation,publications,clinicaltrials,societies,congresses,guidelines)
            list_dict_producation.append(DOC_Count_producation)


    return list_pro,list_dict_producation

def dict_staging(author_id,publications,clinicaltrials,societies,congresses,guidelines):

    DOC_Count={
    author_id: {
                "publications":publications,
                "clinicaltrials":clinicaltrials,
                "societies":societies,
                "congresses":congresses,
                "guidelines":guidelines
                }
            }

    return DOC_Count
def compare_score(DOC_Count_staging,DOC_Count_producation,author_id_producation,author_id_staging,TA,indication_list):

    normalisation_flag = set()
    porting_flag = set()
    congress_normalisation_flag = set()
    congress_porting_flag = set()
    ct_normalisation_flag = set()
    ct_porting_flag = set()
    for staging_meta in DOC_Count_staging:

        if staging_meta.get(author_id_staging) :

            author_id = staging_meta.keys()
            staging_name = author_name_staging(author_id)
            publication_staging=staging_meta.get(author_id_staging).get("publications")
            CT_staging=staging_meta.get(author_id_staging).get("clinicaltrials")
            societies_staging=staging_meta.get(author_id_staging).get("societies")
            congresses_staging=staging_meta.get(author_id_staging).get("congresses")
            guidelines_staging=staging_meta.get(author_id_staging).get("guidelines")
            total_count_staging= publication_staging+CT_staging+societies_staging+congresses_staging+guidelines_staging

    for production_meta in DOC_Count_producation :
        if production_meta.get(author_id_producation):

            author_id = production_meta.keys()
            producation_name = author_name_producation(author_id)
            publication_producation = production_meta.get(author_id_producation).get("publications")
            CT_producation = production_meta.get(author_id_producation).get("clinicaltrials")
            societies_producation = production_meta.get(author_id_producation).get("societies")
            congresses_producation = production_meta.get(author_id_producation).get("congresses")
            guidelines_producation = production_meta.get(author_id_producation).get("guidelines")
            total_count_producation = publication_producation+CT_producation+societies_producation+congresses_producation+guidelines_producation


    if publication_staging == publication_producation :
        doc_diff_flag = ''

    else :
        doc_diff_flag = "Publications"

        ids = ''.join(author_id)

        body = publication_es_query(ids , indication_list)
        response_staging = global_client.search(index="index_name", body = body)
        response_production = global_production.search(index="index_name", body = body)
        diff_pmids = compare_pmids(response_staging , response_production)
        for pmids in diff_pmids :
            body_es = es_query_pmids(pmids)
            response = global_production.search(index="index_name" , body = body_es)
            if response["hits"]["hits"] :
                for docs in response["hits"]["hits"] :
                    authors = docs.get('_source',{}).get('authors',[])
                    for author in authors :
                        if not author.get("author_id","") and author.get("ForeName",'') != 'not found' :
                            normalisation_flag.add(pmids)
            else :
                porting_flag.add(pmids)
    print(doc_diff_flag)

    if congresses_staging == congresses_producation :
        congress_diff_flag = ''

    else :
        congress_diff_flag = "Congress"
        ids = ''.join(author_id)

        body  = congress_es_query(ids , indication_list , TA)
        response_staging = global_client.search(index="index_name" , body = body)
        response_production = global_production.search(index="index_name", body = body)
        diff_congress_ids = compare_congress_ids(response_staging , response_production)
        for congress_ids  in diff_congress_ids :
            body_congress = es_query_congress_id(congress_ids)
            response = global_production.search(index="index_name" , body = body_congress)
            if response["hits"]["hits"] :
                for docs in response["hits"]["hits"] :
                    #if docs.get('_source',{}).get('congress_id','')== 'c2430ca1e40982b8@#@a97ab6aef3ff6e7d':
                        #import ipdb; ipdb.set_trace()
                    authors = docs.get('_source',{}).get('authors',[])
                    for author in authors :
                        if not author.get("author_id","") :
                            congress_normalisation_flag.add(congress_ids)
            else :
                congress_porting_flag.add(congress_ids)
    print(congress_diff_flag)
    if CT_staging == CT_producation :
        CT_diff_flag = ''

    else :
        CT_diff_flag  = "Clinical Trials"

        ids = ''.join(author_id)
        body = clinical_trails_es_query(ids , indication_list)
        response_staging = global_client.search(index = "index_name" , body = body)

        response_production = global_production.search(index = "index_name" , body = body)
        diff_trail_id = compare_trail_ids(response_staging , response_production)

        for trail_id in diff_trail_id :
            body_trail = es_query_using_trail_id(trail_id)


            response = global_production.search(index = "index_name",body = body)
            if response["hits"]["hits"]:
                for docs in response["hits"]["hits"] :
                    authors = docs.get('_source',{}).get('authors',[])
                    for author in authors :
                        if not author.get("author_id","") :
                            ct_normalisation_flag.append(trail_id)
            else :
                ct_porting_flag.append(trail_id)


    print(CT_diff_flag)
    if not normalisation_flag:
        normalisation_flag = None
    if not porting_flag :
        porting_flag = None
    if not congress_normalisation_flag :
        congress_normalisation_flag = None
    if not congress_porting_flag:
        congress_porting_flag = None
    if not ct_normalisation_flag :
        ct_normalisation_flag = None
    if not ct_porting_flag :
        ct_porting_flag = None
    asset_class_diff = doc_diff_flag + CT_diff_flag + congress_diff_flag
    print(staging_name)
    write_meta_csv(staging_name,author_id_staging,producation_name,author_id_producation,
                        publication_staging,publication_producation,normalisation_flag,porting_flag,\
                            CT_staging,CT_producation,ct_normalisation_flag,ct_porting_flag,\
                                guidelines_staging,guidelines_producation,congresses_staging,\
                                    congresses_producation,congress_normalisation_flag,congress_porting_flag,\
                                            societies_staging,societies_producation,asset_class_diff)

def compare_congress_ids(response_staging , response_production) :
    congress_ids_staging = []
    congress_ids_production = []

    for docs in response_staging["hits"]["hits"]:
        congress_ids = docs.get('_source',{}).get('congress_id','')
        congress_ids_staging.append(congress_ids)

    for docs in response_production["hits"]["hits"]:
        congress_ids = docs.get('_source',{}).get('congress_id','')
        congress_ids_production.append(congress_ids)

    difference_congress_ids = set(congress_ids_staging) - set(congress_ids_production)
    list(difference_congress_ids)
    return difference_congress_ids

def compare_trail_ids(response_staging , response_production) :
    trail_id_staging = []
    trail_id_production = []

    for docs in response_staging["hits"]["hits"]:
        clinical_id = docs.get('_source',{}).get("clinical_id",'')
        trail_id_staging.append(clinical_id)

    for docs in response_production["hits"]["hits"]:
        clinical_id = docs.get('_source',{}).get("clinical_id",'')
        trail_id_production.append(clinical_id)

    difference_trail_id = set(trail_id_staging) - set(trail_id_production)
    list(difference_trail_id)

    return difference_trail_id

def compare_pmids(response_staging , response_production):

    pmids_staging = []
    pmids_production = []

    for docs in response_staging["hits"]["hits"]:
        pmid = docs.get('_source',{}).get('publication_id','')
        pmids_staging.append(pmid)

    for docs in response_production["hits"]["hits"]:
        pmid = docs.get('_source',{}).get('publication_id','')
        pmids_production.append(pmid)


    difference_pmids = set(pmids_staging) - set(pmids_production)
    list(difference_pmids)
    return difference_pmids


def author_name_staging(list_staging):
    #difference=list(list_staging)
    #difference_str=",".join(difference)
    author_nm=''
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    for author_id in list_staging:
        params = {
             'author_ids': author_id
        }
        #url = 'http://35.196.135.15/api/v2/kols/list/'
        url = 'url/api/v2/kols/list/'
        req = requests.get(url,params,headers=Header)
        inno_data = req.json().get('data',[])
        for data in inno_data :
            author_nm = data.get('nm','')

        return author_nm
def author_name_producation(list_pro):
    author_nm=''
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
        'content-type': 'application/json'
    }
    for author_id in list_pro:
        params = {
             'author_ids': author_id
        }
        #url = 'http://35.196.135.15/api/v2/kols/score/'
        url = 'url/api/v2/kols/list/'
        req = requests.get(url,params,headers=Header)
        inno_data = req.json().get('data',[])
        for data in inno_data :
            author_nm = data.get('nm','')

        return author_nm
def get_publication_base_query(indication):
    base_query = [
                  {
                    "match_phrase":{
                      "article_title":indication
                    }
                  },
                  {
                    "match_phrase":{
                      "keywords":indication
                    }
                  },
                  {
                    "match_phrase":{
                      "abstract":indication
                    }
                  }]
    return base_query

def es_query_using_trail_id(trail_id) :
    query = {
              "sort": {
                "created_at": "desc"
              },
              "query" :{
                "match":{
                  "clinical_id": trail_id
                }
              }
            }

    return trail_id

def get_publication_query(indication_list):

    query = {
        "bool": {
            "should": []
        }
    }

    for indication in indication_list :
        query["bool"]["should"].extend(get_publication_base_query(indication))

    return query

def publication_es_query(author_id,indication) :
    query = {
              "query":{
                "bool":{
                  "must":[
                    # {
                    #   "bool":{
                    #     "should":[
                    #       {
                    #         "match_phrase":{
                    #           "article_title":indication
                    #         }
                    #       },
                    #       {
                    #         "match_phrase":{
                    #           "keywords":indication
                    #         }
                    #       },
                    #       {
                    #         "match_phrase":{
                    #           "abstract":indication
                    #         }
                    #       }]
                    #   }
                    # },
                    {
                      "bool":{
                        "should":[
                          {
                            "match":{
                              "authors.author_id":author_id
                            }
                          }]
                      }
                    }]
                }
              },
              "size":10000
            }
    query["query"]["bool"]["must"].append(get_publication_query(indication))
    return query

def es_query_pmids(pmid):

    query = {
              "sort":[
                {
                  "created_at":{
                    "order":"desc"
                  }
                }],
              "query": {
                "bool": {
                  "must": [
                    {
                      "match": {
                        "publication_id": pmid
                      }
                    }
                  ]
                }
              },
              "size": 1000
            }
    return query

def es_query_congress_id(congress_id) :
    body = {
              "sort": {
                "created_at": "desc"
              },
              "query" :{
                "match_phrase":{
                  "congress_id": congress_id
                }
              }
            }

    return body

def base_CT_es_query(indication):

    base_query = [
                {
                  "match_phrase":{
                    "detailed_description":indication
                  }
                },
                {
                  "match_phrase":{
                    "intervention.intervention_name":indication
                  }
                },
                {
                  "match_phrase":{
                    "intervention.description":indication
                  }
                },
                {
                  "match_phrase":{
                    "intervention.arm_group_label":indication
                  }
                },
                {
                  "match_phrase":{
                    "study_design.type":indication
                  }
                },
                {
                  "match_phrase":{
                    "study_design.value":indication
                  }
                },
                {
                  "match_phrase":{
                    "public_title":indication
                  }
                },
                {
                  "match_phrase":{
                    "scientific_title":indication
                  }
                },
                {
                  "match_phrase":{
                    "condition":indication
                  }
                },
                {
                  "match_phrase":{
                    "keyword":indication
                  }
                },
                {
                  "match_phrase":{
                    "official_title":indication
                  }
                },
                {
                  "match_phrase":{
                    "abstract":indication
                  }
                }
                ]
    return base_query

def get_ct_query(indication_list):

    query = {
         "bool" :{

         "should":[]
                 }

            }

    for indication in indication_list :
        query["bool"]["should"].extend(get_publication_base_query(indication))

    return query


def clinical_trails_es_query(author_id , indication) :

    query = {
              "query":{
                "bool":{
                  "must":[
                    {
                      "bool":{
                        "should":[
                          {
                          "nested":{
                                "path":"authors",
                                "query":{
                                  "match":{
                                    "authors.author_id":author_id
                                  }
                                }
                              }
                          }]
                      }
                    }]
                }
              },
              "size":10000,
              "_source":"clinical_id"
            }

    query["query"]["bool"]["must"].append(get_ct_query(indication))
    return query

def get_congresses_base_query(indication, TA):
    base_query = [
      {
        "match_phrase":{
          "classification.indications":indication
        }
      },
      {
        "match_phrase":{
          "title":indication
        }
      },
      {
        "match_phrase":{
          "abstract":indication
        }
      },
      {
        "match_phrase":{
          "classification.TA":TA
        }
      }
    ]
    return base_query

def get_congresses_query(indications_list, TA):
    query = {
        "bool": {
            "should": []
        }
    }
    for i in indications_list:
        query["bool"]["should"].extend(get_congresses_base_query(i, TA))
    return query

# MAPPING = {
#     "clinicaltrials": get_congresses_query
# }

# for asset_class in asset_classes:
#     query = MAPPING["asset_class"](indications_list,TA)
def congress_es_query(author_id , indication , TA) :

    query = {
              "query":{
                "bool":{
                  "must":[
                    {
                      "bool":{
                        "should":[
                          {
                          "nested":{
                            "path":"authors",
                            "query":{
                              "match":{
                                "authors.author_id":author_id
                                    }
                                }
                              }
                          }]
                      }
                    }]
                }
              },
              "size":10000,
              "_source":"congress_id"
            }
    query["query"]["bool"]["must"].append(get_congresses_query(indication, TA))
    return query

def others_es_query(indication , TA , author_id) :
    query = {
              "query":{
                "bool":{
                  "must":[
                    {
                      "bool":{
                        "should":[
                          {
                            "match_phrase":{
                              "classification.TA":TA
                            }
                          }]
                      }
                    },
                    {
                      "bool":{
                        "should":[
                          {
                            "match_phrase":{
                              "classification.indications":indication
                            }
                          }]
                      }
                    },
                    {
                      "bool":{
                        "should":[
                          {
                            "query":{
                              "nested":{
                                "path":"authors",
                                "query":{
                                  "match":{
                                    "authors.author_id":author_id
                                  }
                                }
                              }
                            }
                          }]
                      }
                    }]
                }
              },
              "size":10000,
              "_source":"application_id"
            }
    return query

def perform_check(list_pro,list_staging):

    status_flag = 'Matching'
    x = set(list_pro)-set(list_staging)
    y = set(list_staging)-set(list_pro)
    #len(x)
    print("length",len(y))
    print(y)
    print("Diff in staging and producation",y)

    for list1 in list_staging:
        for list2 in list_pro:
            if list1 == list2:
                val_1=list_staging.index(list1)
                val_2=list_pro.index(list2)


                if val_1 == val_2 :
                    status_flag = 'Matching'
                else :
                    status_flag = 'Not Matching'

                staging_name = author_name_staging(list1)

                producation_name = author_name_producation(list2)
                write_csv(staging_name , list1 , val_1 ,val_2,status_flag)
                print(val_1,list1,"-----",val_2,list2)

                print(list1,val_1,"-----",val_2,list2)


    return val_1 , val_2 , status_flag

def write_meta_csv(author_name_staging, author_id_staging, author_name_producation ,author_id_production,staging_pub,production_pub,normalisation_flag,porting_flag,staging_CT,production_CT,ct_normalisation_flag,ct_porting_flag,staging_Guideline,production_guideline,staging_congress,production_congress,congress_normalisation_flag,congress_porting_flag,staging_socities,production_socities,asset_class_diff):
    file_exists = os.path.isfile(out_filename1)
    with open(out_filename1, 'a') as csvfile:
        feild_names = ['Author name Staging','Author ID Staging','Author name Producation','Author ID Producation','Staging Publication','Producation Publication',\
                            'PMIDs not normalised','PMIDS not ported','Staging CT','Producation CT','Trail id not normalised','Trail ids not ported','Staging Guideline','Producation Guideline','Staging Congress',\
                                'Producation Congress','Congresses not normalised','Congresses not ported','Staging Socities','Producation Socities','Asset Classes having diff']

        writer = csv.DictWriter(csvfile, fieldnames=feild_names)
        if not file_exists :
            writer.writeheader()
        writer.writerow({'Author name Staging':author_name_staging,'Author ID Staging':author_id_staging,'Author name Producation':author_name_producation,'Author ID Producation':author_id_production,'Staging Publication':staging_pub,'Producation Publication':production_pub,'PMIDs not normalised':normalisation_flag,'PMIDS not ported':porting_flag,'Staging CT':staging_CT,'Producation CT':production_CT,'Trail id not normalised':ct_normalisation_flag,'Trail ids not ported':ct_porting_flag,'Staging Guideline':staging_Guideline,                                                         'Producation Guideline':production_guideline,'Staging Congress':staging_congress,'Producation Congress':production_congress,                                                             'Congresses not normalised': congress_normalisation_flag,'Congresses not ported':congress_porting_flag,'Staging Socities':staging_socities,'Producation Socities':production_socities,                                                            'Asset Classes having diff':asset_class_diff})

def write_csv(author_name_staging,author_id_staging,position_staging,position_production,matching_flag):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author name Staging','Staging author id','Mapping staging','Mapping Production','Matching flag']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author name Staging':author_name_staging,'Staging author id':author_id_staging, 'Mapping staging':position_staging,\
                               'Mapping Production':position_production,\
                                'Matching flag':matching_flag})



def run_test_cases():

    list_staging,list_dict_staging=staging_data(TEST_CASES["Oncology"])
    # print("=====================================================================================")
    # print("staging list",len(list_staging))
    # print(list_staging)
    # print("=====================================================================================")

    list_pro,list_dict_producation=producation_data(TEST_CASES_pro["Oncology"])
    # print("=====================================================================================")
    # print("producation list",len(list_pro))
    # print(list_pro)
    # print("=====================================================================================")
    #print(list_staging, "==================\n",list_pro)
    perform_check(list_pro,list_staging)
    #import ipdb; ipdb.set_trace()
    author_name_staging(list_staging)
    author_name_producation(list_pro)
    indication_list =[]
    dict_val  = TEST_CASES["Oncology"]
    TA = dict_val.get('TA','')
    for indication in dict_val.get('filters',{}).get('Indications',{}).get('values',[]) :
        indication_list.append(indication)

    for author_id_staging in list_staging :
        for author_id_producation in list_pro :
            if author_id_staging == author_id_producation :
                compare_score(list_dict_staging,list_dict_producation,author_id_staging,author_id_producation,TA,indication_list)


if __name__ == '__main__':
    run_test_cases()
