from account import User_details, User_details
import requests
from body import TEST_CASES
from body_pro import TEST_CASES_pro
import json
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
import time


global_client = elastic_Search_ip)
global_production = elastisearch_ip
import re
import sys
import pprint
#================================================================================
filename="staging_prod_diff_.csv"
list_filename=filename.split('.')
now = time.strftime('%d-%m-%Y_%H:%M:%S')
out_filename=list_filename[0]+now+'.'+list_filename[1]
print("========================================================================")
print("Your output file name is --->",out_filename)
print("========================================================================")
arg_var_TA = sys.argv[1]
print("TA is --->",sys.argv[1])
print("========================================================================")
# arg_var_asset_class = sys.argv[2]
# asset_class_list=sys.argv[2].split(",")
# print("These are asset class list-->",asset_class_list)
# print("========================================================================")
# arg_var_indications = sys.argv[2]
# print("Indications are--->",sys.argv[2])
# print("========================================================================")
# arg_var_country = sys.argv[3]
# print("Country is ---->",sys.argv[3])
# print("========================================================================")
# arg_var_count= int(sys.argv[4])
# print("Script will run ",int(sys.argv[4]),"Times")
# print("========================================================================")

#================================================================================

KOL_ID="60aa0668880a4e43b200ae63456fb16e"
list_staging=[]
list_pro=[]
Application's_url
pags = ''
def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        'application_callback_url', params=payload)
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])

    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    print("Granted access for staging")

    return accesstoken
# to make Auth token generic
def get_auth_token(account):

    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)

    return AUTH_TOKEN

#============================================================================
def auth_login_production(account):

    r = requests.post('url', json=account)

    dict = r.json()

    code = dict['responseData']['callback_url']

    codes = code[26:]
    return codes
def auth_callback_api_production(account):
    global accesstoken
    payload = {
        "code": auth_login_production(account),
        "app":"name_of_app"
    }
    req = requests.get(
        url, params=payload)

    accesstoken_dict = req.json()
    accesstoken = accesstoken_dict['accessToken']
    print("Granted access for Production:")
    return accesstoken
# to make Auth token generic
def get_auth_token_production(account):
    global AUTH_TOKEN_producation
    AUTH_TOKEN_producation = auth_callback_api_production(account)

    return AUTH_TOKEN_producation
    # global AUTH_TOKEN_producation
    # AUTH_TOKEN_producation = auth_callback_api(account)
def staging_score(body,AUTH_TOKEN):

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    url = 'url'
    req = requests.post(url,data=json.dumps(body), headers=Header)
    print("status : ", req.status_code)
    print("Fetched Top 200 KOLs : ",req)
    #pprint.pprint(json.dumps(req.json()))
    print(json.dumps(body))

    list_dict_staging = []
    inno_data = req.json().get('data',[])

    for data in inno_data :
        score_data_staging = data.get('scores',[])
        #for author_data in score_data_staging:
        count = 0
        for score_pro in score_data_staging:
            count+=1
            author_id_staging = score_pro.get('author_id','')
            list_dict_staging.append(author_id_staging)
            if count == 200:
                break

        #print(list_dict_staging)
        return list_dict_staging
def staging_personal_info(list_dict_staging,AUTH_TOKEN):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    BASE_API = 'url'
    #response = {}
    dict_kols_info = {}
    count = 0
    if list_dict_staging:
        for KOL_ID in list_dict_staging:
            count = count + 1
            print(KOL_ID,"==========staging personal_info=================",count)
            url = BASE_API + KOL_ID +'/personal_info'
            req = requests.get(url,headers=Header)
            #print("personal info",req.status_code)
            #print(json.dumps(req.json()))
            if req.status_code == 200:
                innoData = req.json()['data'].get('innoData', {})
                #print("========================kkkkk")
                current_email = innoData.get('emails',{})
                #print(current_email)
                list_email = []
                for em in current_email:
                    email=em.get('email',[])
                    if email:
                        list_email.append(email)
                #print(list_email)
                current_phone= innoData.get('phone_numbers',{})
                #print(current_phone)
                list_phone=[]
                for pho in current_phone:
                    phone_number=pho.get('phone',[])
                    if phone_number:
                        phone_number=re.sub(r'[^\w]', ' ', phone_number)
                        phone="".join(filter(lambda x: x.isdigit(),phone_number))
                        list_phone.append(phone)
                #print(list_phone)

                current_address= innoData.get('addresses',{})
                #print(current_address)
                list_address=[]
                for addr in current_address:
                    address=addr.get('address',[])
                    if address:
                        list_address.append(address)
                #print(list_address)
            #===========================================================
            url = BASE_API + KOL_ID + '/basic_info'
            req = requests.get(url,headers=Header)
            #print(req)
            #print(req.status_code)
            if req.status_code == 200:
                innoAffi = req.json()
                #print(innoAffi)
                Name = innoAffi.get('nm')
                desig=innoAffi.get('designation')
                # designation = ''
                # if desig:
                #     designation=re.sub(r'[^a-z]', '', desig.lower())
                affi=innoAffi.get('aff')
                kol_author_id = innoAffi.get('kolkey')
                country=innoAffi.get('macnty')
                kol_edu=innoAffi.get('education')
                t=innoAffi.get('title')

                dict_kols_info[KOL_ID]=[list_email,list_phone,list_address,Name,desig,affi,kol_author_id,country,kol_edu,t]
            else:
                dict_kols_info[KOL_ID] = [[],[],[],'','','','','','','']

            #print(dict_kols_info)
            if count == 200:
                #print("=========================", count)
                break

    return dict_kols_info


#=================producation=============================================
def producation_score(body,AUTH_TOKEN_producation):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
        'content-type': 'application/json'
    }
    url = 'url'
    req = requests.post(url,data=json.dumps(body), headers=Header)
    #print(json.dumps(body))
    print("status : ", req.status_code)
    print("Response Body : ",req)
    #pprint.pprint(json.dumps(req.json()))

    list_dict_producation = []
    inno_data = req.json().get('data',[])
    for data in inno_data :
        score_data_staging = data.get('scores',[])
        #for author_data in score_data_staging:
        count = 0
        for score_pro in score_data_staging:
            count+=1
            author_id_staging = score_pro.get('author_id','')
            list_dict_producation.append(author_id_staging)
            if count == int(sys.argv[4]):
                break

        #print(list_dict_staging)
        return list_dict_producation
def producation_personal_info(list_dict_producation,AUTH_TOKEN_producation):
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
        'content-type': 'application/json'
    }
    BASE_API = 'url/api/v2/kols/profile/'
    #response = {}
    dict_kols_info_producation = {}
    count = 0
    if list_dict_producation:
        for KOL_ID in list_dict_producation:
            count+=1
            print(KOL_ID,"==========producation personal_info=================",count)
            url = BASE_API + KOL_ID +'/personal_info'
            req = requests.get(url,headers=Header)
            print(req.status_code)
            #print(json.dumps(req.json()))
            if req.status_code == 200:
                innoData = req.json()['data'].get('innoData', None)

                current_email = innoData.get('emails')
                list_email = []
                for em in current_email:
                    email=em.get('email',[])
                    if email:
                        list_email.append(email)
                #print(list_email)
                current_phone= innoData.get('phone_numbers')
                list_phone=[]
                for pho in current_phone:

                    phone_number=pho.get('phone')
                    if phone_number:
                        phone_number=re.sub(r'[^\w]', ' ', phone_number)
                        phone="".join(filter(lambda x: x.isdigit(),phone_number))
                        list_phone.append(phone)
                #print(list_phone)

                current_address= innoData.get('addresses')
                list_address=[]
                for addr in current_address:
                    address=addr.get('address')
                    if address:
                        list_address.append(address)
                #print(list_address)
                #==================================================================
                #print(KOL_ID,"==========producation profile=================",count)
                url = BASE_API + KOL_ID + '/basic_info'
                req = requests.get(url,headers=Header)
                #print(req)
                #print(req.status_code)
                if req.status_code == 200:
                    innoAffi = req.json()
                    #print(innoAffi)
                    Name = innoAffi.get('nm')
                    desig=innoAffi.get('designation')
                    # designation = ''
                    # if desig:
                    #     designation=re.sub(r'[^a-z]', '', desig.lower())
                    affi=innoAffi.get('aff')
                    kol_author_id = innoAffi.get('kolkey')
                    country=innoAffi.get('macnty')
                    kol_edu=innoAffi.get('education')
                    t=innoAffi.get('title')
                    #print(Name,desig,affi,kol_author_id,country,kol_edu,t)
                    #return Name,desig,affi,kol_author_id,country,kol_edu,t


                dict_kols_info_producation[KOL_ID]=[list_email,list_phone,list_address,Name,desig,affi,kol_author_id,country,kol_edu,t]
            else:
                dict_kols_info_producation[KOL_ID]=[[],[],[],'','','','','','','']
                print("dammmmmmmmmm")


            if count == 200:
                break


    return dict_kols_info_producation



def compare_email(staging_email,producation_email):

    count = 0
    for ema in staging_email:
        for emai in producation_email:
            if ema == emai:
                count+=1

    if count == len(staging_email) or ema == 'NA':
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'

    return status_flag

def compare_phone(staging_phone,producation_phone):
    count = 0
    for pho in staging_phone:
        for ph in producation_phone:
            if pho in ph:
                count+=1

    if count == len(staging_phone):
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag
def compare_address(staging_address,producation_address):

    count = 0
    for ema in staging_address:
        for emai in producation_address:
            if ema == emai:
                count+=1

    if count == len(staging_address):
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'

    return status_flag

def compare_designation(staging_designation,producation_designation):


    status_flag = 'Matching'
    if staging_designation == producation_designation:
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'

    return status_flag
def compare_affiliation(staging_designation,producation_designation):

    status_flag = 'Matching'
    if staging_designation == producation_designation:
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag
def compare_country(staging_country,producation_country):
    status_flag = 'Matching'
    if staging_country == producation_country:
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag
def compare_education(staging_education,producation_education):
    status_flag = 'Matching'
    if staging_education == producation_education:
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag

def compare_title(staging_title,producation_title):
    status_flag = 'Matching'
    if staging_title == producation_title:
        status_flag = 'Matching'
    else:
        status_flag = 'Not Matching'
    return status_flag


def run_test_cases():

    AUTH_TOKEN=get_auth_token(User_details)

    AUTH_TOKEN_producation=get_auth_token_production(User_details)
    list_dict_staging=staging_score(TEST_CASES["Diff"],AUTH_TOKEN)

    dict_kols_info=staging_personal_info(list_dict_staging,AUTH_TOKEN)


    list_dict_producation=producation_score(TEST_CASES["Diff"],AUTH_TOKEN_producation)

    dict_kols_info_producation=producation_personal_info(list_dict_producation,AUTH_TOKEN_producation)


    for key in dict_kols_info.keys():
        staging_email=dict_kols_info[key][0]
        producation_email=dict_kols_info_producation[key][0]
        result_email=compare_email(staging_email,producation_email)


        staging_phone=dict_kols_info[key][1]
        producation_phone=dict_kols_info_producation[key][1]
        result_phone=compare_phone(staging_phone,producation_phone)

        staging_address=dict_kols_info[key][2]
        producation_address=dict_kols_info_producation[key][2]
        result_address=compare_address(staging_address,producation_address)

        staging_name=dict_kols_info[key][3]
        producation_name=dict_kols_info_producation[key][3]

        staging_designation=dict_kols_info[key][4]
        producation_designation=dict_kols_info_producation[key][4]
        result_designation=compare_designation(staging_designation,producation_designation)


        stag_affiliation=dict_kols_info[key][5]
        if stag_affiliation:
            kol_aff = re.sub(r'[^\w]',' ',stag_affiliation)
            staging_affiliation=re.sub(r'[^a-z]', '', kol_aff.lower())

        prod_affiliation=dict_kols_info_producation[key][5]
        pro_aff = re.sub(r'[^\w]',' ',prod_affiliation)
        producation_affiliation=re.sub(r'[^a-z]', '', pro_aff.lower())
        result_affiliation=compare_affiliation(staging_affiliation,producation_affiliation)

        staging_author_id=dict_kols_info[key][6]
        producation_author_id=dict_kols_info_producation[key][6]

        staging_country=dict_kols_info[key][7]
        producation_country=dict_kols_info_producation[key][7]
        result_country=compare_country(staging_country,producation_country)

        staging_education=dict_kols_info[key][8]
        producation_education=dict_kols_info_producation[key][8]
        result_education=compare_education(staging_education,producation_education)

        staging_title=dict_kols_info[key][9]
        producation_title=dict_kols_info_producation[key][9]
        result_title=compare_title(staging_title,producation_title)


        write_csv(staging_author_id,producation_author_id,staging_name,producation_name,staging_email,producation_email,result_email,staging_phone,producation_phone,result_phone,staging_address,producation_address,result_address,staging_designation,producation_designation,result_designation,stag_affiliation,prod_affiliation,result_affiliation,staging_country,producation_country,result_country,staging_education,producation_education,result_education,staging_title,producation_title,result_title)


def write_csv(staging_author_id,producation_author_id,staging_name,producation_name,staging_email,producation_email,result_email,staging_phone,producation_phone,result_phone,staging_address,producation_address,result_address,staging_designation,producation_designation,result_designation,stag_affiliation,prod_affiliation,result_affiliation,staging_country,producation_country,result_country,staging_education,producation_education,result_education,staging_title,producation_title,result_title):
        file_exists=os.path.isfile(out_filename)

        with open(out_filename, 'a') as csvfile:
            fieldnames = ['Staging id','Producation id','Staging Name','Producation Name','Staging Email', 'Producation Email', 'Email Flag', 'Staging Phone','Producation Phone', 'Phone Flag','Staging Address','Producation Address','Address Flag','Staging Designation','Producation Designation','Designation Flag','Staging Affiliation','Producation Affiliation','Affiliation Flag','Staging country','Producation country','Country Flag','Staging Education','Producation Education','Education Flag','Staging Title','Producation Title','Title Flag']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Staging id':staging_author_id,'Producation id':producation_author_id,'Staging Name':staging_name,'Producation Name':producation_name,'Staging Email':staging_email,'Producation Email':producation_email,'Email Flag':result_email,'Staging Phone':staging_phone,'Producation Phone':producation_phone,'Phone Flag':result_phone,'Staging Address':staging_address,'Producation Address':producation_address,'Address Flag':result_address,'Staging Designation':staging_designation,'Producation Designation':producation_designation,'Designation Flag':result_designation,'Staging Affiliation':stag_affiliation,'Producation Affiliation':prod_affiliation,'Affiliation Flag':result_affiliation,'Staging country':staging_country,'Producation country':producation_country,'Country Flag':result_country,'Staging Education':staging_education,'Producation Education':producation_education,'Education Flag':result_education,'Staging Title':staging_title,'Producation Title':producation_title,'Title Flag':result_title})

if __name__ == '__main__':
    run_test_cases()



# def staging_profile(list_dict_staging):
#     Header = {
#         'Authorization': 'Bearer ' + AUTH_TOKEN,
#         'content-type': 'application/json'
#     }
#     BASE_API = 'url'
#     dict_kols_profile = {}
#     count = 0
#     if list_dict_staging:
#         for KOL_ID in list_dict_staging:
#             count+=1
#             print(KOL_ID,"==========staging profile=================",count)
#             url = BASE_API + KOL_ID + '/basic_info'
#             req = requests.get(url,headers=Header)
#             #print(req)
#             #print(req.status_code)
#             if req.status_code == 200:
#                 innoAffi = req.json()
#                 #print(innoAffi)
#                 Name = innoAffi.get('nm')
#                 desig=innoAffi.get('designation')
#                 # designation = ''
#                 # if desig:
#                 #     designation=re.sub(r'[^a-z]', '', desig.lower())
#                 affi=innoAffi.get('aff')
#                 kol_author_id = innoAffi.get('kolkey')
#                 country=innoAffi.get('macnty')
#                 kol_edu=innoAffi.get('education')
#                 t=innoAffi.get('title')
#                 dict_kols_profile[KOL_ID] = [Name,desig,affi,kol_author_id,country,kol_edu,t]
#             else:
#                 dict_kols_profile[KOL_ID] = ['','','','','','','']
#
#     return dict_kols_profile
# def producation_profile(list_dict_producation):
#     Header = {
#         'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
#         'content-type': 'application/json'
#     }
#     BASE_API = 'url/api/v2/kols/profile/'
#     dict_kols_profile_producation = {}
#     count = 0
#     if list_dict_producation:
#         for KOL_ID in list_dict_producation:
#             count+=1
#             print(KOL_ID,"==========producation profile=================",count)
#             url = BASE_API + KOL_ID + '/basic_info'
#             req = requests.get(url,headers=Header)
#             #print(req)
#             #print(req.status_code)
#             if req.status_code == 200:
#                 innoAffi = req.json()
#                 #print(innoAffi)
#                 Name = innoAffi.get('nm')
#                 desig=innoAffi.get('designation')
#                 # designation = ''
#                 # if desig:
#                 #     designation=re.sub(r'[^a-z]', '', desig.lower())
#                 affi=innoAffi.get('aff')
#                 kol_author_id = innoAffi.get('kolkey')
#                 country=innoAffi.get('macnty')
#                 kol_edu=innoAffi.get('education')
#                 t=innoAffi.get('title')
#                 #print(Name,desig,affi,kol_author_id,country,kol_edu,t)
#                 #return Name,desig,affi,kol_author_id,country,kol_edu,t
#                 dict_kols_profile_producation[KOL_ID] = [Name,desig,affi,kol_author_id,country,kol_edu,t]
#
#             else:
#                 dict_kols_profile_producation[KOL_ID]=['','','','','','','']
#             if count == 200:
#                 break
#
#     return dict_kols_profile_producation
