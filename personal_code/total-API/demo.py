import yaml
import yamlordereddictloader
import json
import requests
import login
from account import User_details
from body import TEST_CASES

#sys.setdefaultencoding("utf-8")

#BASE_API="url/api/v2"
#AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzQyMjc3MjIsInNlc3Npb25fa2V5IjoiMDc1ZDAyMzUtOGFiZi00Mzc4LTlhZjctZmRlMGRkOGY0MzYyIiwiZXhwIjoxNTM0MzE0MTIyfQ.1yGP_xJj1gh8jL7nuAS9A_RA-35gPSbcKncQ6in08jI"
def auth_login(account):
    #QA_client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X
    #Staging_client_id=8mw9DF9xJSq9Kf6GXkQjjAb11rfOt6P7AoD3hUTR----32
    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())

    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[29:]
    # print(codes)
    # print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        QA_link
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    return AUTH_TOKEN
def read_yaml():
    with open("swagger.yaml") as f:
        yaml_data = yaml.load(f, Loader=yamlordereddictloader.Loader)
        #print(json.dumps(yaml_data))
        #docs = json.dumps(yaml_data)
        for key,value in yaml_data.items():
            if key == "host":
                BASE_API_1= value
                print(BASE_API_1)
        for key,value in yaml_data.items():
            if key == "basePath":
                BASE_API_2=value
                BASE_API=BASE_API_1+BASE_API_2
        #import ipdb; ipdb.set_trace()
        method_dict={}
        for key, value in yaml_data.items():
            if key == "paths":
                for key, value in yaml_data[key].items():
                    url=BASE_API + key
                    # print(url)
                    # print(json.dumps(value))
                    for key,value in value.items():
                        method_type=key
                        body=value
                        #import ipdb; ipdb.set_trace()
                        #print(body,'===========')
                        test_case_name=(body["summary"])
                        try:
                            params=(body["parameters"])
                        except(KeyError):
                            params=(body["paramters"])

                        # print(params)
                        # print(method_type)
                        # print(test_case_name)
                        method_dict[url]=[method_type,test_case_name,params]
                        # if method_type == 'get':
                        #     url=get_methods(url)
                        # if method_type == 'post':
                        #     url=post_method(url)
        #print(json.dumps(method_dict))
        return method_dict


def get_methods(method_dict):
    #print(json.dumps(method_dict,indent=4))
    AUTH_TOKEN = get_auth_token(User_details)

    Header = {
        'Authorization':'Bearer ' + AUTH_TOKEN,
        #.encode('utf-8'),
        'content-type':'application/json'
    }
    #import ipdb; ipdb.set_trace()

    for key,value in method_dict.items():
        method_type=value[0]
        test_case_name=value[1]
        param=value[2]
        import ipdb; ipdb.set_trace()
        if method_type == 'post':
            Header = {
                        'Authorization': 'Bearer ' + AUTH_TOKEN,
                        'content-type': 'application/json'
                    }
            params = {

                    'dc': 'as'
                }
            body = json.dumps(TEST_CASES["Interactions"])
            req = requests.post(key,params=params, data=body, headers=Header)
            print("Scenario status : ", req.status_code)

        # if method_type == 'get':
        #     params={
        #         "group": "my",
        #         "from": 0,
        #         "limit": 10
        #     }
        #     req = requests.get(key.format(id='60aa0668880a4e43b200ae63456fb16e'),params,headers=Header)
        #     if req.status_code == 200:
        #         print("Test Case passed----------------",req.status_code,key)
        #     elif req.status_code == 404:
        #         print("No data present-------------",req.status_code,key.format(id='60aa0668880a4e43b200ae63456fb16e'))
        #     else:
        #         print("something else-------------",req.status_code,key)


            #print(key,"================", req.status_code)
        #if method_type == 'post':


# {
#     "dataType": "Public",
#     "from": 0,
#     "limit": 10
# }
# [["name", "group"], ["in", "query"], ["type", "string"], ["enum", ["org", "my"]], ["required", false]]'
#



if __name__ == '__main__':
    method_dict=read_yaml()
    get_methods(method_dict)





# for doc in docs:
#     for k,v in doc.items():
#         print k, "->", v
#     print "\n",
