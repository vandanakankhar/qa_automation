TEST_CASES={"Interactions":{
  "interactionTitle": "New Drug Trial Request",
  "interactionDetails": "For Evofosfamide",
  "intervention": "Evofosfamide",
  "note": "",
  "interactionType": "Business Lunch/Dinner",
  "start": "2016-04-25T08:00:00.000Z",
  "end": "2016-04-25T09:00:00.000Z",
  "country": "India",
  "city": "New Delhi",
  "venueDetails": "Hauz Khas, New Delhi",
  "attachments": [
    "strategy.doc-1461588510445"
  ],
  "ktls": {
    "nm": "Emily Sommers",
    "kolkey": "726e8c26-0e32-4d30-9cd2-1e623b2a7ccd",
    "innoKOLs": True
  }
},
"Private_event":{
  "eventTitle": "Duke Cancer Research Findings",
  "eventDetails": "New drug trial research and developments",
  "eventType": "Scientific lecture",
  "eventUrl": "https://www.dukeuniversity.com/events/major",
  "start": "2016-04-25T08:00:00.000Z",
  "end": "2016-04-25T09:00:00.000Z",
  "audienceType": "Mixed audience",
  "audienceSize": "100-200",
  "country": "United States Of America",
  "city": "Durham",
  "venueDetails": "Duke University, North Carolina",
  "attachments": [
    "strategy.doc-1461588510445"
  ],
  "eventCoOrdinator": {
    "name": "Harrison Skylet",
    "email": "harrison.skylet@application.com"
  },
  "speakers": [
    {
      "nm": "Ruth Ann Marrie",
      "kolkey": "b24110bfc3c14e94bfae1f192907eb33",
      "status": "string",
      "innoKOLs": True
    }
  ]
}}
