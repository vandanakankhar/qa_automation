import requests
from account import User_details, User_details
import json
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
import time
import re
import sys
import pprint
list_staging=[]
list_pro=[]
Application's_url
pags = ''
# reload(sys)
# sys.setdefaultencoding("utf-8")
#AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mjc1ODMyMTIsInNlc3Npb25fa2V5IjoiZTQwNDhhYjMtMWMzOC00ZTYzLWE4M2YtZTE5Y2FjYzFlMzc5IiwiZXhwIjoxNTI3NjY5NjEyfQ.AzJx7t3tipVjSmaDrt5N2AX83Cu0-aDjtlLud3qbUiU"
# filename="filter.csv"
# list_filename=filename.split('.')
# now = time.strftime('%d-%m-%Y %H:%M:%S')
# out_filename=list_filename[0]+'.'+list_filename[1]
out_filename="output.csv"

def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    print(r.status_code)

    # pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):

    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get('application_callback_url', params=payload)
    print(req.status_code)

    accesstoken_dict = req.json()
    # print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    return AUTH_TOKEN

def read_csv(input_file):
    get_auth_token(User_details)
    input_file = "author_id.csv"
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0
        for line in reader  :
            author_id=line.get('Author_id')
            count=count + 1
            if count<=61:
                continue
            print(count)
            research_activity(author_id)


def research_activity(author_id):
    print(author_id)
    AUTH_TOKEN=get_auth_token(User_details)

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    url = BASE_API + 'kols/profile/' + author_id + '/research_activity'
    res = requests.get(url, params, headers=Header)
    print("status : ", res.status_code)
    inno_data = res.json().get('data',[])
    #print(json.dumps(inno_data))

    for dict in inno_data:
        count=dict.get('count')
        name=dict.get('name')
        #.encode('utf-8'))
        print(name)
        docs=dict.get('docs')
        Societies=[]
        Advocacy=[]
        Hospital=[]
        HTA=[]
        Regulatory_Bodies=[]
        Guidelines=[]
        Diagnostic_center = []
        for doc in docs:
            title_societies=0
            title_advocacy=0
            title_hospital=0
            title_HTA=0
            title_Regulatory_bodies=0
            title_guidlines=0
            title_diag_centers = 0
            # if name == 'Publications':
            #     title_publication=doc.get('article_title')
            #     print(name,"=======",count)
            #     print(title_publication)
            # if name == 'Clinical Trials':
            #     title_clinical_trial=doc.get('public_title')
            #     print(name,"=====================",count)
            #     print(title_clinical_trial)
            # if name == 'Congresses':
            #     title_congress=doc.get('title')
            #     print(name,"=====================",count)
            #     print(title_congress)
            if name == 'Societies':
                title_societies=doc.get('body_name')
                Societies.append(title_societies)
            if name == 'Guidelines':
                title_guidlines=doc.get('title')
                Guidelines.append(title_guidlines)
            if name == 'Advocacy':
                title_advocacy=doc.get('body_name')
                Advocacy.append(title_advocacy)
            if name == 'Regulatory bodies':
                title_Regulatory_bodies=doc.get('body_name')
                Regulatory_Bodies.append(title_Regulatory_bodies)
            if name == 'HTA':
                title_HTA=doc.get('body_name')
                HTA.append(title_HTA)
            if name == 'Top Hospital KOLs':
                title_hospital=doc.get('body_name')
                Hospital.append(title_hospital)
            if name == 'Diagnostic Centers':
                title_diag_centers=doc.get('body_name')
                Diagnostic_center.append(title_diag_centers)

        write_csv(author_id,name,count,Societies,Guidelines,Advocacy,Regulatory_Bodies,HTA,Hospital,Diagnostic_center)
def write_csv(author_id,name,count,Societies,Guidelines,Advocacy,Regulatory_Bodies,HTA,Hospital,Diagnostic_center):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author id','Name','Count','Societies','Guidelines','Advocacy','Regulatory Body','HTA','Hospital','Diagnostic Center']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author id':author_id,'Name':name,'Count':count,'Societies':Societies,'Guidelines':Guidelines,'Advocacy':Advocacy,'Regulatory Body':Regulatory_Bodies,'HTA':HTA,'Hospital':Hospital,'Diagnostic Center':Diagnostic_center})

if __name__ == '__main__':

    read_csv("author_id.csv")
