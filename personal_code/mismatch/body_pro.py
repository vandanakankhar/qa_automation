import sys
TEST_CASES_pro = {
    "Diff" :{
  "from": 0,
  "limit": 10,
  "TA":  sys.argv[1],
  "asset_class": "Top KOLs",
  "filters": {
    "kol_info": [
      {
        "name": "Publications",
        "value": 15,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Clinical Trials",
        "value": 13,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Congresses",
        "value": 12,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Guidelines",
        "value": 10,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Regulatory Bodies",
        "value": 5,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "HTA",
        "value": 5,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Societies",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Advocacy",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Top Hospital KOLs",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Diagnostic Centers",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      }
    ],
    "showScore": True,
    "kolType": "allkols",
    "Indications": {
      "values": sys.argv[2].split(","),
      "isAnd": True
    },
  "Countries": {
      "values": sys.argv[3].split(","),
      "isAnd": False
    }
  }
}
}
