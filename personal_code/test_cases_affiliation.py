import random
TEST_ORGANIZATIONS = [

    'Organization'+ str(random.randint(1, 100)) +'1',
    'Organization'+ str(random.randint(1, 100)) +'2',
    'Organization'+ str(random.randint(1, 100)) +'3',
    'Organization'+ str(random.randint(1, 100)) +'4'


]
TEST_COUNTRY = [


    'Country'+ str(random.randint(1, 100)) +'1',
    'Country'+ str(random.randint(1, 100)) +'2',
    'Country'+ str(random.randint(1, 100)) +'3',
    'Country'+ str(random.randint(1, 100)) +'4',
]
TEST_DESIGNATION = [
    # Designation
       '1' + str(random.randint(1, 100)) + 'Designation',
       '2' + str(random.randint(1, 100)) + 'Designation',
       '3' + str(random.randint(1, 100)) + 'Designation',
       '4' + str(random.randint(1, 100)) + 'Designation',
]
TEST_CASES_Affiliation = [
    {
        'test_case_name': 'Add_affiliation',
        'test_case_descripation': 'Add affiliation',
        'precondition': 'One association is already present',

        'test_case_senario': {
                "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
        },
        'test_case_execute': {
           "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[1],
                        "country": TEST_COUNTRY[1],
                        "designation": TEST_DESIGNATION[1],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
                },
        'test_case_history':{
             "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
        }
    },
    {
        'test_case_name': 'Edit_Affiliation',
        'test_case_descripation': 'Edit Affiliation',
        'precondition': 'One association is already present',

        'test_case_senario': {
                "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"

                    }
                ]
        },
        'test_case_execute': {
           "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[1],
                        "country": TEST_COUNTRY[1],
                        "designation": TEST_DESIGNATION[1],
                        "from_date": "2017",
                        "to_date": "Present"

                    }
                ]
                },
        'test_case_history':{
             "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
        }
    },
    {
        'test_case_name': 'Delete_Affiliation',
        'test_case_descripation': 'Delete affiliation',
        'precondition': 'One association is already present',

        'test_case_senario': {
                "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
        },
        'test_case_execute': {
           "affiliations": [

                ]
                },
        'test_case_history':{
             "affiliations": [
                    {
                        "affiliation": TEST_DESIGNATION[0],
                        "country": TEST_COUNTRY[0],
                        "designation": TEST_DESIGNATION[0],
                        "from_date": "2017",
                        "to_date": "Present"
                    }
                ]
        }
    },
]
