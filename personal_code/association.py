from __future__ import print_function


import json
import pprint
import time

import requests
from pymongo import DESCENDING

from accounts import User_details, User_details
from db import get_collections_history, get_collections_history_Merck,get_current_association_Merck,get_history_association_Merck
from association_data import(
    ASSOCIATION_NAME, ASSOCIATION_TYPE, TEST_CASES_ASSOCIATION, BUSINESS_UNIT)

#---------------------------------------------------------
AUTH_TOKEN = ""
KOL_id = ID
KOL_ID_2 = "60aa0668880a4e43b200ae63456fb16e"
f = open("output.txt", "w+")
BASE_API="url/api/v2/kols/profile/"
#m checking

def auth_login(account):

    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    # print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    codes = code[29:]
    #print(codes)
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    
    req = requests.get(
        'url/api/v1/auth/callback', params=payload)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
#print(accesstoken)
    # print("Granted access token:", accesstoken[10:])
    return accesstoken
# to make Auth token generic


def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(account)


#---------------------------------------------------------
# Create Environment for preconditions


BASE_API = "url/api/v2/kols/profile/"


def run_condition(body):
    '''
    This method is used to create enviroment and excute main test case. Only pass two different dic.
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + " ", '-' * 3)
    
    # print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }
    
    #print(body)
    body = json.dumps(body)
    #print(body)
    url = BASE_API + KOL_ID + '/associations'
    #print(url)
    req = requests.put(BASE_API + KOL_ID + '/associations',
                       params=params, data=body, headers=Header)
    print("status : ", req.status_code)
    # print("Environment Created!!! : ", req.status_code)
    # test_case_results['create_environment'] = "success"
    #print(req.status_code, req.text)
#-----------------------------------------------------------------------------

def perform_current_check(association_data,key):
    current_dict = check_current_collection()
    associations = current_dict.get('company_associations',[])
    expected_current_collection = association_data.get(key)
    #expected_current_collection = expected_current_collection.get["company_associations"]
    
    for bu1 in associations:
        BU_Value_db = bu1.get('BU')
    for bu2 in expected_current_collection:
        BU_value = bu2.get('BU')
    
    
    if BU_Value_db == BU_value:
        print("BU in current collection Matched")
    elif not expected_current_collection['BU'] and not associations['BU']:
        print("BU in current collection Matched")
    else:
        print("BU in current collection Not Matched")
    
    for activity_type_1 in associations:
        activity_type_Value_db = activity_type_1.get('activity_type')
    for activity_type_2 in expected_current_collection:
        activity_type_Value = activity_type_2.get('activity_type')
    
    if activity_type_Value_db == activity_type_Value:
        print("activity_type in current collection Matched")
    else:
        print("activity_type in current collection Not Matched")
    
    for association_1 in associations:
        association_Value_db = association_1.get('association')
    for association_2 in expected_current_collection:
        association_Value = association_2.get('association')
    
    if association_Value_db == association_Value:
        print("Association in current collection Matched")
    else:
        print("association in current collection Not Matched")

#--------------------------------------------------------------------
def perform_current_check_history(association_data,key):
    history_dict = check_history_collection()
    #print(type(history_dict))
    associations_history = history_dict.get('company_associations',[])
    #print(associations_history)
    expected_history_collection = association_data.get(key)
    BU_Value_db_H=''
    BU_value_H=''
    activity_type_Value_db_H=''
    activity_type_Value_H=''
    association_Value_db_H=''
    association_Value_H=''
    
    for bu1 in associations_history:
        BU_Value_db_H = bu1.get('BU')
    for bu2 in expected_history_collection:
        BU_value_H = bu2.get('BU')
    
    
    if BU_Value_db_H == BU_value_H:
        print("BU in History collection Matched")
    else:
        print("BU in History collection Not Matched")
    
    for activity_type_1 in associations_history:
        activity_type_Value_db_H = activity_type_1.get('activity_type')
    for activity_type_2 in expected_history_collection:
        activity_type_Value_H = activity_type_2.get('activity_type')
    
    if activity_type_Value_db_H == activity_type_Value_H:
        print("Activity_type in History collection Matched")
    else:
        print("Activity_type in History collection Not Matched")
    
    for association_1 in associations_history:
        association_Value_db_H = association_1.get('association')
    for association_2 in expected_history_collection:
        association_Value_H = association_2.get('association')
    
    if association_Value_db_H == association_Value_H:
        print("Association in History collection Matched")
    else:
        print("Association in History collection Not Matched")


    

        

   



#-----------------------------------------------------------------------------
def check_current_collection():
   
    collection = get_current_association_Merck()
    find_current = collection.find({'author_id': KOL_ID})
    current_data=find_current[0]
    # print(current_data)
    return current_data
#-------------------------------------------------------------------------------
def check_history_collection():

    collection = get_history_association_Merck()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING).limit(1)
    history_list = list(find_last_history)
    #print(len(history_list))
    
    history_data = history_list[0]
    #print(find_last_history)
    #print(history_data['_id'])
    history_data.pop('_id')
    #print(history_data)
    #print(json.dumps(history_data))
    return history_data

    #perform_check(history_data, test_case, 'test_case_history', "History")
#----------------------------------------------------------------------------------------------


def run_test_cases():
    get_auth_token(User_details)
    
    for association_data in TEST_CASES_ASSOCIATION:

        print("-" * 20)
        print("Executing",association_data['test_case_name'])
        print("-" * 20)
        print(association_data['test_case_descripation'])
        print("-" * 20)
        #print(association_data['precondition'])
        print("-" * 20)
       
       # 1. Generate Environment
        run_condition(association_data['test_case_senario'])
        print("-" * 20)
        # 2. Execute Test case
        run_condition(association_data['test_case_execute'])
        print("-" * 20)

        check_current_collection()
        
        check_history_collection()
        
        perform_current_check(association_data['test_case_execute'],'company_associations')
        print("-" * 20)
        perform_current_check_history(association_data['test_case_senario'],'company_associations')
        get_auth_token(User_details)
       


if __name__ == '__main__':
    run_test_cases()
