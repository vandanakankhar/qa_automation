TA_synonyms = {
#"neurology": ["neurology", "neuro", "Alzheimer", "Parkinson" ,"Brain","Sclerosis","Dementia"]
#"oncology":["breast cancer","breast carcinoma","Breast Tumor","malignant neoplasm of breast","Inflammatory carcinoma of breast","breast neoplasm","Invasive carcinoma of breast"]
#"oncology":[ "cancer","parp", "olaparib", "carcinoma", "parp-1", "parp-2", "malignancy", "breast cancer", "talazoparib", "niraparib", "veliparib", "parp inhibitor","tumor"]
#"oncology":["Pancreatic Cancer","pancreas","carcinoma of pancreas","exocrine cancer","Pancreatic Ca","pancreas ca"]
#"oncology":["NSCLC","SCLC","non small cell lung cancer","lung cancer","lung carcinoma"]
#"oncology":["Uveal melanoma","Ocular melanoma","choroidal","eye cancer", "eye melanoma","iris","ocular oncology","uvea"]
"gastroenterology":["IBD", "Ulcerative colitis", "Inflammatory bowel disease", "Crohn's disease", "colitis", "gastroenterology", "gastritis","Crohns Bowel obstruction", "Ulcers"]
#"reproductive health":["In-vitro Fertilization","Assisted reproductive technology (ART)","Embryo Transfer","oocyte retrieval","embryo assessment","Ovulation Induction","Artificial insemination","Ovulation Induction"]
#"cardiology": ["Chronic Heart Failure","Cardiac Failure","Heart Decompensation","Myocardial Failure","Congestive Heart Failure","Essential Hypertension","Ventricular Dysfunction","ADHF","Sacubitril","Valsartan","Entresto","Right-Sided Heart Failure","Left-Sided Heart Failure"]
#"dermatology" : ["Derma","dermatology","dermatitis","psoriasis","eczema","skin diseases"]
#"endocrinology": ["Thyroid", "hypothyroidism", "hyperthyroidism", "obesity", "endocrinology", "endocrine disorders", "goitre"]
}

asset_class_mappings = {
"publications": "non_nested",
"congresses": "nested"
}
