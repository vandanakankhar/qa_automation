import config
from jaccard_similarity import jaccard_similarity
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
import json
import csv
import pprint
import sys
import multiprocessing
from itertools import permutations
reload(sys)
sys.setdefaultencoding("utf-8")
import time
import pymongo
from pymongo import MongoClient
import pdb
import requests
from datetime import datetime
from elasticsearch import Elasticsearch
global_client = elastisearch_ip
from time import gmtime, strftime
path ='/home/yash.pandey/Downloads/'
import multiprocessing
from itertools import permutations
import os.path
end_time=  strftime("%Y-%m-%d", gmtime())
out_filename = "wed_output.csv"
from subprocess import call


class author_corpus_test(object):



    def create_es_connection(self):
        client = elastisearch_ip
        return client

    def build_body(self, name):
            body = {
                      "query": {
                        "bool": {
                          "must_not": [
                            {
                              "term": {
                                "is_normalize": {
                                  "value": False
                                }
                              }
                            }
                          ],
                          "must": [
                            {
                              "bool": {
                                "minimum_should_match": 1,
                                "should": [
                                  {
                                    "match": {
                                      "name.autocomplete": name
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      },
                      "size": 500
                    }

            return body

    def get_searchable_name(self,name):
        searchable_name = []
        name_list = name.split()
        #print name_list
        for n in name_list:
            if len(n) > 1:
                searchable_name.append(n)


        #searchable_name.sort() #uuuu
        name = " ".join(searchable_name)
        return name

    def remove_stopwords(self, affiliation):
        stop_words = set(stopwords.words('english'))
        word_tokens = word_tokenize(affiliation)
        filtered_sentence = [w for w in word_tokens if not w in stop_words]

        return filtered_sentence

    def pre_process_affiliations(self, affiliations):
        affiliations_clean = []
        for aff in affiliations:
            aff = aff.lower()
            aff = aff.encode('ascii','ignore')
            if aff and aff not in affiliations_clean:
                affiliations_clean.append(aff)

        return affiliations_clean

    def compare_affiliations(self, affiliations, elastic_affiliations):
        affiliations = self.pre_process_affiliations(affiliations)
        elastic_affiliations = self.pre_process_affiliations(elastic_affiliations)
        affiliation_list_of_list = []
        elastic_affiliation_list_of_list = []
        for affiliation in affiliations:
            affiliation_list = self.remove_stopwords(affiliation)
            affiliation_list_of_list.append(affiliation_list)
            #for elastic_affiliation in elastic_affiliations:
            #    elastic_affiliation_list = self.remove_stopwords(elastic_affiliation)
            #    similarity_score = jaccard_similarity(affiliation_list, elastic_affiliation_list)
            #    if similarity_score >= 0.60:
            #        return True
        for elastic_affiliation in elastic_affiliations :
            elastic_affiliation_list = self.remove_stopwords(elastic_affiliation)
            elastic_affiliation_list_of_list.append(elastic_affiliation_list)


        for affiliations in affiliation_list_of_list :
            for elastic_affiliations in elastic_affiliation_list_of_list :
                similarity_score = jaccard_similarity(affiliations,elastic_affiliations)
                if similarity_score >= 0.60:
                       return True

        if not affiliations or not elastic_affiliations:
            return True

        return False

    def pre_process_countries(self, countries):
        countries_clean = []
        for country in countries:
            country = country.lower()
            if country and country not in countries_clean:
                countries_clean.append(country)

        return countries

    def compare_country(self, countries, elastic_country):
        countries = self.pre_process_countries(countries)
        elastic_country = self.pre_process_countries(elastic_country)

        if not countries or not elastic_country:
            return True
        elif countries:
            country_intersection = list(set(countries).intersection(elastic_country))
            if country_intersection :
                return True

        return False

    #  dict_merck={}
    def get_initial_current_word_not_persist_existing(self,fullname_list):
        # start to end: get initial of one and keep others as it is
        # B Nasser Al Jafen
        # Bandar N Al Jafen
        # Bandar Nasser A Jafen
        names_with_initial = []
        for i in range(0, len(fullname_list)-1):
            name = ""
            for j in xrange(0, len(fullname_list)):
               if j == i:
                  name = name + " " + fullname_list[j][0]
               else:
                  name = name + " " + fullname_list[j]
            names_with_initial.append(name.strip())

        return names_with_initial

    def namef(self,l,i):
        x = l[i][0]
        l[i] = x
        return l

    def get_initial_of_one_and_persist_previous_initial(self,name):
        list1 = name.split()
        count = len(list1) - 1
        i = count - 1
        l2 = []
        final = []
        while i >= 0 :
            l = []
            #print 'Before : ', l2
            # l2 += [namef(list1,i)]
            l.append(" ".join(self.namef(list1,i)))
            final.extend(l)

            # l2[i] = namef(list1,i)
            #print 'After :' , l2
            i = i-1

        return list(set(final))

    def get_initial_of_one_and_persist_previous_initial_include_last(self,name):
        list1 = name.split()
        count = len(list1) - 1
        i = count
        l2 = []
        final = []
        while i >= 0 :
            l = []
            #print 'Before : ', l2
            # l2 += [namef(list1,i)]
            l.append(" ".join(self.namef(list1,i)))
            final.extend(l)

            # l2[i] = namef(list1,i)
            #print 'After :' , l2
            i = i-1

        return list(set(final))

    def getInitialBasedName(self,fullname):
        names_with_initial = []
        fullname_list = fullname.split()
        for i in range(0, len(fullname_list)-1):
            name = ""
            for j in xrange(0, len(fullname_list)):
                if i >= j:
                   name = name + " " + fullname_list[j][0]
                else:
                   name = name + " " + fullname_list[j]
            names_with_initial.append(name.strip())

        s1 = self.get_initial_current_word_not_persist_existing(fullname_list)
        s2 = self.get_initial_of_one_and_persist_previous_initial(fullname)
        s3 = self.get_initial_of_one_and_persist_previous_initial_include_last(fullname)
        list_s1 = s1 + s2 + s3
        final_list1 = list_s1 + names_with_initial
        merged_list = list(set(final_list1))
        #print "s1",s1
        #print "s2",s2
        #print "s3",s3
        #print "existing",names_with_initial
        #print "Final list-------->",final_list1
        #print "Merged list-------->",merged_list
        return merged_list

    def getAllPossible(self,name_com):
        name=name_com.split()
        dict_merck={}

        if len(name)<=4:
            a=[]
            b=[]

            for i in permutations(name):
                a.append((" ").join(i))


            b.extend(a)
            for j in a:
                b.extend(self.getInitialBasedName(j))
            dict_merck[name_com]=b


            return dict_merck

        dict_merck[name_com] = [name_com]
        return dict_merck

    def compare(self, elasticname, arrayname, authorid=None):

        # new_name=''.join(e for e in elasticname if e.isalnum()).lower()
        new_name = re.sub(r'[^a-z]', '', elasticname.lower())
        new_arrname = re.sub(r'[^a-z]', '', arrayname.lower())

        if(new_name==new_arrname):
             return True
        else:
            return False

    def affliation_generator(self, affiliation):

        affiliations = []
        countries = []
        header={
        'content-type': 'application/json'
        }
        payload =  {"term": affiliation}
        response = requests.post('URL', data=json.dumps(payload), headers=header)
        if response.status_code == 200:
            responses = response.json()
            for response in responses:
                affiliations.extend(response.get('sponsor',[]))
                countries.extend(response.get('country',[]))

                if not affiliations:
                    affiliations.extend(response.get('subDivision',[]))

        return affiliations,countries

    def csv_writer(self,Kol_name,elastic_name,elastic_id):
        columnTitleRow = "Actual_Kolname , Duplicate_Kolname , Es_author_id\n"
        #with open('filtered_kol.csv','w') as file:

    def get_research_activity(self, author_id, asset_class):
        mapping_type = config.asset_class_mappings.get(asset_class)
        if mapping_type == 'non_nested':
            body = {
                      "sort": {
                        "created_at": "desc"
                      },
                      "query" :{
                        "match":{
                          "authors.author_id": author_id
                        }
                      }
                    }

        elif mapping_type == 'nested':
            body = {
                     "sort": {
                       "created_at": "desc"
                     },
                     "query": {
                       "nested": {
                         "path": "authors",
                         "query": {
                           "bool": {
                             "must": [
                               {
                                 "terms": {
                                   "authors.author_id": [
                                     author_id
                                   ]
                                 }
                               }
                             ]
                           }
                         }
                       }
                     }
                    }

        response = global_client.search(index=asset_class+"_alias", body = body , request_timeout=100)
        return response

    def compare_therapeutic_area_synonyms(self, TA, string):

        TA_synonyms = config.TA_synonyms.get(TA.lower().strip())
        for synonym in TA_synonyms:
            if synonym in string:
                return True
        return False

    def compare_author_affiliations(self, doc, author_id, input_affiliations):
        authors = doc.get('authors')
        if authors:
            for author in authors:
                if author.get('author_id') == author_id:
                    affiliations = author.get('author_affiliation')
                    if not affiliations:
                        affiliations = author.get('affiliation', [])

                    author_affiliations, author_countries = \
                                self.affliation_generator("; ".join(affiliations))
                    affiliation_compare_result = self.compare_affiliations(\
                                                input_affiliations, author_affiliations)

                    if affiliation_compare_result and affiliations:
                        return True

        return False

    def compare_research_activity_publications(self, author_id, data, input_affiliations, TA):
        for doc in data:
            doc = doc['_source']
            author_result = self.compare_author_affiliations(doc, author_id, input_affiliations)
            if author_result:
                return True

            journal_title = doc.get("std_journal_title", "").lower()
            journal_title_result = self.compare_therapeutic_area_synonyms(TA, journal_title)
            if journal_title_result:
                 return True

            article_title = doc.get("article_title", "").lower()
            article_title_result = self.compare_therapeutic_area_synonyms(TA, article_title)
            if article_title_result:
              return True

        return False

    def compare_research_activity_congress(self, author_id, data, input_affiliations, TA):
        for doc in data:
            doc = doc['_source']
            author_result = self.compare_author_affiliations(doc, author_id, input_affiliations)
            if author_result:
                return True

            congress_name = doc.get("congress_name", "").lower()
            congres_name_result = self.compare_therapeutic_area_synonyms(TA, congress_name)
            if congres_name_result:
                 return True

            congress_title = doc.get("title", "").lower()
            congress_title_result = self.compare_therapeutic_area_synonyms(TA, congress_title)
            if congress_title_result:
              return True

        return False

    def get_final_result(self, doc, name, affiliations, countries, therapeutic_area, ton_percent_accurate, eighty_percent_accurate):
        #print(doc['_source']['name'])
        elastic_name = doc['_source']['name']
        # if elastic_name != 'A J Perry':
        #     return

        #if elastic_name == 'Monta Emersone':
            #import ipdb; ipdb.set_trace()

        elastic_affiliation = doc.get('_source', {}).get('current_affiliation', {}).get('affiliation', "")
        elastic_country = doc.get('_source', {}).get('current_affiliation', {}).get('extracted_terms', {}).get('country', [])

        #print(doc['_source'].get('author_id', ''))
        elastic_author_id = doc['_source'].get('author_id','')

        name_match_result = self.compare(elastic_name, name, elastic_author_id)

        if(name_match_result == True):
            #import ipdb; ipdb.set_trace()
            combined_elastic_afflication = elastic_affiliation+", "+ ", ".join(elastic_country)
            combined_elastic_afflication = combined_elastic_afflication.strip(',')
            elastic_affiliations, elastic_countries = self.affliation_generator(combined_elastic_afflication)
            if not elastic_affiliations and elastic_affiliation:
                elastic_affiliations.append(elastic_affiliation)

            if elastic_country:
                elastic_countries.extend(elastic_country)

            affiliation_result = self.compare_affiliations(affiliations, elastic_affiliations)
            if affiliation_result:
                country_result = self.compare_country(countries, elastic_countries)

                if not elastic_affiliations and not elastic_countries:
                    publications_data = self.get_research_activity(elastic_author_id, 'publications')
                    if publications_data['hits']['total']:
                        data = publications_data['hits']['hits']
                        pubmed_result = self.compare_research_activity_publications(elastic_author_id, \
                                                data, affiliations, therapeutic_area)
                        if pubmed_result:
                            ton_percent_accurate = False
                            eighty_percent_accurate = True
                            return True ,ton_percent_accurate ,eighty_percent_accurate
                        else:
                            return False ,ton_percent_accurate ,eighty_percent_accurate
                    else:
                        congress_data = self.get_research_activity(elastic_author_id,'congresses')
                        if congress_data['hits']['total']:
                            data = congress_data['hits']['hits']
                            congress_result=self.compare_research_activity_congress(elastic_author_id,\
                                                data,affiliations,therapeutic_area)
                            if congress_result:
                                ton_percent_accurate = False
                                eighty_percent_accurate = True
                                return True , ton_percent_accurate, eighty_percent_accurate
                            else:
                                return False , ton_percent_accurate , eighty_percent_accurate

                if country_result:
                    ton_percent_accurate = True
                    eighty_percent_accurate = False
                    return True , ton_percent_accurate ,eighty_percent_accurate

            else:
                return False , ton_percent_accurate ,eighty_percent_accurate
        else:
            return False , ton_percent_accurate , eighty_percent_accurate

        return False , ton_percent_accurate  ,eighty_percent_accurate

    def write_result(self, name, author_id, affiliation, country, therapeutic_area, ton_percent_possible_duplicates ,eighty_percent_possible_duplicate):

        file_exists=os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            fieldnames = ['name', 'Input_author_id','affiliation', 'country', 'therapeutic_area', \
                            '100%_possible_duplicates','80%_possible_duplicates', "author_count"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'name': name, 'Input_author_id':author_id ,'affiliation': affiliation, 'country':country, \
                                       'therapeutic_area':therapeutic_area, \
                                       '100%_possible_duplicates': "@@@".join(ton_percent_possible_duplicates), \
                                        '80%_possible_duplicates':"@@@".join(eighty_percent_possible_duplicate) ,'author_count':len(ton_percent_possible_duplicates | eighty_percent_possible_duplicate)})


    def opencsv(self, input_file):

        arr=[]
        result_list=[]
        names_dict={}
        #reader = csv.dictreader(input_file)
        count=0
        start_time= strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print(start_time)
        with open(input_file,'r') as csvfile :
            reader = csv.DictReader(csvfile)
            for line in reader:
                count = count + 1
                # if count<=1035:
                #     continue
                ton_percent_possible_duplicates = set()
                eighty_percent_possible_duplicate = set()
                ton_percent_accurate = False
                eighty_percent_accurate = False
                t=line.get('Name')
                print t
                affiliation=line.get('affiliation in Author corpus')
                country=line.get('country in Author corpus')
                therapeutic_area = line.get('TA')
                input_author_id = line.get('author_id')
                combined_affliation=affiliation+", "+country
                combined_affliation = combined_affliation.strip()
                combined_affliation = combined_affliation.strip(',')
                #import ipdb; ipdb.set_trace()
                affiliations, countries = self.affliation_generator(combined_affliation)

                if not affiliations:
                    affiliations.append(affiliation)

                countries.append(country)
                #pprint.pprint(t)

                names_dict=a.getAllPossible(t)
                #pprint.pprint(dict)

                names = names_dict[t]

                for name in names:

                    searchable_name = a.get_searchable_name(t)
                    if searchable_name:
                        body = self.build_body(searchable_name)
                        #time.sleep(0.5)
                        response = global_client.search(index="authors_alias", body = body, request_timeout=100)

                        for doc in response['hits']['hits']:

                            result , ton_percent_accurate ,eighty_percent_accurate = self.get_final_result(doc, name, \
                                        affiliations, countries, therapeutic_area, \
                                            ton_percent_accurate, eighty_percent_accurate)
                            if result and ton_percent_accurate:
                                # print(t+"-->"+doc['_source']['name']+"-->"+doc['_source']['author_id'])
                                ton_percent_possible_duplicates.add(doc['_source']['name']+' | '+doc['_source']['author_id'])
                                #print possible_duplicates
                                #print len(possible_duplicates)
                            elif result and eighty_percent_accurate :
                                eighty_percent_possible_duplicate.add(doc['_source']['name']+' | '+doc['_source']['author_id'])

                self.write_result(t, input_author_id, affiliation, country, therapeutic_area, ton_percent_possible_duplicates, eighty_percent_possible_duplicate)
                print(count)

            end_time=  strftime("%Y-%m-%d %H:%M:%S", gmtime())
            print('Start time'+start_time)
            print('End Time'+end_time)
            return arr


if __name__ =='__main__':
    #TA = sys.argv[1]
    #call(["python3", "Top_20.py",TA])
    #call(["python" ,"kol_info.py",TA])

    a = author_corpus_test()
    # a.opencsv("Merck_KOLs.csv")
    a.opencsv("new_kol_info.csv")
    es_client = a.create_es_connection()
