#Scipt to fetch data for the upper side 3 filter and side filter
import requests
import json
import pprint
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
global_client = Elasticsearch(['elastic_Search_username_password'])
out_filename ="top20_Neurology.csv"
out_filename_1 ="top20_Oncology.csv"
out_filename_2="top20_Cardiology.csv"
out_filename_3 ="top20_Dermatology.csv"
out_filename_4 ="top20_Reproductive_Health.csv"
out_filename_5 ="top20_Metabolic_Diseases.csv"
out_filename_6 ="top20_Endocrinology.csv"
from accounts import User_details
global count
count = 0
AUTH_TOKEN = ''
import sys
def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def es_query(author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        'application_callback_url', params=payload)
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    return AUTH_TOKEN
def get_input(count, as_class, TA, list_indications, country,intervention,moa):

    get_auth_token(User_details)
    #import ipdb; ipdb.set_trace()
    #print(as_class, TA, list_indications, country,intervention,moa)
    url = 'url'
    payload = {}
    if as_class == "Top KOLs" :
        payload={"from":0,"limit":10,"TA":TA,"asset_class":as_class,"filters":{"kol_info":[{"name":"Publications","value":15,"showDate":True,"startDate":0,"endDate":0},{"name":"Clinical Trials","value":13,"showDate":True,"startDate":0,"endDate":0},{"name":"Congresses","value":12,"showDate":True,"startDate":0,"endDate":0},{"name":"Guidelines","value":10,"showDate":True,"startDate":0,"endDate":0},{"name":"Regulatory Bodies","value":5,"showDate":False,"startDate":0,"endDate":0},{"name":"HTA","value":5,"showDate":False,"startDate":0,"endDate":0},{"name":"Societies","value":10,"showDate":False,"startDate":0,"endDate":0},{"name":"Advocacy","value":10,"showDate":False,"startDate":0,"endDate":0},{"name":"Top Hospital KOLs","value":10,"showDate":False,"startDate":0,"endDate":0},{"name":"Diagnostic Centers","value":10,"showDate":False,"startDate":0,"endDate":0}],"showScore":True,"kolType":"allkols","Indications":{"values":list_indications,"isAnd":False},"MoAs":{"values":moa,"isAnd":False},"Countries":{"values":country,"isAnd":False},"Interventions":{"values":intervention,"isAnd":False}}}
    else:
        payload={"from":0,"limit":10,"TA":TA,"asset_class":as_class,"filters":{"kol_info":[{"name":"Publications","value":100,"showDate":False,"startDate":0,"endDate":0}],"showScore":False,"kolType":"allkols","Indications":{"values":list_indications,"isAnd":False},"MoAs":{"values":moa,"isAnd":False},"Countries":{"values":country,"isAnd":False},"Interventions":{"values":intervention,"isAnd":False}}}
    headers = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    #print(json.dumps(payload))
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    r = r.json()
    print(as_class, TA, list_indications, country,intervention,moa)

    #pprint.pprint(r)
    #import ipdb; ipdb.set_trace()
    emerging_author_id_list = get_emerging_kol(count, as_class, TA, list_indications, country)
    top_kol_list = []
    emerging_kol_name =''
    if r :
        for i in r['data'][0]['scores']:
            count+= 1
            print(count)
            #if count <= 15:
                #continue
            #author_id=i['author_id']
            print(i['author_id'],list_indications,country,as_class)
            top_kol_list.append(i['author_id'])
            #writer.writerow({'AUTHOR_ID' : i['author_id'], "TA" : TA, "INDICATION" : list_indications, "COUNTRY" : country, "RESOUCES" : as_class})
            if count == 20:
                break
    if len(top_kol_list) >= len(emerging_author_id_list) :
        for i in range(0,len(top_kol_list)) :
            #import ipdb; ipdb.set_trace()
            try :
                if top_kol_list[i] :
                    top_kol_body = es_query(top_kol_list[i])
                    response = global_client.search(index="authors_alias", body = top_kol_body, request_timeout=100)
                    for doc in response['hits']['hits']:
                        top_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try :
                if emerging_author_id_list[i] :
                    emerging_kol_body = es_query(emerging_author_id_list[i])
                    response_emerging = global_client.search(index="authors_alias", body = emerging_kol_body, request_timeout=100)
                    for doc in response_emerging['hits']['hits'] :
                        emerging_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try:
                if not emerging_kol_name :
                    emerging_kol_name = ''
                write_csv( top_kol_list[i], top_kol_name, emerging_author_id_list[i], emerging_kol_name , TA, list_indications, country, as_class,intervention,moa)
            except IndexError:
                pass
    elif len(emerging_author_id_list) > len(top_kol_list) :
        for i in range(0,len(emerging_author_id_list)) :
            try :
                if top_kol_list[i] :
                    top_kol_body = es_query(top_kol_list[i])
                    response = global_client.search(index="authors_alias", body = top_kol_body, request_timeout=100)
                    for doc in response['hits']['hits']:
                        top_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try :
                if emerging_author_id_list[i] :
                    emerging_kol_body = es_query(emerging_author_id_list[i])
                    response_emerging = global_client.search(index="authors_alias", body = emerging_kol_body, request_timeout=100)
                    for doc in response_emerging['hits']['hits'] :
                        emerging_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try:
                if not emerging_kol_name :
                    emerging_kol_name = ''
                write_csv( top_kol_list[i], top_kol_name, emerging_author_id_list[i], emerging_kol_name , TA, list_indications, country, as_class,intervention,moa)
            except IndexError:
                pass
def get_emerging_kol(count, as_class, TA, list_indications, country):
    get_auth_token(User_details)

    #print(as_class, TA, list_indications, country)
    url = 'url'
    payload = {}
    if as_class == "Top KOLs":
        print("Ignore")
    else:
        payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [
            {"name": as_class, "value": 100, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": False, "kolType": "emerging", "Countries": {"values": country, "isAnd": False}}}
    headers = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    #print(json.dumps(payload))
    #import ipdb; ipdb.set_trace()
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        # Whoops it wasn't a 200
        return "Error: " + str(e)
    r = r.json()
    emerging_author_id_list = []
    for i in r['data'][0]['scores']:
        count+= 1
        print(count)
        #if count <= 15:
            #continue
        #author_id=i['author_id']
        print("emerging :"+i['author_id'],list_indications,country,as_class)
        if i['author_id'] :
            emerging_author_id_list.append(i['author_id'])
        if count == 20:
            break
    return emerging_author_id_list
def write_csv(author_id,top_kol_name,author_id_emerging,emerging_kol_name,TA, list_indications, country, as_class,intervention,moa) :
    if TA == "Neurology":
        file_exists = os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Oncology":
        file_exists = os.path.isfile(out_filename_1)
        with open(out_filename_1, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Cardiology":
        file_exists = os.path.isfile(out_filename_2)
        with open(out_filename_2, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Dermatology":
        file_exists = os.path.isfile(out_filename_3)
        with open(out_filename_3, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Reproductive Health":
        file_exists = os.path.isfile(out_filename_4)
        with open(out_filename_4, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Metabolic Diseases":
        file_exists = os.path.isfile(out_filename_5)
        with open(out_filename_5, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
    elif TA == "Gastroenterology":
        file_exists = os.path.isfile(out_filename_6)
        with open(out_filename_6, 'a') as csvfile:
            feild_names=['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES','Intervention','MOA']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class,'Intervention':intervention,'MOA':moa})
if __name__ == '__main__':
    TA_value = sys.argv[1]
    if TA_value == "All":
        TA = ["Neurology","Oncology","Cardiology","Dermatology","Reproductive Health","Metabolic Diseases","Endocrinology","Gastroenterology"]
    else :
        TA = [TA_value]
    INDICATION_DCT = {"Gastroenterology":["Inflammatory Bowel Disease"],"Neurology": ["Multiple Sclerosis","Alzheimer","Parkinson"], "Oncology": ["Breast Cancer"],"Cardiology": ["Myocardial Infarction"],"Dermatology": ["Dermatitis", "Psoriasis"],"Reproductive Health": ["Male Infertility","Female Infertility","Ovarian Disease","Assisted Reproduction"],"Metabolic Diseases":["Diabetes"],"Endocrinology": ["Thyroid Disorders", "Obesity"]}

    LIST_ASSET_CLASS = ["Top KOLs","Publications", "Clinical Trials","Congresses"]
    # "Societies","Top Hospital KOLs","Advocacy","Guidelines"]
    #, Publications, Clinical Trials, and Congresses
    #"Ovarian Cancer","Lung Cancer","Pancreatic Cancer","Prostate Cancer","Peritoneal Cancer","Fallopian Tube Cancer","Gastric Cancer","Endometrial Cancer","Rectal Cancer","Acute Lymphocytic Leukemia","Lymphoma Head and Neck Cancer","Acute Myeloid Leukemia","Solid Tumor"
    INTERVENTIONS =["INO 1001","ABT 767","2X 121","BGB 290","Fluzoparib","Niraparib","Olaparib","Rucaparib","Talazoparib","Veliparib","IMP 4297"]
    MOA =["PARP Inhibitor"]
    COUNTRY_DCT ={
                "Inflammatory Bowel Disease":["Germany"],
                "Uveal Melanoma":["United States Of America","Germany","France","United Kingdom","Spain","Italy"],
                "Multiple Sclerosis": ["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Alzheimer":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Parkinson":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Breast Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Acute Lymphocytic Leukemia":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Acute Lymphoblastic Leukemia":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Ovarian Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Lung Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Pancreatic Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Prostate Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Peritoneal Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Fallopian Tube Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Gastric Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Endometrial Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Rectal Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Lymphoma":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Head and Neck Cancer":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Acute Myeloid Leukemia":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Solid Tumor":["Germany","France","United Kingdom","Spain","Italy","Austria","Netherlands","Belgium"],
                "Dermatitis":["France","Germany","United States Of America"],
                "Psoriasis":["France","Germany","United States Of America"],
                "Myocardial Infarction":["Germany","Denmark","Switzerland","France","United States of America","United Kingdom","France","Spain","Japan","Australia"],
                "Male Infertility":["Switzerland","Spain","Germany","Italy"],
                "Female Infertility":["Switzerland","Spain","Germany","Italy"],
                "Ovarian Disease":["Switzerland","Spain","Germany","Italy"],
                "Assisted Reproduction":["Switzerland","United States of America","Germany"],
                "Diabetes":["United States Of America"],
                "Thyroid Disorders":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"],
                "Obesity":["Costa Rica","Chile","Panama","Ecuador","Mexico","Peru","Argentina","Brazil","Columbia"]}


    count = 0
    for ta_name in TA:
        for resource in LIST_ASSET_CLASS:
            for indication in INDICATION_DCT[ta_name]:
                for country in COUNTRY_DCT[indication]:
                    for intervention in INTERVENTIONS:
                        for moa in MOA:
                            get_input(count, resource, ta_name, [indication], [country],intervention,moa)
                            print(count,ta_name,[indication],[country],resource,[intervention],[moa])
# if __name__ == '__main__':
#     country(TA,INDICATION_DCT,LIST_ASSET_CLASS)
#
#
#
    # LIST_ASSET_CLASS = ["Top KOLs", "Publications"]
    # # list_country=["Costa Rica", "Chile", "Panama", "Ecuador", "Mexico", "Peru", "Argentina", "Brazil", "Columbia", ]
    # TA = ["Neurology"]
    # INDICATION_DCT = {"Neurology": ["Multiple Sclerosis","Alzheimer","Parkinson"]
    #                     ""}
    # COUNTRY_DCT = {"Neurology": ["Panama", "Germany"]}
    # author_ids = set()
    # for as_class in list_asset_class:
    #     list_authors_as_class=get_input(as_class,"Neurology",["Multiple Sclerosis"])
    #     for ids in list_authors_as_class:
    #         print author_ids
    #         author_ids.add(ids)
    # for ta_name in INDICATION_DCT:
    #     for resource in LIST_ASSET_CLASS:
    #         for indication in INDICATION_DCT[ta_name]:
    #             get_input(count, resource, ta_name, [indication])
    #             count = 0
    # for as_class in list_asset_class:
    #     for countries in list_country:
    #         list_authors_as_class = get_input(
    #             as_class, "Endocrinology", ["Obesity"], [countries])
    #         for ids in list_authors_as_class:
    #             author_ids.add(ids)
