

TA = ''
indication = ''
author_id = ''
def get_top_hospital_kols_query(TA, indications, author_id):
    query = {
            "bool": {
                "must": [
                    {
                        "bool": {
                            "should": [
                                {
                                    "match_phrase": {
                                        "classification.TA": TA
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    if author_id:
        author_id_query =  {
            "bool": {
                "should": [
                    {
                        "match": {
                            "author_ids_hash": author_id
                        }
                    }
                ]
            }
        }
        query["bool"]["must"].append(author_id_query)
    if indications:
        indication_query =  {
            "bool": {
                "should": [
                ]
            }
        }
        base_query = {
            "match_phrase": {
                "classification.indications": None
            }
        }
        for i in range(0, len(indications)):
            indication = indications[i]
            base_query["match_phrase"]["classification.indications"] = indication
            indication_query["bool"]["should"].append(base_query)
        query["bool"]["must"].append(indication_query)

    return query

functions_dict = {
    "Top Hospital KOLs": get_top_hospital_kols_query
}

def clinical_trails_kols_query(indication ,author_id) :
    #print(author_id,indication)
    query = {"query": {
                "bool": {
                    "must": [



                        ]
                    }
                }
            }


    if author_id :
        author_id_query ={
                            "match": {
                                    "author_ids_hash": author_id
                            }
                        }


    if indication :

        indication_query = {
                        "bool": {
                            "should":[]
                        }
                    }

        #for ind in indication :


        indication_query['bool']['should'].extend(base_ct_query(indication))
        indication_query['bool']['should'].append(author_id_query)

    query['query']['bool']['must'].append(indication_query)

    return query

def base_ct_query(indications) :

        query = []

        for indication in indications :

            base_query= [
                        {
                            "match_phrase": {
                                "detailed_description": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "intervention.intervention_name": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "intervention.description": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "intervention.arm_group_label": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "study_design.type": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "study_design.value": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "public_title": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "scientific_title": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "condition": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "keyword": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "official_title": indication
                            }
                        },
                        {
                            "match_phrase": {
                                "abstract": indication
                            }
                        }
                    ]
            query.extend(base_query)

        return query

def HTA_kol_query(TA ,indication ,author_id):
     query = {
               "query":{
                   "bool":{
                       "must":[]
                       }
               }
             }
     if TA :
         TA_query ={
                "bool": {
                    "should": [
                                {
                                "match_phrase": {
                                    "classification.TA": TA
                                    }
                                }
                            ]
                        }
                    }
     if author_id :
        author_id_query ={
                            "match": {
                                    "author_ids_hash": author_id
                            }
                    }
     if indication :
        indication_query = {
                        "bool": {
                            "should": []
                                }
                            }
        indication_query.extend(HTA_base_query(indication))

     query['query']['bool']['must'].append(indication_query)
     query['query']['bool']['must'].append(TA_query)
     query['query']['bool']['must'].append(author_id_query)

     return query


def HTA_base_query(indication) :
    base_query = [
                  {
                    "match_phrase": {
                        "classification.indications":indication
                        }
                  }
                  ]
    return base_query

def congresses_kol_query(indication , TA ,author_id):
    query = {
              "query" : {
                 "bool" : {
                    "must" :[

                    ]
                 }
              }
            }
    if author_id :
        author_id_query = {
                        "match": {
                            "author_ids_hash": author_id
                        }
                    }
    if indication :
        indication_query =  {
                        "bool": {
                            "should": []
                              }
                            }
        indication_query['bool']['should'].extend(base_congresses_query(indication ,TA))

    query.append(author_id_query)
    query.append(indication_query)

    return query

def base_congresses_query(indication , TA) :
    base_query = [
                     {
                        "match_phrase": {
                            "classification.indications": indication
                        }
                    },
                    {
                        "match_phrase": {
                            "title": indication
                        }
                    },
                    {
                        "match_phrase": {
                            "abstract": indication
                        }
                    },
                    {
                        "match_phrase": {
                            "classification.TA": TA
                        }
                    }
               ]

    return base_query

def publication_kol_query(indication, author_id) :

    query = {
            "query":{
                "bool":{
                    "must":[]
                }
            }
        }
    if author_id :
        author_id_query = {
                            "match": {
                                "author_ids_hash": author_id
                            }
                        }
    if indication :
        indication_query = {
                    "bool":{
                        "should":[]
                    }
        }
        indication_query['bool']['should'].extend(base_publication_query(indication))

    query['query']['bool']['must'].append(author_id_query)
    query['query']['bool']['must'].append(indication_query)

    return query

def base_publication_query(indications):
    query = []
    for indication in indications :
        base_query  = [
                        {
                        "match_phrase": {
                            "article_title": indication
                        }
                        },
                        {
                        "match_phrase": {
                            "keywords": indication
                        }
                        },
                        {
                        "match_phrase": {
                            "abstract": indication
                        }
                        },
                    ]
        query.extend(base_query)

    return query

def regulatory_bodies_kol_query(TA ,indication ,author_id) :
    query = {
            "query" :{
                "bool":{
                    "must":[]
                }
            }
        }

    if TA :
        TA_query = {
                    "bool": {
                            "should": [
                                {
                                    "match_phrase": {
                                        "classification.TA": TA
                                    }
                                }
                            ]
                        }
                    }
    if indication :
        indication_query  =  {
                                "bool":{
                                    "should":[]
                                }
                            }
        indication_query['bool']['should'].extend(regulatory_bodies_base_query(indication))

    query['query']['bool']['must'].append(indication_query)
    query['query']['bool']['must'].append(author_id_query)

    return query

def regulatory_bodies_base_query(indication) :
    base_query = [
                    {
                    "match_phrase": {
                        "classification.indications": indication
                    }
                }
                ]
    return base_query
