from __future__ import print_function


import json
import pprint
import time

import requests
from pymongo import DESCENDING

from accounts import User_details, User_details
from db import get_collections_history, get_collections_history_Merck
from test_cases import (TEST_ADDRESSES, TEST_CASES, TEST_EMAILS, TEST_PHONES)
from test_cases_merck import (TEST_ADDRESSES_MERCK, TEST_CASES_MERCK,
                              TEST_EMAILS_MERCK, TEST_PHONES_MERCK)
#---------------------------------------------------------
#                     1.  LOGIN LOGIC
#---------------------------------------------------------
AUTH_TOKEN = ""
KOL_id = ID
KOL_ID_2 = "60aa0668880a4e43b200ae63456fb16e"
f = open("output.txt", "w+")
BASE_API="url/api/v2/kols/profile/"


def auth_login(account):

    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    # print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    codes = code[29:]
    #print(codes)
    #print("Granted login codes :", codes)

    return codes


def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }

    req = requests.get(
        'url/api/v1/auth/callback', params=payload)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print("Granted access token:", accesstoken[10:])
    #print(accesstoken)
    return accesstoken
# to make Auth token generic


def get_auth_token():
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(User_details)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(User_details)

#---------------------------------------------------------
#          2.Environment for Preconditions
#---------------------------------------------------------
# Create Environment for preconditions
def run_condition(body, condition_type):
    '''
    This method is used to create enviroment and excute main test case. Only pass two different dic.
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    '''
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }
'''
    body = json.dumps(body)

    req = requests.put(BASE_API + KOL_ID + '/personal_info',
                        data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)
    #print(condition_type, "Response Body : ", json.dumps(req.json()))
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"

#---------------------------------------------------------
#          3.Main Test Case
#---------------------------------------------------------
def perform_check(test_case,key,check_type,account):

    collection=check_history_collection()
    expected_current_collection = test_case.get(key)
    current_emails = collection.get('emails')
    #import ipdb; ipdb.set_trace()
    flag_check_email = True
    #If both have something

    emails = expected_current_collection.get('emails','')
    if emails and current_emails:
        for email in current_emails:
            database_email = email.get('email','')
        for email in emails:
            current_email=email.get('email','')
        print(database_email)
        print(current_email)
        if database_email == current_email:
            flag_check_email = True
        else:
            flag_check_email = False
    # Both are empty
    elif not expected_current_collection['emails'] and not current_emails:
        flag_check_email = True
    #One of them is empty
    else:
        flag_check_email = False
    if flag_check_email:
        print("For Email,Test case is Pass ")
    else:
        print("For Email,Test case is Fail ")
   #----------------------------Phone-----------------------------
    current_phones = collection.get('phone_numbers')
    phones=expected_current_collection.get('phone_numbers')
    flag_check_phone = True
    if phones and current_phones:
        for phone in current_phones:
            database_phone=phone.get('email','')
        for phone_data in phones:
            phone=phone_data.get('email','')

            if database_phone == phone:
                    flag_check_phone = True
            else:
                    flag_check_phone = False
    #Both are empty
    elif not expected_current_collection.get('phone_numbers') and not current_phones:
        #print(check_type, " Phone check:", "Pass")
        flag_check_phone = True

    # One of them is empty
    else:
        #print(check_type, " Phone check:", "Fail")
        flag_check_phone = False
    if flag_check_phone:
        # #pprint.pprint(current_phones)
        print("For Phone,Test case is Pass")
        f.write("For Phone,Test case is Pass")
    else:
        # pprint.pprint(current_phones)
        print("For Phone,Test case is Fail")
        f.write("For Phone,Test case is Fail")

    #--------------Address---------------------------------------
    current_addresses = collection.get('addresses')
    addresses = expected_current_collection.get('addresses')
    # if both have something
    flag_check_address = True
    if addresses and current_addresses:
        for address in current_addresses:
            database_address=address.get('address','')
        for address in addresses:
            current_addresses=address.get('address','')

            if database_address == current_addresses:
                flag_check_address = True
            else:
                flag_check_address = False
    # Both are empty
    elif not expected_current_collection['addresses'] and not current_addresses:
        #print(check_type, " address check:", "Pass")
        flag_check_address = True

    # One of them is empty
    else:
        #print(check_type, " address check:", "Fail")
        flag_check_address = False

    if flag_check_address:
        # #pprint.pprint(current_addresses)
        print("For Address,Test case is pass")
        f.write("For Address,Test case is Pass")
    else:

        print("For Address,Test case is Fail")
        f.write("For Address,Test case is Fail")

    if flag_check_email and flag_check_address and flag_check_phone:
        print(check_type, " Collection Check:", "Pass")


    else:
        print(check_type, " Collection Check:", "Fail")

#---------------------------------------------------------

def perform_check_client(test_cases_merck,key,check_type):

    collection=check_history_collection_client()
    expected_current_collection = test_cases_merck.get(key)
    current_emails = collection.get('emails')
    flag_check_email = True
    emails = expected_current_collection.get('emails','')
    if emails and current_emails:
        for email in current_emails:
            database_email=email.get('email','')
        for email in emails:
            current_email=email.get('email','')

            if database_email == current_email:
                flag_check_email = True
            else:
                flag_check_email = False
    # Both are empty
    elif not expected_current_collection['emails'] and not current_emails:
        flag_check_email = True
    #One of them is empty
    else:
        flag_check_email = False
    if flag_check_email:
        print("For Email,Test case is Pass ")
    else:
        print("For Email,Test case is Fail ")
    #----------------------------Phone-----------------------------
    current_phones = collection.get('phone_numbers')

    phones=expected_current_collection.get('phone_numbers')
    flag_check_phone = True
    if phones and current_phones:
        for phone in current_phones:
            database_phone=phone.get('email','')
        for phone_data in phones:
            phone=phone_data.get('email','')

            if database_phone == phone:
                    flag_check_phone = True
            else:
                    flag_check_phone = False
    #Both are empty
    elif not expected_current_collection.get('phone_numbers') and not current_phones:
        flag_check_phone = True
    # One of them is empty
    else:
        flag_check_phone = False
    if flag_check_phone:
        print("For Phone,Test case is Pass")
        f.write("For Phone,Test case is Pass")
    else:
        # pprint.pprint(current_phones)
        print("For Phone,Test case is Fail")
        f.write("For Phone,Test case is Fail")

    #--------------Address---------------------------------------
    current_addresses = collection.get('addresses')
    addresses = expected_current_collection.get('addresses')
    # if both have something
    flag_check_address = True
    if addresses and current_addresses:
        for address in current_addresses:
            database_address=address.get('address','')
        for address in addresses:
            current_addresses=address.get('address','')

            if database_address == current_addresses:
                flag_check_address = True
            else:
                flag_check_address = False
    # Both are empty
    elif not expected_current_collection['addresses'] and not current_addresses:
        #print(check_type, " address check:", "Pass")
        flag_check_address = True
    # One of them is empty
    else:
        #print(check_type, " address check:", "Fail")
        flag_check_address = False

    if flag_check_address:
        # #pprint.pprint(current_addresses)
        print("For Address,Test case is pass")
        f.write("For Address,Test case is Pass")
    else:

        print("For Address,Test case is Fail")
        f.write("For Address,Test case is Fail")

    if flag_check_email and flag_check_address and flag_check_phone:
        print(check_type, " Collection Check:", "Pass")
    else:
        print(check_type, " Collection Check:", "Fail")

#------------------------------------------------------
# To get data from current collection


def check_execution_status_current_collection(account, test_case,client):
    #expected_current_collection = test_case.get('test_case_current_collection')
    expected_current_collection = test_case.get('test_case_current_collection')
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    req = requests.get(BASE_API  + KOL_ID + '/personal_info', headers=Header)
    #print(BASE_API  + KOL_ID + '/personal_info')
    #import ipdb; ipdb.set_trace()
    #print("Response Body : ", json.dumps(req.json()))
    #print(Header)
    print("Checking data in "  + client + " current collection with test cases:")
    if req.status_code == 200:
        innoData = req.json()['data'].get('innoData', None)
        orgData = req.json()['data'].get('orgData',None)
        #import ipdb; ipdb.set_trace()
        if orgData == {} and account == User_details:
            print("Merck data is blank")
        else:
            print("you can not access "  +  client  + " data")

        current_emails = innoData.get('emails')
        flag_check_email = True
        if expected_current_collection.get('emails') and current_emails:
                for email in current_emails:
                    #import ipdb; ipdb.set_trace()
                    for email_data in expected_current_collection.get('emails',[]):
                        if email.get('email','') == email_data:
                            flag_check_email = True
                        else:
                            flag_check_email = False

            # Both are empty
        elif not expected_current_collection['emails'] and not current_emails:
                flag_check_email = True
            #One of them is empty
        else:
                flag_check_email = False

        if flag_check_email:
            print("For Email,Test case is Pass ")
        else:
            print("For Email,Test case is Fail ")
            #---------------Phone-------------------------------------
        current_phones = innoData.get('phone_numbers')
        flag_check_phone = True
        if expected_current_collection.get('phone_numbers') and current_phones:
            for phone in current_phones:
                #import ipdb; ipdb.set_trace()
                for phone_data in expected_current_collection.get('phone_numbers',[]):
                    if phone.get('phone','') == phone_data:
                        flag_check_phone = True
                    else:
                        flag_check_phone = False

        # Both are empty
        elif not expected_current_collection['emails'] and not current_phones:
            flag_check_phone = True
        #One of them is empty
        else:
            flag_check_phone = False

        if flag_check_phone:

            print("For Phone,Test case is Pass ")

        else:

            print("For Phone,Test case is Fail ")
         #-----------address-----------------------------------------------
        current_addresses = innoData.get('addresses')
        flag_check_address = True
        if expected_current_collection.get('addresses') and current_addresses:
         for address in current_addresses:
             #import ipdb; ipdb.set_trace()
             for address_data in expected_current_collection.get('addresses',[]):
                 if address.get('address','') == address_data:
                     flag_check_address = True
                 else:
                     flag_check_address = False
         # Both are empty
        elif not expected_current_collection['addresses'] and not current_addresses:
         flag_check_address = True
         #One of them is empty
        else:
         flag_check_address = False

        if flag_check_address:
            print("For Address,Test case is Pass ")
        else:
                print("For Address,Test case is Fail ")

    else:
        print(req.status_code, req.text)
#---------------------------------------------------------
#         5.Check another Client Collection
#---------------------------------------------------------
def check_execution_status_on_client_current_collection(account, test_case, client):
    expected_current_collection = test_case.get('test_case_current_collection')
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    url = BASE_API + KOL_ID + '/personal_info'
    #print(url)
    last_updated_email=''
    req = requests.get(url, headers=Header)
   #print(req.status_code, req.text)
    print("Checking data in " + client + " current collection with test cases:")
    # #pprint.pprint(req.json())
    if req.status_code == 200:
        innoData = req.json()['data'].get('orgData', None)
        current_emails = innoData.get('emails')
        print("Current collection Status:")
        if innoData:
            flag_check_email = True
            if expected_current_collection.get('emails') and current_emails:
                for email in current_emails:
                    for email_data in expected_current_collection.get('emails',[]):
                        if email.get('email','') == email_data:
                            flag_check_email = True
                        else:
                            flag_check_email = False
            # Both are empty
            elif not expected_current_collection['emails'] and not current_emails:
                flag_check_email = True
            #One of them is empty
            else:
                flag_check_email = False

            if flag_check_email:
                print("For Email,Test case is Pass ")
            else:
                print("For Email,Test case is Fail ")
            #---------------Phone-------------------------------------
            current_phones = innoData.get('phone_numbers')
            flag_check_phone = True
            if expected_current_collection.get('phone_numbers') and current_phones:
                for phone in current_phones:
                    #import ipdb; ipdb.set_trace()
                    for phone_data in expected_current_collection.get('phone_numbers',[]):
                        if phone.get('phone','') == phone_data:
                            flag_check_phone = True
                        else:
                            flag_check_phone = False
            # Both are empty
            elif not expected_current_collection['emails'] and not current_phones:
                flag_check_phone = True
            #One of them is empty
            else:
                flag_check_phone = False
            if flag_check_phone:
                print("For Phone,Test case is Pass ")
            else:
                print("For Phone,Test case is Fail ")
         #-----------address-----------------------------------------------
            current_addresses = innoData.get('addresses')
            flag_check_address = True
            if expected_current_collection.get('addresses') and current_addresses:
             for address in current_addresses:
                 #import ipdb; ipdb.set_trace()
                 for address_data in expected_current_collection.get('addresses',[]):
                     if address.get('address','') == address_data:
                         flag_check_address = True
                     else:
                         flag_check_address = False
             # Both are empty
            elif not expected_current_collection['addresses'] and not current_addresses:
             flag_check_address = True
             #One of them is empty
            else:
             flag_check_address = False

            if flag_check_address:
                print("For Address,Test case is Pass ")
            else:
                print("For Address,Test case is Fail ")
        else:
            print("You can not access Merck Data")
    else:
        print(req.status_code, req.text)
#---------------------------------------------------------
#       6.Create Environment for Merck
#---------------------------------------------------------


def run_condition_Merck(body, condition_type):
    '''
    run_condition(test_case_senario,"Scenario")
    run_condition(test_case_execute,"Test cast")
    '''
    print('-' * 3, "Executing " + condition_type + " ", '-' * 3)
    #print('-' * 3, "Creating Environment for Test Case")
    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        'id': KOL_ID,
        'dc': 'as'
    }

    body = json.dumps(body)

    req = requests.put('url/api/v2/kols/' + KOL_ID + '',
                       params=params, data=body, headers=Header)
    print(condition_type, "status : ", req.status_code)
    #print("Environment Created!!! : ", req.status_code)
    #test_case_results['create_environment'] = "success"
#---------------------------------------------------------
#                    6.DB collections
#---------------------------------------------------------
# To check History collection


def check_history_collection():

    collection = get_collections_history()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING).limit(1)
    history_data = find_last_history[0]
    #print(history_data)

    #perform_check(history_data,'test_case_history', "History")
    return history_data
# To check History collection


def check_history_collection_client():

    collection = get_collections_history_Merck()
    find_last_history = collection.find({'author_id': KOL_ID}).sort(
        'last_updated_dev', DESCENDING).limit(1)
    history_data = find_last_history[0]

    #perform_check(history_data, test_case, 'test_case_history', "History")
    return history_data

#---------------------------------------------------------
#                     Final Step
#---------------------------------------------------------

def run_test_cases():

    get_auth_token()
    for test_case in TEST_CASES:
        print("-" * 20)
        print("Executing",test_case['test_case_name'])
        print("-" * 20)
        print(test_case['test_case_descripation'])
        print("-" * 20)
        print(test_case['precondition'])
        print("-" * 20)
        run_condition(
            test_case['test_case_senario'], 'Scenario')
        time.sleep(5)
        run_condition(
            test_case['test_case_execute'], 'Test Case')
        time.sleep(5)
        check_execution_status_current_collection(
            User_details, test_case,'application')
        print("-" * 20)
        perform_check(test_case,'test_case_senario','History',User_details)
        print("-" * 20)
        check_execution_status_on_client_current_collection(User_details,test_case,'Merck')
        print("-" * 20)
        time.sleep(5)
        check_history_collection()
        # get_auth_token(User_details)
        print("-" * 20)

        '''

    get_auth_token(User_details)
    for test_cases_merck in TEST_CASES_MERCK:
        print("-" * 20)
        print("Executing",test_cases_merck['test_case_name'])
        print("-" * 20)
        print(test_cases_merck['test_case_descripation'])
        print("-" * 20)
        print(test_cases_merck['precondition'])
        print("-" * 20)
        run_condition(
            test_cases_merck['test_case_senario'], 'Scenario')
        time.sleep(5)
        print("-" * 20)
        run_condition(
            test_cases_merck['test_case_execute'], 'Test Case')
        time.sleep(5)
        print("-" * 20)
        check_execution_status_on_client_current_collection(
            User_details, test_cases_merck, 'Merck')
        time.sleep(5)
        print("-" * 20)

        perform_check_client(test_cases_merck,'test_case_senario','History')
        print("-" * 20)
        check_execution_status_current_collection(
            User_details, test_cases_merck,'application')
        get_auth_token(User_details)

        print("-" * 20)
        '''

#---------------------------------------------------------


if __name__ == '__main__':
    run_test_cases()
