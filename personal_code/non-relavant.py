from accounts import User_details, User_details
import requests
from elasticsearch import Elasticsearch
from body import TEST_CASES

from non_relavant_config import clinical_trails_kols_query , publication_kol_query
import json
global_client = Elasticsearch(["10.240.0.18:9200"])
import csv
import os.path
import pprint
out_filename = "Non_relavant_profiles.csv"

AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjIyMjY4MjMsInNlc3Npb25fa2V5IjoiYjAzOTYxMTctNjA4YS00YWVhLWI1ZGUtNTAxYjAyOWFiYmVhIiwiZXhwIjoxNTIyMzEzMjIzfQ.u_XzePzvmhT8EAUFdkkwajoYZnXgtXiOOydDu7ITeII"


Application's_url
def auth_login(account):
    r = requests.post('https://qa.sso.application.de/api/v0/oauth/login/?client_id=lpGgje82IkDfvg1HF333X6NXeQHnvJkuvyAWps3X&response_type=code&scope=login%20info%20permissions%20filters%20roles%20plan%20hierarchy', json=account)
    #print(r.status_code)
    # #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    codes = code[29:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        'url/api/v1/auth/callback', params=payload)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    print("Granted access token:", accesstoken[10:])
    return accesstoken
    # to make Auth token generic


def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    global AUTH_TOKEN_Merck
    AUTH_TOKEN_Merck = auth_callback_api(account)

def author_id(body):

    #print('-' * 3, "Executing " + condition_type + " ", '-' * 3)

    #print('-' * 3, "Creating Environment for Test Case")

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    req = requests.post(BASE_API + 'kols/score/',
                        data=json.dumps(body), headers=Header)

    print("status : ", req.status_code)
    #print(json.dumps(req.json()))
    inno_data = req.json().get('data',[])
    for data in inno_data :
        score_data = data.get('scores',[])

        for author_data in score_data:
            author_id = author_data.get('author_id','')


            with open('Preferred_terms.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                preferred_terms = []
                for row in reader:
                    import ipdb; ipdb.set_trace()
                    preferred_terms.append(row['Preferred Terms'])
                    for terms in preferred_terms :
                        query = clinical_trails_kols_query(terms,author_id)
                        query_publication = publication_kol_query(terms,author_id)

                    response_publication = global_client.search(index='index_name' , body=query_publication , request_timeout=100)

                    response = global_client.search(index="index_name", body = query)
		    #clinical_trails_kols_query(author_id,Neurology)
		    # print(BASE_API + 'kols/score/')
		    # print("Response Body : ",req)
				    for docs in response["hits"]["hits"] :

		   clinical_trail_ids = docs.get('_source',{}).get('clinical_id')
					#print(clinical_trail_ids)
					keyword = docs.get('_source',{}).get('keyword',[])

					# for key in keyword :
					#     print(key)
					print("Download your result file")
					write_csv(author_id,row['Preferred Terms'],clinical_trail_ids,keyword)

		    # f= open("guru99.txt","w+")
		    # f.write(json.dumps(response))
		    # f.close()
                    # query_CT = clinical_trails_kols_query(row['Preferred Terms'],author_id)
                    # response = global_client.search(index="index_name", body = query_CT)
                    # for docs in response["hits"]["hits"] :
                    #     clinical_trail_ids = docs.get('_source',{}).get('clinical_id')
                    #     #print(clinical_trail_ids)
                    #     keyword = docs.get('_source',{}).get('keyword',[])
                    #     # for key in keyword :
                    #     #     print(key)
                    #     print("Download your result file")
                    #     write_csv(author_id,row['Preferred Terms'],clinical_trail_ids,keyword)

                    query_publication=publication_kol_query(row['Preferred Terms'],author_id)
                    import ipdb; ipdb.set_trace()
                    pprint.pprint(query_publication)
                    response=global_client.search(index="index_name", body = query_publication)
                    print(response)
                    f= open("guru99.txt","w+")
                    f.write(json.dumps(response))
                    f.close()

def write_csv(author_id,indication,clinical_trail_ids,keyword):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author ID','Indications','clinical Trail ids','keywords']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow({'Author ID':author_id ,'Indications':indication,'clinical Trail ids' :clinical_trail_ids,'keywords':keyword})
def run_test_cases():
    author_id(TEST_CASES["Neurology"])
    excute_test_case()


if __name__ == '__main__':
    run_test_cases()
