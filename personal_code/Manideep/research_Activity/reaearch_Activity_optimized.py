import requests
from account import User_details, User_details
import json
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
import time
import re
import pprint
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

list_staging=[]
list_pro=[]
Application's_url
pags = ''
# reload(sys)
# sys.setdefaultencoding("utf-8")
#AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mjc1ODMyMTIsInNlc3Npb25fa2V5IjoiZTQwNDhhYjMtMWMzOC00ZTYzLWE4M2YtZTE5Y2FjYzFlMzc5IiwiZXhwIjoxNTI3NjY5NjEyfQ.AzJx7t3tipVjSmaDrt5N2AX83Cu0-aDjtlLud3qbUiU"
# filename="filter.csv"
# list_filename=filename.split('.')
# now = time.strftime('%d-%m-%Y %H:%M:%S')
# out_filename=list_filename[0]+'.'+list_filename[1]
out_filename="demo.csv"

def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    print(r.status_code)

    # pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes
def auth_callback_api(account):

    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get('application_callback_url', params=payload)
    print(req.status_code)

    accesstoken_dict = req.json()
    # print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    AUTH_TOKEN = auth_callback_api(account)
    # print AUTH_TOKEN
    return AUTH_TOKEN

def read_csv(input_file):
    get_auth_token(User_details)
    input_file = "author_id.csv"
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0

        for line in reader  :
            author_id=line.get('Author_id')
            count=count + 1
            print(count)
            research_activity(author_id)

asset_class_keys = {'Publications':'article_title','Clinical Trials':'public_title','Congresses':'title','Societies':'body_name','Guidelines':'title','Advocacy':'body_name','HTA':'body_name','Top Hospital KOLs':'body_name','Regulatory bodies':'body_name','Patents':'title','Press Releases':'title','Diagnostic Centers':'body_name'}

def research_activity(author_id):
    print(author_id)
    # AUTH_TOKEN=get_auth_token(User_details)
    AUTH_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzc3ODc1ODgsInNlc3Npb25fa2V5IjoiYWE1ZWNiOTItYWM4MS00OWY4LTlmZjMtZTViMTllNTU0ODEzIiwiZXhwIjoxNTM3ODczOTg4fQ.n3q71YSloWVXhw8RxhGcJY5bIuiPHTJEei4qcjLDcpA'

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    url = BASE_API + 'kols/profile/' + author_id + '/research_activity'
    res = requests.get(url, params, headers=Header)
    print("status : ", res.status_code)
    inno_data = res.json().get('data',[])
    #print(json.dumps(inno_data))

    for dict in inno_data:
        count=dict.get('count')
        name=dict.get('name')
        author_info = {}
        author_info = {'Author id':author_id,'Name':name,'Count':count}
        print(name)
        docs=dict.get('docs')
        vars()[name.replace(' ','_')] = []
        for doc in docs:
            vars()[name.replace(' ','_')].append(doc.get(asset_class_keys[name],''))
        author_info[name] = vars()[name.replace(' ','_')]
        write_csv(author_info)

def write_csv(author_info):
       file_exists = os.path.isfile(out_filename)
       with open(out_filename, 'a') as csvfile:
           feild_names= ['Author id','Name','Count','Publications','Clinical Trials','Congresses','Societies','Guidelines','Advocacy','Regulatory Body','HTA','Top Hospital KOLs','Regulatory bodies','Patents','Press Releases','Diagnostic Centers']
           writer = csv.DictWriter(csvfile, fieldnames=feild_names)
           if not file_exists:
               writer.writeheader()
           writer.writerow(author_info)

if __name__ == '__main__':

    read_csv("author_id.csv")
