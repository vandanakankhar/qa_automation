from datetime import datetime,  timedelta
import time

class ES_queries:

    def query_publication_using_kol_name(self, name, author_id):
            """Elastic query to using author_name in the publications index for time range of 1 year"""
        #Hit ES: Publications index to match:
        #input name in authors.author_name AND created_at <= last 1 year
            d1 = datetime.now()
            #d1 = 1396310400

            #d2 = 1288051200
            d2 = datetime.now() - timedelta(days=1825)
            current_date = int(time.mktime(d1.timetuple()))
            last_year_date = int(time.mktime(d2.timetuple()))
            body = {
                      "query":
                      {
                        "bool" : {
                          "must_not" : [
                            {
                              "match": {
                                "authors.author_id": author_id
                              }
                            }
                          ],
                          "must" : [
                            {
                              "match": {
                                "authors.author_name": name
                              }
                            },
                            {
                              "range": {
                                "created_at": {
                                  "gte": last_year_date,
                                  "lte": current_date
                                }
                              }
                            }
                          ]
                        }
                      },
                      "size":1000
                    }

            return body
    def query_publication_using_author_id(self,author_id):
        d1 = datetime.now()
        d2 = datetime.now() - timedelta(days=1825)
        current_date = int(time.mktime(d1.timetuple()))
        last_year_date = int(time.mktime(d2.timetuple()))
        body_author_id = {
                            "sort":[{"created_at":{"order":"desc"}}],
                        	"query": {
                        		"bool": {
                        			"must": [{
                        					"match": {
                        						"authors.author_id": author_id
                        					}
                        				},
                        				{
                        					"range": {
                        						"created_at": {
                        							"gte": last_year_date,
                        							"lte": current_date
                        						}
                        					}
                        				}
                        			]
                        		}


                        },
                        "size": 1000
                        }

        return body_author_id

    def query_publication_using_author_id_getting_affliation(self,author_id):
        """ Getting affiliations using author_id in publication_v4 index"""
        d1 = datetime.now()
        d2 = datetime.now() - timedelta(days=365)
        current_date = int(time.mktime(d1.timetuple()))
        last_year_date = int(time.mktime(d2.timetuple()))
        body={

              "query": {
                  "bool": {
                      "must": [{
                              "match": {
                                  "authors.author_id": author_id
                              }
                          },
                          {
                              "range": {
                                  "created_at": {
                                      "gte": last_year_date,
                                      "lte": current_date
                                  }
                              }
                          }
                      ]
                  }
              },
              "size": 0,
              "aggs": {
                "affiliations": {
                  "terms": {
                    "field": "authors.author_affiliation"
                  }
                }
              },
              "size": 200
            }
        return body

    def query_clinical_trials_using_kol_name(self,author_name,author_id):
        d1 = datetime.now()
        d2 = datetime.now() - timedelta(days=365)
        current_date = int(time.mktime(d1.timetuple()))
        last_year_date = int(time.mktime(d2.timetuple()))
        body={
              "sort": {
                "created_at": "desc"
              },
              "query": {
                "bool": {
                  "must": [
                    {
                      "nested": {
                        "path": "authors",
                        "query": {
                          "bool": {
                            "must_not" : [
                              {
                                "match": {
                                  "authors.author_id": author_id
                                }
                              }
                            ],
                            "must" : [
                              {
                                "match": {
                                  "authors.author_name": author_name
                                }
                              }
                            ]
                          }
                        }
                      }
                    },
                    {
                      "range": {
                        "created_at": {
                          "gte": last_year_date,
                          "lte": current_date
                        }
                      }
                    }]
                }
              },
              "size": 1000
            }

        return body

    def query_clinical_trials_using_kol_id(self,kol_id):
        d1 = datetime.now()
        d2 = datetime.now() - timedelta(days=365)
        current_date = int(time.mktime(d1.timetuple()))
        last_year_date = int(time.mktime(d2.timetuple()))
        body = {
                  "sort": {
                    "created_at": "desc"
                  },
                  "query": {
                    "bool": {
                      "must": [
                        {
                          "nested": {
                            "path": "authors",
                            "query": {
                              "bool": {
                                "must" : [
                                  {
                                    "match": {
                                      "authors.author_id": kol_id
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        },
                        {
                          "range": {
                            "created_at": {
                              "gte": last_year_date,
                              "lte": current_date
                            }
                          }
                        }]
                    }
                  },
                  "size": 200
                }
        return body
