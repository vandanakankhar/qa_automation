from elasticsearch import Elasticsearch
import requests
import csv
import pprint
import os.path
import time
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

global_client = Elasticsearch(['elastic_Search_username_password'])
out_filename = os.getcwd()+"/"+time.ctime(int(time.time())).replace(':','_').replace(' ','_')+".csv"

def es_query(author_id):
    body={
        "query": {
            "match": {
                "author_id": author_id
            }
        }
    }
    return body

def write_result(name,author_id,affiliation, country,ta):
    file_exists=os.path.isfile(out_filename)
    with open(out_filename, 'a') as csvfile:
        fieldnames = ['Name', 'affiliation in Author corpus', 'country in Author corpus','author_id' , 'TA']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if not file_exists:
            writer.writeheader()
        writer.writerow({'Name': name.encode('utf-8'), 'affiliation in Author corpus': affiliation.encode('utf-8'), 'country in Author corpus':country.encode('utf-8'),'author_id':author_id.encode('utf-8'), 'TA':ta})

def get_kol_info(input_file,TA):
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count = 0
        for line in reader :
            author_id=line.get("Author_id")
            count=count+1
            print(author_id,"=========================================",count)

            body=es_query(author_id)
            response =global_client.search(index="authors_alias", body = body, request_timeout=10)

            for doc in response['hits']['hits']:
                author_country=''
                author_name=doc.get('_source',{}).get('name','').encode('utf-8')
                author_affiliation=doc.get('_source',{}).get('current_affiliation',{}).get('affiliation','').encode('utf-8')
                for country in doc.get('_source',{}).get('current_affiliation',{}).get('extracted_terms',{}).get('country',[]):
                    author_country=country.encode('utf-8')
                write_result(author_name,author_id,author_affiliation,author_country,TA)

if __name__=='__main__':
    TA = sys.argv[1]
    print("kol_information script has started and the TA is :"+TA)
    get_kol_info("onco.csv",TA)
