import requests
import json
import urllib3.contrib.pyopenssl
from xlsxcessive.xlsx import Workbook, save

urllib3.contrib.pyopenssl.inject_into_urllib3()
url = 'url/api/v1/kols/score'
author_url = 'url/api/v1/kols/list?asset_class=Publications&author_ids=%s&fields=name,affiliation,author_id,country'
headers = {
	"Authorization": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MTIzNjg0NTMsInNlc3Npb25fa2V5IjoiYWVjOTI4MGYtMGFjYy00NmJlLWEyOTItMjQxN2FkYjkzMzM2IiwiZXhwIjoxNTEyNDU0ODUzfQ.O17w4IpXI0uM6nV5Q8Cw4JZIbPLXu8AoFjWiLo0IhuE",
	"Content-Type": "application/json"
}
TA = [
	# "Reproductive Health",
	"Oncology"
]
Resources = [
	# "Top KOLs",
	# "Publications",
	# "Clinical Trials",
	# "Congresses",
	# "Regulatory Bodies",
	# "HTA",
	# "Advocacy",
	# "Societies",
	# "Top Hospital KOLs",
	# "Guidelines",
	"Diagnostic Centers"
]
Indications = [
	# "Male Infertility",
	# "Female Infertility"
	"Breast Cancer"
]
Countries = [
	"No Country Filter",
	# "Switzerland",
	# "Spain",
	# "Italy",
	"Germany",
	"United Kingdom",
	"France"
]

date_asset_classes = ["Publications", "Clinical Trials", "Congresses", "Guidelines"]
payload = {
	"from": 0,
	"limit": 10,
	"TA": "Oncology",
	"asset_class": "Top KOLs",
	"filters": {
	"Indications": [
		"Female Infertility"
	],
	"kol_info": [
      {
        "name": "Publications",
        "value": 15,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Clinical Trials",
        "value": 13,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Congresses",
        "value": 12,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Guidelines",
        "value": 10,
        "showDate": True,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Regulatory Bodies",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "HTA",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Societies",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Advocacy",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Top Hospital KOLs",
        "value": 10,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      },
      {
        "name": "Diagnostic Centers",
        "value": 0,
        "showDate": False,
        "startDate": 0,
        "endDate": 0
      }
	],
	"kolType": "allkols"
	}
}

payload2 = {
	"from":0,
	"limit":10,
	"TA":"Oncology",
	"asset_class":"Publications",
	"filters":{
		"Indications":[
			"Breast Cancer"
		],
		"kol_info":[
			{
				"name":"Publications",
				"value":100,
				"showDate":False,
				"startDate":0,
				"endDate":0
			}
		],
		"kolType":"emerging"
	}
}
ta = "Oncology"
for indication in Indications:
	payload["TA"] = ta
	payload["filters"]["Indications"] = [indication]
	wb = Workbook()
	for resource in Resources:
		payload["asset_class"] = resource
		if resource != "Top KOLs":
			payload["filters"]["kol_info"] = [
				{
					"name": resource,
					"value": 100,
					# "showDate": False,
					"showDate": True if resource in date_asset_classes else False,
					"startDate": 0,
					"endDate": 0
				}
			]
		row = 0
		s1 = wb.new_sheet(resource)
		for country in Countries:
			if country != "No Country Filter":
				payload["filters"]["Countries"] = [country]
			else:
				if payload["filters"].get("Countries"):
					payload["filters"].pop("Countries")
			headerfmt = wb.stylesheet.new_format()
			headerfmt.font(size=10, bold=True)
			s1.cell(coords=(row,1), value=country, format=headerfmt)
			row += 1

			header = ['author_id', 'Name', 'Country', 'Affiliation', 'Score']
			for col, label in enumerate(header):
				s1.cell(coords=(row,col), value=label, format=headerfmt)
			row += 1

			r = requests.post(url, data=json.dumps(payload), headers=headers)
			docs = r.json()["data"][0]["scores"]
			author_ids = [doc["author_id"] for doc in docs]
			author_ids_string = ','.join(author_ids)
			r2 = requests.get(author_url % author_ids_string,  headers=headers)
			if r2.status_code == 200:
				names_list = r2.json()["data"]
			else:
				print country, resource
				break
			for i in range(0, len(docs)):
				doc = docs[i]
				print row, country, resource
				author_id = doc["author_id"]
				score = doc["score"]
				if i <= len(names_list) -1 and names_list[i]:
					name = names_list[i].get("nm") if names_list[i].get("nm") else ""
					country_name = names_list[i].get("macnty") if names_list[i].get("macnty") else ""
					aff = names_list[i].get("aff") if names_list[i].get("aff") else ""
				else:
					name = ""
					country_name = ""
					aff = ""
				s1.cell(coords=(row, 0), value=author_id)
				s1.cell(coords=(row, 1), value=name)
				s1.cell(coords=(row, 2), value=country_name)
				s1.cell(coords=(row, 3), value=aff)
				s1.cell(coords=(row, 4), value=score)
				row += 1
			row += 2
	save(wb, ta + "_" + indication + '.xlsx')
