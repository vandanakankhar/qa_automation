TA_synonyms = {
#"neurology": ["neurology", "neuro", "Alzheimer", "Parkinson" ,"Brain","Sclerosis","Dementia"]
"oncology":['cancer', 'parp', 'olaparib', 'carcinoma', 'parp-1', 'parp-2', 'malignancy', 'breast cancer', 'talazoparib', 'parp inhibitor', 'tumor']
# "oncology":["Uveal melanoma","Ocular melanoma","choroidal","eye cancer", "eye melanoma","iris","ocular oncology","uvea"]
#"gastroenterology":["Bowel Disease","irritable bowel syndrome","spastic colitis","mucus colitis","ibd","ibs","colon spasm"]
#"reproductive health":["In-vitro Fertilization","Assisted reproductive technology (ART)","Embryo Transfer","oocyte retrieval","embryo assessment","Ovulation Induction","Artificial insemination","Ovulation Induction"]
#"cardiology": ["Chronic Heart Failure","Cardiac Failure","Heart Decompensation","Myocardial Failure","Congestive Heart Failure","Essential Hypertension","Ventricular Dysfunction","ADHF","Sacubitril","Valsartan","Entresto","Right-Sided Heart Failure","Left-Sided Heart Failure"]
# "dermatology" : ["Derma","dermatology","dermatitis","psoriasis","eczema","skin diseases"]
#"endocrinology": ["Thyroid", "hypothyroidism", "hyperthyroidism", "obesity", "endocrinology", "endocrine disorders", "goitre"]
}

asset_class_mappings = {
"publications": "non_nested",
"congresses": "nested"
}
