#Bandar Nasser Al Jafen
from itertools import permutations

def get_initial_current_word_not_persist_existing(fullname_list):
    # start to end: get initial of one and keep others as it is
    # B Nasser Al Jafen
    # Bandar N Al Jafen
    # Bandar Nasser A Jafen
    names_with_initial = []
    for i in range(0, len(fullname_list)-1):
        name = ""
        for j in xrange(0, len(fullname_list)):
           if j == i:
              name = name + " " + fullname_list[j][0]
           else:
              name = name + " " + fullname_list[j]
        names_with_initial.append(name.strip())

    return names_with_initial

def namef(l,i):
    x = l[i][0]
    l[i] = x
    return l

def get_initial_of_one_and_persist_previous_initial(name):
    list1 = name.split()
    count = len(list1) - 1
    i = count - 1
    l2 = []
    final = []
    while i >= 0 :
        l = []
        #import ipdb; ipdb.set_trace()
        #print 'Before : ', l2
        # l2 += [namef(list1,i)]
        l.append(" ".join(namef(list1,i)))
        final.extend(l)

        # l2[i] = namef(list1,i)
        #print 'After :' , l2
        i = i-1

    return list(set(final))


def getInitialBasedName(fullname):
    names_with_initial = []
    fullname_list = fullname.split()
    for i in range(0, len(fullname_list)-1):
        name = ""
        for j in xrange(0, len(fullname_list)):
            if i >= j:
               name = name + " " + fullname_list[j][0]
            else:
               name = name + " " + fullname_list[j]
        names_with_initial.append(name.strip())

    s1 = get_initial_current_word_not_persist_existing(fullname_list)
    s2 = get_initial_of_one_and_persist_previous_initial(fullname)
    list_s1= s1 + s2
    final_list1 = list_s1 + names_with_initial
    merged_list = list(set(final_list1))
    y_list=[]
    if len(merged_list)==3:
        for list1 in merged_list:
            x_list=list1.split()
            y_str = x_list[0] +" " +x_list[2]
            y_list.append(y_str)

    merged_list = merged_list + y_list
    return merged_list

def getAllPossible(name_com):
    name=name_com.split()
    dict_merck={}

    if len(name)<=4:
        a=[]
        b=[]

        for i in permutations(name):
            a.append((" ").join(i))


        b.extend(a)
        for j in a:
            b.extend(getInitialBasedName(j))

        import ipdb; ipdb.set_trace()
        dict_merck[name_com]=list(set(b))
        return dict_merck

    dict_merck[name_com] = [name_com]
    import ipdb; ipdb.set_trace()
    print(dict_merck)
    return dict_merck

getAllPossible("Vandana Pakya kankhar")
