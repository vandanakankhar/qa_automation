'''
Author: Nikhil Fulzele
Date: 11/04/2017
Desc: Jaccard Similarity
'''

'''
Method to calculate the jaccard similarity of given two input list
Arguments:
Input:
1) input1 <list<any:int, string, float>>
2) input2 <list<any:int, string, float>>
3) jaccard_type <str> : <defalut, min, max> : type of jaccard to be calculated
4) repeation <bool> : Default is True : Whether to consider repeation of not
5) distance <inte> : allowed difference between two strings #not implemented yet
Output:
Float value between 0 and 1
'''
def jaccard_similarity(input1, input2, jaccard_type='default', repeation=True, distance=1):
	input_list1 = input1
	input_list2 = input2

	if not repeation:
		input_list1 = list(set(input_list1))
		input_list2 = list(set(input_list2))

	total_len = len(input_list1) + len(input_list2)
	min_len = min(len(input_list1), len(input_list2))
	max_len = max(len(input_list1), len(input_list2))

	terms_dict = {}
	for term in input_list1:
		if term in terms_dict:
			terms_dict[term] += 1
		else:
			terms_dict[term] = 1

	for term in input_list2:
		if term in terms_dict:
			terms_dict[term] -= 1
		else:
			terms_dict[term] = -1

	miss_match = 0
	for entry in terms_dict:
		miss_match += abs(terms_dict[entry])

	match = (total_len-miss_match)/2.0

	if jaccard_type == 'default':
		try:
			return match/float(match + miss_match)
		except ZeroDivisionError:
			return 0.0

	if jaccard_type == 'min':
		try:
			return match/float(min_len)
		except ZeroDivisionError:
			return 0.0

	if jaccard_type == 'max':
		try:
			return match/float(max_len)
		except ZeroDivisionError:
			return 0.0

if __name__ == '__main__':
	print jaccard_similarity([1,2,3],[2,3,4,5,2])
	print jaccard_similarity([1,2,3],[2,3,4,5,2],'max')
	print jaccard_similarity([1,2,3],[2,3,4,5,2],'min')
	print ""
	print jaccard_similarity([1,2,3],[2,3,4,5,2],repeation=False)
	print jaccard_similarity([1,2,3],[2,3,4,5,2],'max',repeation=False)
	print jaccard_similarity([1,2,3],[2,3,4,5,2],'min',repeation=False)
