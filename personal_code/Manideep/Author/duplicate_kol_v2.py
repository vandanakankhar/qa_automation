import config
from jaccard_similarity import jaccard_similarity
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
import json
import csv
import pprint
import sys
import multiprocessing
from itertools import permutations
reload(sys)
sys.setdefaultencoding("utf-8")
import time
import pymongo
from pymongo import MongoClient
import pdb
import requests
from datetime import datetime
from elasticsearch import Elasticsearch
global_client = elastic_Search_ip)
from time import gmtime, strftime
path ='/home/yash.pandey/Downloads/'
import multiprocessing
from itertools import permutations
import os.path
out_filename = "duplicate_kol_v2_output.csv"

class duplicate_kol_test(object):
    def get_initial_current_word_not_persist_existing(self ,fullname_list):
        names_with_initial = []
        for i in range(0, len(fullname_list)-1):
            name = ""
            for j in xrange(0, len(fullname_list)):
               if j == i:
                  name = name + " " + fullname_list[j][0]
               else:
                  name = name + " " + fullname_list[j]
            names_with_initial.append(name.strip())

        return names_with_initial

    def namef(self,l,i):
        x = l[i][0]
        l[i] = x
        return l

    def get_initial_of_one_and_persist_previous_initial_include_last(self,name):
        list1 = name.split()
        count = len(list1) - 1
        i = count
        l2 = []
        final = []
        while i >= 0 :
            l = []
            #print 'Before : ', l2
            # l2 += [namef(list1,i)]
            l.append(" ".join(self.namef(list1,i)))
            final.extend(l)

            # l2[i] = namef(list1,i)
            #print 'After :' , l2
            i = i-1

        return list(set(final))

    def get_initial_of_one_and_persist_previous_initial(self,name):
        list1 = name.split()
        count = len(list1) - 1
        i = count - 1
        l2 = []
        final = []
        while i >= 0 :
            l = []
            l.append(" ".join(self.namef(list1,i)))
            final.extend(l)
            i = i-1

        return list(set(final))


    def getInitialBasedName(self ,fullname):
        names_with_initial = []
        fullname_list = fullname.split()
        for i in range(0, len(fullname_list)-1):
            name = ""
            for j in xrange(0, len(fullname_list)):
                if i >= j:
                   name = name + " " + fullname_list[j][0]
                else:
                   name = name + " " + fullname_list[j]
            names_with_initial.append(name.strip())

        s1 = self.get_initial_current_word_not_persist_existing(fullname_list)
        s2 = self.get_initial_of_one_and_persist_previous_initial(fullname)
        s3 = self.get_initial_of_one_and_persist_previous_initial_include_last(fullname)
        list_s1= s1 + s2 + s3
        final_list1 = list_s1 + names_with_initial
        merged_list = list(set(final_list1))
        y_list=[]
        if len(fullname)==3:
                x_list=fullname.split()
                y_str = x_list[0] +" " +x_list[2]
                y_list.append(y_str)

        merged_list = merged_list + y_list
        return merged_list

    def getAllPossible(self ,name_com):
        name=name_com.split()
        dict_merck={}

        if len(name)<=4:
            a=[]
            b=[]

            for i in permutations(name):
                a.append((" ").join(i))


            b.extend(a)
            for j in a:
                b.extend(self.getInitialBasedName(j))

            dict_merck[name_com]=list(set(b))
            return dict_merck

        dict_merck[name_com] = [name_com]
        print(dict_merck)
        return dict_merck

    def get_searchable_name(self,name):
        searchable_name = []
        name_list = name.split()
        #print name_list
        for n in name_list:
            if len(n) > 1:
                searchable_name.append(n)


        #searchable_name.sort() #uuuu
        name = " ".join(searchable_name)
        return name

    def build_body(self , name ):
        query = {
                  "query": {
                    "bool": {
                      "must_not": [
                        {
                          "term": {
                            "is_normalize": {
                              "value": False
                            }
                          }
                        }
                      ],
                      "must": [
                        {
                          "bool": {
                            "minimum_should_match": 1,
                            "should": [
                              {
                                "match": {
                                  "name.autocomplete": name
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  },
                  "size": 500
                }
        return query

    def get_research_activity(self, author_id, asset_class):
        mapping_type = config.asset_class_mappings.get(asset_class)
        if mapping_type == 'non_nested':
            body = {
                      "sort": {
                        "created_at": "desc"
                      },
                      "query" :{
                        "match":{
                          "authors.author_id": author_id
                        }
                      }
                    }

        elif mapping_type == 'nested':
            body = {
                     "sort": {
                       "created_at": "desc"
                     },
                     "query": {
                       "nested": {
                         "path": "authors",
                         "query": {
                           "bool": {
                             "must": [
                               {
                                 "terms": {
                                   "authors.author_id": [
                                     author_id
                                   ]
                                 }
                               }
                             ]
                           }
                         }
                       }
                     }
                    }

        response = global_client.search(index=asset_class+"_alias", body = body , request_timeout=100)
        return response



    def affiliation_generator(self, affiliation):

        affiliations = []
        countries = []
        header={
        'content-type': 'application/json'
        }
        payload =  {"term": affiliation}
        response = requests.post('URL', data=json.dumps(payload), headers=header)
        if response.status_code == 200:
            responses = response.json()
            for response in responses:
                affiliations.extend(response.get('sponsor',[]))
                countries.extend(response.get('country',[]))

                if not affiliations:
                    affiliations.extend(response.get('subDivision',[]))

        return affiliations,countries


    def name_match(self ,name, elastic_name):
        new_name = re.sub(r'[^a-z]', '', elastic_name.lower())
        new_arrname = re.sub(r'[^a-z]', '', name.lower())

        if(new_name==new_arrname):
             return True
        else:
            return False

    def remove_stopwords(self, affiliation):
        stop_words = set(stopwords.words('english'))
        word_tokens = word_tokenize(affiliation)
        filtered_sentence = [w for w in word_tokens if not w in stop_words]

        return filtered_sentence

    def pre_process_affiliations(self, affiliations):
        affiliations_clean = []
        for aff in affiliations:
            aff = aff.lower()
            aff = aff.encode('ascii','ignore')
            if aff and aff not in affiliations_clean:
                affiliations_clean.append(aff)

        return affiliations_clean

    def compare_affiliations(self, affiliations, elastic_affiliations):
        affiliations = self.pre_process_affiliations(affiliations)
        elastic_affiliations = self.pre_process_affiliations(elastic_affiliations)
        flag = 'Non empty profile'
        affiliation_list_of_list = []
        elastic_affiliation_list_of_list = []
        for affiliation in affiliations:
            affiliation_list = self.remove_stopwords(affiliation)
            affiliation_list_of_list.append(affiliation_list)

        for elastic_affiliation in elastic_affiliations :
            elastic_affiliation_list = self.remove_stopwords(elastic_affiliation)
            elastic_affiliation_list_of_list.append(elastic_affiliation_list)


        for affiliations in affiliation_list_of_list :
            for elastic_affiliations in elastic_affiliation_list_of_list :
                similarity_score = jaccard_similarity(affiliations,elastic_affiliations)
                if similarity_score >= 0.60:
                       return True , flag

        if not affiliations or not elastic_affiliations:
            flag = 'empty profile'
            return True , flag

        return False , flag
    def pre_process_countries(self, countries):
        countries_clean = []
        for country in countries:
            country = country.lower()
            if country and country not in countries_clean:
                countries_clean.append(country)

        return countries

    def compare_country(self, countries, elastic_country ,flag):
        flag = 'Non empty profile'
        countries = self.pre_process_countries(countries)
        elastic_country = self.pre_process_countries(elastic_country)

        if not countries or not elastic_country:
            flag = 'empty profile'
            return True , flag
        elif countries:
            country_intersection = list(set(countries).intersection(elastic_country))
            if country_intersection :
                return True , flag

        return False , flag

    def compare_therapeutic_area_synonyms(self, TA, string):
        TA_synonyms = config.TA_synonyms.get(TA.lower().strip())
        for synonym in TA_synonyms:
            if synonym in string:
                return True
        return False

    def compare_author_affiliations(self, doc, author_id, input_affiliations):

        authors = doc.get('authors')
        if authors:
            for author in authors:
                if author.get('author_id') == author_id:
                    affiliations = author.get('author_affiliation')
                    if not affiliations:
                        affiliations = author.get('affiliation', [])

                    author_affiliations, author_countries = \
                                self.affiliation_generator("; ".join(affiliations))
                    affiliation_compare_result  = self.compare_affiliations(\
                                                input_affiliations, author_affiliations)

                    if affiliation_compare_result and affiliations:
                        return True

        return False

    def compare_research_activity_publications(self, author_id, data, input_affiliations, TA):
        for doc in data:
            journal_title_result=''
            doc = doc['_source']
            if not doc :
                flag = 'empty profile'

            else :
                flag = 'Non empty profile'
                author_result = self.compare_author_affiliations(doc, author_id, input_affiliations)
                if author_result:

                    journal_title = doc.get("std_journal_title", "").lower()
                    journal_title_result = self.compare_therapeutic_area_synonyms(TA, journal_title)
                    if journal_title_result:
                         return True , flag

                    article_title = doc.get("article_title", "").lower()
                    article_title_result = self.compare_therapeutic_area_synonyms(TA, article_title)
                    if article_title_result:
                      return True , flag

                    if doc.get("abstract"):
                        abstract = doc.get("abstract").lower()
                        abstract_result = self.compare_therapeutic_area_synonyms(TA , abstract)
                        if abstract_result :
                            return True , flag

        return False , flag

    def compare_research_activity_congress(self, author_id, data, input_affiliations, TA):
        for doc in data:
            congres_name_result = ''
            doc = doc['_source']
            if not doc :
                flag = 'empty profile'
            else :
                flag = 'Non empty profile'
                #author_result = self.compare_author_affiliations(doc, author_id, input_affiliations)
                #if author_result:

                congress_name = doc.get("congress_name", "").lower()
                congres_name_result = self.compare_therapeutic_area_synonyms(TA, congress_name)
                if congres_name_result:
                     return True , flag

                congress_title = doc.get("title", "").lower()
                congress_title_result = self.compare_therapeutic_area_synonyms(TA, congress_title)
                if congress_title_result:
                  return True ,flag

        return False , flag

    def get_filter_author_id(self, docs, name, affiliations, countries, TA):
        flag = ''
        elastic_name = docs.get('_source',{}).get('name','')
        #if elastic_name == 'William Charles Watters' and name =='William Charles Watters' :
            #import ipdb; ipdb.set_trace()
        elastic_affiliation = docs.get('_source',{}).get('current_affiliation',{}).get('affiliation','')
        elastic_country = docs.get('_source',{}).get('current_affiliation',{}).get('extracted_terms',{}).get('country',[])
        elastic_author_id = docs.get('_source',{}).get('author_id','')

        name_match_result = self.name_match(name ,elastic_name)
        if name_match_result:
            combined_elastic_afflication = elastic_affiliation+", "+ ", ".join(elastic_country)
            combined_elastic_afflication = combined_elastic_afflication.strip(',')
            elastic_aff , elastic_countries = self.affiliation_generator(combined_elastic_afflication)

            if not elastic_aff and elastic_affiliation :
                elastic_aff.append(elastic_affiliation)

            if not elastic_countries and elastic_country :
                elastic_countries.append(elastic_country)

            affiliation_result , flag = self.compare_affiliations(affiliations , elastic_aff)
            country_result  , flag = self.compare_country(countries, elastic_countries , flag)

            if affiliation_result or country_result :
                publications_data = self.get_research_activity(elastic_author_id, 'publications')
                if publications_data['hits']['total']:
                    data = publications_data['hits']['hits']
                    pubmed_result , flag = self.compare_research_activity_publications(elastic_author_id, \
                                            data, affiliations, TA)
                    if pubmed_result:
                        return True , flag
                    else:
                        return False , flag =='empty profile'
                else:

                    congress_data = self.get_research_activity(elastic_author_id,'congresses')
                    if congress_data['hits']['total']:
                        data = congress_data['hits']['hits']
                        congress_result , flag =self.compare_research_activity_congress(elastic_author_id,\
                                            data,affiliations, TA)
                        if congress_result:
                            return True , flag
                        else:
                            return False , flag
                    else :
                        return True ,flag
            else :
                return False , flag == 'empty profile'
        else:
            return False ,flag == 'empty profile'

        return False , flag == 'empty profile'

    def write_result(self, name, author_id, affiliation, country, therapeutic_area, possible_duplicates, empty_duplicates_profile):

        file_exists = os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            fieldnames = ['name', 'Input_author_id','affiliation', 'country', 'therapeutic_area', \
                            'possible_duplicates','empty_profile', "duplicate_author_count","empty_profile_count"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'name': name, 'Input_author_id':author_id ,'affiliation': affiliation, 'country':country, \
                                       'therapeutic_area':therapeutic_area, \
                                       'possible_duplicates': "@@@".join(possible_duplicates), \
                                       'empty_profile':"@@@".join(empty_duplicates_profile),'duplicate_author_count':len(possible_duplicates),\
                                       'empty_profile_count':len(empty_duplicates_profile)})

    def read_csv(self , input_file):
        affiliations = []
        countries = []
        count=0
        with open(input_file,'r') as csvfile :
            reader = csv.DictReader(csvfile)
            for line in reader:
                count += 1
                #if count <= 70 :
                    #continue
                possible_duplicates = set()
                empty_duplicates_profile = set()
                input_name = line.get('Name')
                input_affiliation = line.get('affiliation in Author corpus')
                input_country = line.get('country in Author corpus')
                input_therapeutic_area = line.get('TA')
                input_author_id = line.get('author_id')
                combined_affliation= input_affiliation+", "+input_country
                combined_affliation = combined_affliation.strip()
                combined_affliation = combined_affliation.strip(',')
                affiliations, countries = self.affiliation_generator(combined_affliation)

                if not affiliations:
                    affiliations.append(input_affiliation)

                print(input_name)

                countries.append(input_country)
                names_dict = obj.getAllPossible(input_name)
                names = names_dict[input_name]
                for name in names :

                    searchable_name = self.get_searchable_name(name)
                    if searchable_name :
                        body  =  self.build_body(searchable_name)
                        response = global_client.search(index="authors_alias", body = body)

                        for docs in response['hits']['hits']:
                            result ,flag = self.get_filter_author_id(docs , name, \
                                                   affiliations, countries, input_therapeutic_area)
                            if result and flag ==  'Non empty profile':
                                possible_duplicates.add(docs['_source']['name']+' | '+docs['_source']['author_id'])

                            elif result and flag == 'empty profile':
                                empty_duplicates_profile.add(docs['_source']['name']+' | '+docs['_source']['author_id'])

                self.write_result(input_name, input_author_id, input_affiliation, input_country, input_therapeutic_area, possible_duplicates, empty_duplicates_profile)
                print(count)



if __name__=='__main__':
    obj = duplicate_kol_test()
    obj.read_csv('5_Apr_dermaoutput_kol_information.csv')
