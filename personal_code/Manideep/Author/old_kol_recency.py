from elasticsearch import Elasticsearch
import requests
import re
from csv_reader import author_corpus_test
import csv
global_client = Elasticsearch(['elastic_Search_username_password'])
out_filename = 'data_recency_output.csv'
import os.path
from datetime import datetime,  timedelta
import time
from ES_query_kol_recency import ES_queries
import sys
import config



class data_recency(author_corpus_test, ES_queries):
    """ Creating the elastic connection """
    def create_es_connection(self):
        client = elastic_Search_ip)
        return client

    def get_string_id(self, string):
        return re.sub(r'[^a-z]', '', string.lower())

    """ getting the list of affiliation from the elastic search  Querying using author_id"""
    def get_elastic_affiliation(self,name,author_id):
        list_aff=[]
        list_country=[]
        #import ipdb; ipdb.set_trace()
        body =obj.query_publication_using_author_id_getting_affliation(author_id)
        response = global_client.search(index="index_name", body = body, request_timeout=10)

        already_processed = set()
        es_total_affiliations = []
        api_total_affiliations = []
        api_total_countries = []
        key_clean=''


        doc = response.get('aggregations',{}).get('affiliations',{})
        buckets = doc.get('buckets',[])
        bucket_hash = {}
        for bucket in buckets:
            new_key=''
            key = bucket.get('key', '')
            if key:
                keys = obj.remove_stopwords(key)
                new_key=" ".join(keys)

                key_id = self.get_string_id(new_key)
                bucket_hash[key_id] = new_key

        #import ipdb; .set_trace()
        key_clean = "; ".join(bucket_hash.values())
        list_aff,list_country=obj.affliation_generator(key_clean)
        return list_aff

    def get_filter_trail_id(self, elastic_name ,name ,elastic_affiliations, affiliations , countries, elastic_author_id ,therapeutic_area ,research_data):

        name_match_result = self.CT_name_compare_name(name , elastic_name)
        if name_match_result :
            combined_affliation = affiliation_using_author_id.append(elastic_affiliations)
            elastic_coun , elastic_aff = obj.affliation_generator(combined_affliation)
            affiliation_match = obj.affiliation_match(affiliations , elastic_aff)
            if affiliation_match :
                country_result = obj.compare_country(countries , elastic_coun)
                if not affiliation_match and not country_result :
                    clinical_results = self.compare_research_activity_clinical_trails(research_data , therapeutic_area)
                    if clinical_results :
                        return True
                    else:
                        publications_data = obj.get_research_activity(elastic_author_id,'publications')
                        if publications_data['hits']['total']:
                            data = publications_data['hits']['hits']
                            pubmed_result = obj.compare_research_activity_publications(elastic_author_id,\
                                                      data,affiliations,therapeutic_area)
                            if pubmed_result :
                                return True
                        else:
                            return False
                if country_result :
                    return True
                else :
                    return False
            else :
                return False
        else:
            return False

        return False


    """Filtering the kol by applying name, affiliations, country, with the same from  elastic search"""
    def get_filter_pmid(self, elastic_name, name, elastic_affiliations,affiliations,elastic_author_id,author_id ,countries, therapeutic_area, affiliation_using_author_id):
                name_match_result=obj.compare(elastic_name,name)
                if name_match_result:

                    for elastic_affiliation in elastic_affiliations:
                        elastic_affiliation = elastic_affiliation.strip(',')
                        elastic_aff , elastic_coun = obj.affliation_generator(elastic_affiliation)
                        """Adding input affiliation with the affiliation got from quering es using author_id """
                        affiliation_combined = affiliations + affiliation_using_author_id
                        if not elastic_aff:
                            elastic_aff.append(elastic_affiliation)
                        #import ipdb; ipdb.set_trace()
                        """Comparing affiliation  """
                        affiliation_result=obj.compare_affiliations(affiliation_combined,elastic_aff)
                        if affiliation_result:
                            """Comparing country """
                            country_result=obj.compare_country(countries,elastic_coun)
                            if not elastic_aff and not elastic_coun :
                                publications_data = obj.get_research_activity(elastic_author_id,'publications')
                                if publications_data['hits']['total']:
                                    data = publications_data['hits']['hits']
                                    pubmed_result = obj.compare_research_activity_publications(elastic_author_id,\
                                                              data,affiliations,therapeutic_area)
                                    if pubmed_result :
                                        return True
                                    else:
                                        return False
                                else:
                                    congress_data =obj.get_research_activity(elastic_author_id,'congresses')
                                    if congress_data['hits']['total']:
                                        data = congress_data['hits']['hits']
                                        congress_result = obj.compare_research_activity_congress(elastic_author_id,\
                                                                data,affiliations,therapeutic_area)
                                        if congress_result:
                                            return True
                                        else:
                                            return False
                            if country_result:
                                return True
                        else:
                            return False
                else:
                    return False

                return False
    """ Getting the publication id and created at using author_id for comparing it with the other list"""
    def get_publication_id_based_on_author_id(self,author_id):
        body = obj.query_publication_using_author_id(author_id)
        response = global_client.search(index="index_name", body = body, request_timeout=10)
        list2_publication_id=[]
        created_at_list=[]
        for doc in response['hits']['hits']:
            """Getting the publication_id and appending it into the list"""
            list_publication_id = doc.get('_source',{}).get('publication_id',"")
            list2_publication_id.append(list_publication_id)
            list2_created_at=doc.get('_source',{}).get('created_at')
            created_at_list.append(list2_created_at)

        return list2_publication_id , created_at_list

    def get_CT_id_based_on_author_id(self,author_id):
        body = obj.query_clinical_trials_using_kol_id(author_id)
        response = global_client.search(index="index_name", body = body, request_timeout=10)
        list2_trail_id=[]
        trail_created_at_list=[]
        for doc in response['hits']['hits']:
            list_trial_id = doc.get('_source',{}).get('clinical_id','')
            list2_trail_id.append(list_trial_id)
            created_at = doc.get('_source',{}).get('created_at')
            trail_created_at_list.append(created_at)

        return list2_trail_id , trail_created_at_list

    def CT_name_compare_name(self , name , elastic_name):
        if name in elastic_name :
            return True

    def compare_research_activity_clinical_trails(self,data ,TA) :
        TA_synonyms = config.TA_synonyms.get(TA.lower())
        for synonym in TA_synonyms:
            if synonym in data.lower():
                return True
        return False


    """Writing the ouptut in the csv file"""
    def write_csv(self,name,author_id,affiliation,country,Status,updated_pmid,latest_time_stamp,latest_time_stamp_NN):
        file_exists=os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            feild_names= ['Name','Author_id','affiliation','country','PMIDs or Trail_id not tagged by NN','Latest timestamp by Automation','Latest timestamp by NN','Status']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Name':name,'Author_id':author_id, 'affiliation':affiliation,\
                                'country':country,\
                                 'PMIDs or Trail_id not tagged by NN':updated_pmid,\
                                   'Latest timestamp by Automation':latest_time_stamp,'Latest timestamp by NN':latest_time_stamp_NN,'Status':Status})


    def read_csv(self,input_file,environment):
        start_time= datetime.now()
        count = 0
        """Reading the data from the input file{author_name}{affiliation}{author_id}{country}{TA}"""
        with open(input_file,'r')as csvfile:
            reader = csv.DictReader(csvfile)

            for line in reader  :
                count += 1
                # if count <= 889 :
                #     continue
                name = line.get('Name')
                country = line.get('country in Author corpus')
                affiliation = line.get('affiliation in Author corpus')
                input_author_id = line.get('author_id')
                TA = line.get('TA')
                combined_affliation=affiliation+", "+country
                combined_affliation=combined_affliation.strip()
                combined_affliation=combined_affliation.strip(',')
                affiliations, countries = obj.affliation_generator(combined_affliation)
                pmid_list = []
                timestamp_latest = []
                timestamp_nn_latest = []
                status_list = []
                print(name)
                if not affiliations:
                    affiliations.append(affiliation)

                countries.append(country)
                name_dict=obj.getAllPossible(name)
                author_names=name_dict[name]
                """Getting the affiliations using author_id in the elastic query """
                affiliation_using_author_id=self.get_elastic_affiliation(name,input_author_id)

                #author_names = list(set([obj.get_searchable_name(x) for x in author_names]))
                for author_name in author_names :
                    searchable_name = name #uuuu
                    #print(searchable_name)
                    #import ipdb; ipdb.set_trace()
                    if  environment == 'Publications':
                        difference_str,latest_time_stamp,latest_time_stamp_NN, status = self.get_publications(searchable_name,input_author_id,author_name,affiliations,countries,TA,affiliation_using_author_id)
                        if difference_str :
                            pmid_list.extend(difference_str.split(","))
                            timestamp_latest.append(latest_time_stamp)
                            if latest_time_stamp_NN :
                                timestamp_nn_latest.append(latest_time_stamp_NN)
                            status_list.append(status)

                    # self.write_csv(name ,input_author_id ,affiliation, country,status, diff, time_stamp,time_stamp_NN)

                    elif  environment == 'Clinical':
                        difference_str,latest_time_stamp,latest_time_stamp_NN, status = self.get_clinical_trials(searchable_name,input_author_id,author_name, affiliations , countries, TA, affiliation_using_author_id)
                        if difference_str :
                            pmid_list.extend(difference_str.split(","))
                            timestamp_latest.append(latest_time_stamp)
                            if latest_time_stamp_NN :
                                timestamp_nn_latest.append(latest_time_stamp_NN)
                            status_list.append(status)

                    # print "here"
                if pmid_list :
                    pmid_list = list(set(pmid_list))
                    pmid_list = ",".join(pmid_list)
                    timestamp_latest = max(map(int,timestamp_latest))
                    if timestamp_nn_latest :
                        timestamp_nn_latest = max(map(int,timestamp_nn_latest))
                    status_list = list(set(status_list))
                    status_list = ",".join(status_list)
                else :
                    pmid_list = None
                    timestamp_latest = None
                    timestamp_nn_latest = None
                    status ='updated'
                    status_list.append(status)
                    status_list = ",".join(status_list)
                self.write_csv(name ,input_author_id ,affiliation, country,status_list, pmid_list, timestamp_latest,timestamp_nn_latest)
                print(count)

    def get_publications(self, searchable_name, input_author_id, name, affiliations,countries,TA, affiliation_using_author_id):

        body=obj.query_publication_using_kol_name(searchable_name,input_author_id)
        response = global_client.search(index="index_name", body = body, request_timeout=10)
        publication_id_list1=[]
        created_at_list1=[]
        latest_time_stamp=''
        for doc in response['hits']['hits']:
            for authors in doc.get('_source',{}).get('authors',[]):
                if authors.get('author_name')==name:
                    elastic_name = authors.get('author_name',"")
                    elastic_affiliations = authors.get('author_affiliation',[])
                    elastic_author_id = authors.get('author_id',"")
                    """Getting the filtered output in result"""
                    result = self.get_filter_pmid(elastic_name, name, \
                                elastic_affiliations,affiliations,elastic_author_id,input_author_id ,countries, TA,\
                                   affiliation_using_author_id)
                    if result :
                        """Collecting the publication_id if the  """
                        created_at = doc.get('_source',{}).get('created_at',"")
                        created_at_list1.append(created_at)
                        latest_time_stamp = max(created_at_list1)
                        publication_id_list = doc.get('_source',{}).get('publication_id',"")
                        publication_id_list1.append(publication_id_list)
        publication_id_list2 , created_at_list = self.get_publication_id_based_on_author_id(input_author_id)
        difference_str, latest_time_stamp, latest_time_stamp_NN, status = self.get_filtered_output(publication_id_list1,publication_id_list2,\
                                                                                                          latest_time_stamp,created_at_list)

        return difference_str , latest_time_stamp , latest_time_stamp_NN, status



    def get_clinical_trials(self, searchable_name, input_author_id,name , affiliations , countries,TA , affiliation_using_author_id):
        body_clinical_trails=obj.query_clinical_trials_using_kol_name(searchable_name , input_author_id)
        response_CT = global_client.search(index="index_name", body = body_clinical_trails, request_timeout=10)
        CT_created_at_list = []
        trails_ids_list1 = []
        latest_time_stamp_CT = ''
        elastic_affiliation = ''
        for doc in response_CT['hits']['hits']:
                public_title = doc.get('_source',{}).get('public_title','')
                abstract =  doc.get('_source',{}).get('abstract','')
                detailed_descreption = doc.get('_source',{}).get('detailed_description','')
                official_title = doc.get('_source' ,  {}).get('official_title','')

                research_data = public_title + abstract + detailed_descreption + official_title

                for authors in doc.get('_source',{}).get('authors',[]):
                    if authors.get('author_name') :
                        if name.lower() in authors.get('author_name').lower() :
                            #import ipdb; ipdb.set_trace()
                            elastic_name = authors.get('author_name',"")
                            if authors.get('affiliation','') :
                                elastic_affiliation = authors.get('affiliation','')

                            combined_affliation = affiliation_using_author_id.append(elastic_affiliation)
                            elastic_author_id = authors.get('author_id',"")
                            CT_result = self.get_filter_trail_id(elastic_name,name, affiliations ,combined_affliation,\
                                                                      countries,elastic_author_id , \
                                                                          TA,research_data)
                            if CT_result :
                                CT_created_at = doc.get('_source',{}).get('created_at',"")
                                CT_created_at_list.append(CT_created_at)
                                latest_time_stamp_CT = max(CT_created_at_list)
                                trails_ids = doc.get('_source',{}).get('clinical_id',"")
                                trails_ids_list1.append(trails_ids)

        trails_ids_list2 , created_at_list = self.get_CT_id_based_on_author_id(input_author_id)
        difference_str, latest_time_stamp ,latest_time_stamp_NN ,status= self.get_filtered_output(trails_ids_list1,trails_ids_list2,\
                                        latest_time_stamp_CT,created_at_list)

        return difference_str , latest_time_stamp , latest_time_stamp_NN ,status


    """Getiing the difference between the two sets"""
    def get_filtered_output(self,list1 ,list2 ,latest_time_stamp ,created_at_list) :

                if set(list1)-set(list2):
                    """Changing the status as updated in case of difference of list1 and list2"""
                    status='not updated'
                    difference_pmid = set(list1)-set(list2)
                    difference=list(difference_pmid)
                    difference_str=",".join(difference)
                    latest_time_stamp_NN = None
                    if created_at_list:
                        latest_time_stamp_NN = max(created_at_list)

                else :
                    status='updated'
                    difference_str=None
                    latest_time_stamp_NN = None
                end_time=  datetime.now()

                return difference_str,latest_time_stamp, latest_time_stamp_NN , status


if __name__=='__main__':
    obj=data_recency()
    try:
        obj.read_csv('sunday_kol_info.csv',sys.argv[1])
    except:
        print('Damn You')
    #es_client = obj.create_es_connection()
