import requests
from account import User_details, User_details
import json
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
import time
import re
import pprint
import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")


Application's_url

#AUTH_TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mjc1ODMyMTIsInNlc3Npb25fa2V5IjoiZTQwNDhhYjMtMWMzOC00ZTYzLWE4M2YtZTE5Y2FjYzFlMzc5IiwiZXhwIjoxNTI3NjY5NjEyfQ.AzJx7t3tipVjSmaDrt5N2AX83Cu0-aDjtlLud3qbUiU"
# filename="filter.csv"
# list_filename=filename.split('.')
# now = time.strftime('%d-%m-%Y %H:%M:%S')
# out_filename=list_filename[0]+'.'+list_filename[1]

def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    print(r.status_code)

    # pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes

def auth_callback_api(account):

    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get('application_callback_url', params=payload)
    print(req.status_code)

    accesstoken_dict = req.json()
    # print(accesstoken_dict)
    #print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken

# to make Auth token generic
def get_auth_token(account):
    global AUTH_TOKEN
    # AUTH_TOKEN = auth_callback_api(account)
    AUTH_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzc1OTY0NTAsInNlc3Npb25fa2V5IjoiNmQ3ZTk3NDAtMTNhMS00MzlmLTk1Y2YtZTIxZmE2YmNjNjM1IiwiZXhwIjoxNTM3NjgyODUwfQ.Lfex-VzZk_QoTQUJhwwBUDUXXdgyxxXETjcX61AQk3c'
    return AUTH_TOKEN

def read_csv(input_file):
    get_auth_token(User_details)
    input_file = "author_id.csv"
    with open(input_file,'r')as csvfile:
        reader = csv.DictReader(csvfile)
        count=0

        for line in reader  :
            author_id=line.get('Author_id')
            count=count + 1
            print(count)
            research_activity(author_id)

asset_class_keys = {'Publications':'article_title','Clinical Trials':'public_title','Congresses':'title','Societies':'body_name','Guidelines':'title','Advocacy':'body_name','HTA':'body_name','Regulatory bodies':'body_name','Top Hospital KOLs':'body_name','Patents':'title','Press Releases':'title','Diagnostic Centers':'body_name'}

output_filename= open(os.getcwd()+"/demo.csv",'w')
field_names= ['Author id','Name','Count','Publications','Clinical Trials','Congresses','Societies','Guidelines','Advocacy','Patents','Regulatory bodies','HTA','Diagnostic Centers','Top Hospital KOLs','Press Releases']
writer = csv.DictWriter(output_filename, fieldnames=field_names)
writer.writeheader()

def research_activity(author_id):
    print(author_id)
    AUTH_TOKEN=get_auth_token(User_details)

    Header = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    params = {
        "dataType": "Public",
        "from": 0,
        "limit": 10
    }

    url = BASE_API + 'kols/profile/' + author_id + '/research_activity'
    res = requests.get(url, params, headers=Header)
    print("status : ", res.status_code)
    inno_data = res.json().get('data',[])

    for dict in inno_data:
        
        count=dict.get('count')
        name=str(dict.get('name'))
        print(name)
        docs=dict.get('docs')
        vars()[name.replace(' ','_')] = []
        authors_research_activity = {'Author id':author_id,'Name':name,'Count':count}

        for doc in docs:
            vars()[name.replace(' ','_')].append(doc.get(asset_class_keys[name],''))
        authors_research_activity[name] = vars()[name.replace(' ','_')]
        
        writer.writerow(authors_research_activity)

if __name__ == '__main__':
    read_csv("author_id.csv")