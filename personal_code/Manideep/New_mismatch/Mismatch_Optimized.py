from accounts import User_details, User_details
import requests
from body import TEST_CASES
from body_pro import TEST_CASES_pro
import json
import csv
import os.path
import time
import pprint
from elasticsearch import Elasticsearch
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
out_filename ="Top200_kol_result.csv"
out_filename1 = "Meta_info.csv"
global_client = Elastic_Search_usernam_password
global_production = Elastic_Search_usernam_password
#global_production = Elasticsearch(['elastic_Search_username_password'])
#import sys

list_staging=[]
list_pro=[]
Application's_url
pags = ''

def auth_login(account):
  r = requests.post('Url_of_application', json=account)
  dict = r.json()
  code = dict['responseData']['callback_url']
  codes = code[34:]
  return codes

def auth_callback_api(account):
  global accesstoken
  payload = {
      "code": auth_login(account),
      "app":"name_of_app"
  }
  req = requests.get(
      'application_callback_url', params=payload)
  accesstoken_dict = req.json()
  accesstoken = accesstoken_dict['accessToken']
  print("Granted access for staging")
  return accesstoken

# to make Auth token generic
def get_auth_token(account):
  global AUTH_TOKEN
  AUTH_TOKEN = auth_callback_api(account)

def auth_login_production(account):
  r = requests.post('url', json=account)
  dict = r.json()
  code = dict['responseData']['callback_url']
  codes = code[26:]
  return codes

def auth_callback_api_production(account):
  global accesstoken
  payload = {
      "code": auth_login_production(account),
      "app":"name_of_app"
  }
  req = requests.get(url, params=payload)
  accesstoken_dict = req.json()
  accesstoken = accesstoken_dict['accessToken']
  print("Granted access for Production:")
  return accesstoken

# to make Auth token generic
def get_auth_token_production(account):
  global AUTH_TOKEN_producation
  AUTH_TOKEN_producation = auth_callback_api_production(account)

def staging_data(body):
  get_auth_token(User_details)
  Header = {
      'Authorization': 'Bearer ' + AUTH_TOKEN,
      'content-type': 'application/json'
  }
  url = 'url'
  req = requests.post(url,data=json.dumps(body), headers=Header)

  dict_staging = {}
  inno_data = req.json().get('data',[])
  for data in inno_data :
      score_data_staging = data.get('scores',[])
      for score_pro in score_data_staging:
          author_id_staging = score_pro.get('author_id','')
          list_staging.append(author_id_staging)
          score_profile = score_pro.get('score_profile','')

          dict_staging[author_id_staging] = {}
          for asset_cls in score_profile:
              asset_class_staging=asset_cls.get('asset_class','')
              vars()[asset_class_staging] = asset_cls.get('doc_count','')
              dict_staging[author_id_staging][asset_class_staging] = vars()[asset_class_staging]

  return list_staging,dict_staging

def producation_data(body_pro):
  get_auth_token_production(User_details)
  Header = {
      'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
      'content-type': 'application/json'
  }
  #url='http://35.196.135.15/api/v2/kols/score/'
  url='url'
  req = requests.post(url,data=json.dumps(body_pro), headers=Header)

  dict_production = {}
  inno_data = req.json().get('data',[])
  for data in inno_data :
      score_data_producation = data.get('scores',[])
      for score_pro in score_data_producation:
          author_id_producation = score_pro.get('author_id','')
          list_pro.append(author_id_producation)
          score_profile = score_pro.get('score_profile','')

          dict_production[author_id_producation] = {}
          for asset_cls in score_profile:
              asset_class_producation=asset_cls.get('asset_class','')
              vars()[asset_class_producation] = asset_cls.get('doc_count','')
              dict_production[author_id_producation][asset_class_producation] = vars()[asset_class_producation]

  return list_pro,dict_production

# fetch respective data related to asset classe following is the format of return values
# return elastic_search_query, staging_index_name, production_index_name, key_to_extract_data_from_es_response
def asseet_class_es_query(asset_class,ids,indication_list,TA=''):
  if asset_class == 'publications':
      query = publication_es_query(ids , indication_list)
      return query,'index_name','index_name','publication_id'
  elif asset_class == 'clinicaltrials':
      query = clinical_trails_es_query(ids , indication_list)
      return query,'index_name','index_name','clinical_id'
  elif asset_class == 'congresses':
      query = congress_es_query(ids , indication_list , TA)
      return query,'index_name','index_name','congress_id'

def es_query_ids(asset_class,ids):
  if asset_class == 'publications':
      query = es_query_pmids(ids)
  elif asset_class == 'clinicaltrials':
      query = es_query_using_trail_id(ids)
  elif asset_class == 'congresses':
      query = es_query_congress_id(ids)
  return query

def compare_score(DOC_Count_staging,DOC_Count_producation,author_id_producation,author_id_staging,TA,indication_list):
  author_info = {}
  asset_classes_stagging = DOC_Count_staging[author_id_staging].keys()
  asset_classes_production = DOC_Count_producation[author_id_producation].keys()
  asset_class_existing = list(set.union(set(asset_classes_production),set(asset_classes_stagging)))
  # print asset_class_existing

  for i in asset_class_existing:
    vars()[i+"_normalisation_flag"] = set()
    vars()[i+"_porting_flag"] = set()

  if DOC_Count_staging.get(author_id_staging) :
    total_count_staging = 0
    staging_name = author_name_staging(author_id_staging)
    author_info.update({'Author name Staging':staging_name,'Author ID Staging':author_id_staging})
    for asset_class in asset_class_existing:
        vars()[asset_class+'_staging'] = DOC_Count_staging[author_id_staging].get(asset_class,0)
        author_info['Staging Score '+asset_class] = vars()[asset_class+'_staging']
        total_count_staging += vars()[asset_class+'_staging']

  if DOC_Count_producation.get(author_id_producation):
    total_count_producation = 0
    producation_name = author_name_producation(author_id_producation)
    author_info.update({'Author name Producation':producation_name,'Author ID Producation':author_id_producation})
    for asset_class in asset_class_existing:
        vars()[asset_class+'_producation'] = DOC_Count_producation[author_id_staging].get(asset_class,0)
        total_count_producation += vars()[asset_class+'_producation']
        author_info['Producation Score '+asset_class] = vars()[asset_class+'_producation']

  asset_class_diff = ''
  flag_asset_class = list(set.intersection(set(asset_class_existing),set(['publications','clinicaltrials','congresses'])))
  print producation_name
  for asset_class in flag_asset_class:
    if vars()[asset_class+"_staging"] == vars()[asset_class+"_producation"]:
      vars()[asset_class+'_diff_flag'] = ''
    else:
      vars()[asset_class+'_diff_flag'] = asset_class
      ids = ''.join(author_id_staging)

      body = asseet_class_es_query(asset_class,ids,indication_list,TA)

      response_staging = global_client.search(index=body[1], body = body[0])
      response_production = global_production.search(index=body[2], body = body[0])
      diff_ids = compare_ids(body[3],response_staging , response_production)
      for ids in diff_ids :
        body_es = es_query_ids(asset_class,ids)
        response = global_production.search(index=body[2], body = body_es)
        if response["hits"]["hits"]:
          for docs in response["hits"]["hits"]:
            authors = docs.get('_source',{}).get('authors',[])
            for author in authors :
              if asset_class == 'publications':
                if not author.get("author_id","") and author.get("ForeName",'') != 'not found':
                  vars()[asset_class+'_normalisation_flag'].add(ids)
              else:
                if not author.get("author_id",""):
                  vars()[asset_class+'_normalisation_flag'].add(ids)
        else :
          vars()[asset_class+'_porting_flag'].add(ids)

    vars()[asset_class+'_normalisation_flag'] = list(vars()[asset_class+'_normalisation_flag'])
    vars()[asset_class+'_porting_flag'] = list(vars()[asset_class+'_porting_flag'])

    if not vars()[asset_class+'_normalisation_flag']:
      vars()[asset_class+"_normalisation_flag"] = None
    if not vars()[asset_class+'_porting_flag']:
      vars()[asset_class+'_porting_flag'] = None

    asset_class_diff += vars()[asset_class+'_diff_flag']
    author_info.update({asset_class+' not normalised':vars()[asset_class+'_normalisation_flag'],asset_class+' not ported':vars()[asset_class+'_porting_flag']})

  author_info['Asset Classes having diff'] = asset_class_diff

  staging_research_acitivity = research_activity_staging(author_id_staging)
  stagging_asset_classes = []
  for i in staging_research_acitivity:
    stagging_asset_classes.append(i['name'])
    vars()['Staging_Total_'+i['name'].replace(' ','_')] = i['count']
    author_info['Staging Total '+i['name']] = i['count']

  production_research_activity = research_activity_production(author_id_producation)
  production_asset_classes = []
  for i in production_research_activity:
    production_asset_classes.append(i['name'])
    vars()['Production_Total_'+i['name'].replace(' ','_')] = i['count']
    author_info['Production Total '+i['name']] = i['count']

  common_research_asset_classes = list(set.union(set(stagging_asset_classes),set(production_asset_classes)))
  asset_class_not_available = []
  for i in common_research_asset_classes:
    try:
      if vars()['Staging_Total_'+i.replace(' ','_')] == vars()['Production_Total_'+i.replace(' ','_')]:
        score_diff_flag = 'Matching'
      else:
        score_diff_flag = 'Not Matching'
      author_info[i+' Total'] = score_diff_flag
    except KeyError:
      asset_class_not_available.append(i)
      # print asset_class_not_available

  author_info['Asset Class Difference'] = " | ".join(asset_class_not_available)
  write_meta_csv(author_info)

def compare_ids(key,response_staging , response_production):
  ids_staging = []
  ids_production = []

  for docs in response_staging["hits"]["hits"]:
      ids = docs.get('_source',{}).get(key,'')
      ids_staging.append(ids)

  for docs in response_production["hits"]["hits"]:
      ids = docs.get('_source',{}).get(key,'')
      ids_production.append(ids)

  difference_ids = set(ids_staging) - set(ids_production)
  return difference_ids

def author_name_staging(list_staging):

  #difference=list(list_staging)
  #difference_str=",".join(difference)
  author_nm=''
  Header = {
    'Authorization': 'Bearer ' + AUTH_TOKEN,
    'content-type': 'application/json'
  }
 # for author_id in list_staging:
  params = {
    'author_ids': list_staging
  }
  url = 'url/api/v2/kols/list/'
  req = requests.get(url,params,headers=Header)
  inno_data = req.json().get('data',[])
  for data in inno_data :
      author_nm = data.get('nm','')
  return author_nm

def author_name_producation(list_pro):
  author_nm=''
  Header = {
    'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
    'content-type': 'application/json'
  }
  #for author_id in list_pro:
  params = {
        'author_ids': list_pro
  }
  #url = 'http://35.196.135.15/api/v2/kols/score/'
  url = 'url/api/v2/kols/list/'
  req = requests.get(url,params,headers=Header)
  inno_data = req.json().get('data',[])
  for data in inno_data :
    author_nm = data.get('nm','')
  return author_nm

def get_publication_base_query(indication):
  base_query = [
                {
                  "match_phrase":{
                    "article_title":indication
                  }
                },
                {
                  "match_phrase":{
                    "keywords":indication
                  }
                },
                {
                  "match_phrase":{
                    "abstract":indication
                  }
                }]
  return base_query

def es_query_using_trail_id(trail_id) :
  query = {
            "sort": {
              "created_at": "desc"
            },
            "query" :{
              "match":{
                "clinical_id": trail_id
              }
            }
          }

  return trail_id

def get_publication_query(indication_list):
  query = {
      "bool": {
          "should": []
      }
  }

  for indication in indication_list :
      query["bool"]["should"].extend(get_publication_base_query(indication))

  return query

def publication_es_query(author_id,indication) :
  query = {
            "query":{
              "bool":{
                "must":[
                  # {
                  #   "bool":{
                  #     "should":[
                  #       {
                  #         "match_phrase":{
                  #           "article_title":indication
                  #         }
                  #       },
                  #       {
                  #         "match_phrase":{
                  #           "keywords":indication
                  #         }
                  #       },
                  #       {
                  #         "match_phrase":{
                  #           "abstract":indication
                  #         }
                  #       }]
                  #   }
                  # },
                  {
                    "bool":{
                      "should":[
                        {
                          "match":{
                            "authors.author_id":author_id
                          }
                        }]
                    }
                  }]
              }
            },
            "size":10000
          }
  query["query"]["bool"]["must"].append(get_publication_query(indication))
  return query

def es_query_pmids(pmid):

  query = {
            "sort":[
              {
                "created_at":{
                  "order":"desc"
                }
              }],
            "query": {
              "bool": {
                "must": [
                  {
                    "match": {
                      "publication_id": pmid
                    }
                  }
                ]
              }
            },
            "size": 1000
          }
  return query

def es_query_congress_id(congress_id) :
  body = {
            "sort": {
              "created_at": "desc"
            },
            "query" :{
              "match_phrase":{
                "congress_id": congress_id
              }
            }
          }

  return body

def base_CT_es_query(indication):

  base_query = [
              {
                "match_phrase":{
                  "detailed_description":indication
                }
              },
              {
                "match_phrase":{
                  "intervention.intervention_name":indication
                }
              },
              {
                "match_phrase":{
                  "intervention.description":indication
                }
              },
              {
                "match_phrase":{
                  "intervention.arm_group_label":indication
                }
              },
              {
                "match_phrase":{
                  "study_design.type":indication
                }
              },
              {
                "match_phrase":{
                  "study_design.value":indication
                }
              },
              {
                "match_phrase":{
                  "public_title":indication
                }
              },
              {
                "match_phrase":{
                  "scientific_title":indication
                }
              },
              {
                "match_phrase":{
                  "condition":indication
                }
              },
              {
                "match_phrase":{
                  "keyword":indication
                }
              },
              {
                "match_phrase":{
                  "official_title":indication
                }
              },
              {
                "match_phrase":{
                  "abstract":indication
                }
              }
              ]
  return base_query

def get_ct_query(indication_list):

  query = {
        "bool" :{

        "should":[]
                }

          }

  for indication in indication_list :
      query["bool"]["should"].extend(base_CT_es_query(indication))

  return query


def clinical_trails_es_query(author_id , indication) :

  query = {
            "query":{
              "bool":{
                "must":[
                  {
                    "bool":{
                      "should":[
                        {
                        "nested":{
                              "path":"authors",
                              "query":{
                                "match":{
                                  "authors.author_id":author_id
                                }
                              }
                            }
                        }]
                    }
                  }]
              }
            },
            "size":10000,
            "_source":"clinical_id"
          }

  query["query"]["bool"]["must"].append(get_ct_query(indication))
  return query

def get_congresses_base_query(indication, TA):
  base_query = [
    {
      "match_phrase":{
        "classification.indications":indication
      }
    },
    {
      "match_phrase":{
        "title":indication
      }
    },
    {
      "match_phrase":{
        "abstract":indication
      }
    },
    {
      "match_phrase":{
        "classification.TA":TA
      }
    }
  ]
  return base_query

def get_congresses_query(indications_list, TA):
  query = {
      "bool": {
          "should": []
      }
  }
  for i in indications_list:
      query["bool"]["should"].extend(get_congresses_base_query(i, TA))
  return query

# MAPPING = {
#     "clinicaltrials": get_congresses_query
# }

# for asset_class in asset_classes:
#     query = MAPPING["asset_class"](indications_list,TA)
def congress_es_query(author_id , indication , TA) :

  query = {
            "query":{
              "bool":{
                "must":[
                  {
                    "bool":{
                      "should":[
                        {
                        "nested":{
                          "path":"authors",
                          "query":{
                            "match":{
                              "authors.author_id":author_id
                                  }
                              }
                            }
                        }]
                    }
                  }]
              }
            },
            "size":10000,
            "_source":"congress_id"
          }
  query["query"]["bool"]["must"].append(get_congresses_query(indication, TA))
  return query

def others_es_query(indication , TA , author_id) :
  query = {
            "query":{
              "bool":{
                "must":[
                  {
                    "bool":{
                      "should":[
                        {
                          "match_phrase":{
                            "classification.TA":TA
                          }
                        }]
                    }
                  },
                  {
                    "bool":{
                      "should":[
                        {
                          "match_phrase":{
                            "classification.indications":indication
                          }
                        }]
                    }
                  },
                  {
                    "bool":{
                      "should":[
                        {
                          "query":{
                            "nested":{
                              "path":"authors",
                              "query":{
                                "match":{
                                  "authors.author_id":author_id
                                }
                              }
                            }
                          }
                        }]
                    }
                  }]
              }
            },
            "size":10000,
            "_source":"application_id"
          }
  return query

def perform_check(list_pro,list_staging):

  status_flag = 'Matching'
  x = set(list_pro)-set(list_staging)
  y = set(list_staging)-set(list_pro)
  #len(x)
  # print("length",len(y))
  # print(y)
  # print("Diff in staging and producation",y)


  for list1 in list_staging:
    for list2 in list_pro:
      if list1 == list2:
        val_1=list_staging.index(list1)
        val_2=list_pro.index(list2)

        if val_1 == val_2 :
            status_flag = 'Matching'
        else :
            status_flag = 'Not Matching'

        staging_name = author_name_staging(list1)

        producation_name = author_name_producation(list2)

        write_csv(staging_name , list1, val_1 ,val_2,status_flag)
        # print(val_1,author_id,"-----",val_2,author_id)

        # print(author_id,val_1,"-----",val_2,author_id)

  return val_1 , val_2 , status_flag

def write_meta_csv(author_info):
  file_exists = os.path.isfile(out_filename1)
  with open(out_filename1, 'a') as csvfile:
    feild_names = ['Author name Staging','Author ID Staging','Author name Producation','Author ID Producation','Asset Class Difference','Staging Score publications','Producation Score publications','publications not normalised','publications not ported','Staging Total Publications','Production Total Publications','Publications Total','Staging Score clinicaltrials','Producation Score clinicaltrials','clinicaltrials not normalised','clinicaltrials not ported','Staging Total Clinical Trials','Production Total Clinical Trials','Clinical Trials Total','Staging Score congresses','Producation Score congresses','congresses not normalised','congresses not ported','Staging Total Congresses','Production Total Congresses','Congresses Total','Staging Score societies','Producation Score societies','Staging Total Societies','Production Total Societies','Societies Total','Staging Score hospitals','Producation Score hospitals','Staging Total Top Hospital KOLs','Production Total Top Hospital KOLs','Top Hospital KOLs Total','Staging Score pags','Producation Score pags','Staging Total Advocacy', 'Production Total Advocacy', 'Advocacy Total','Staging Score guidelines','Producation Score guidelines','Staging Total Guidelines','Production Total Guidelines','Guidelines Total','Staging Score regulatorybodies','Producation Score regulatorybodies','Staging Total Regulatory bodies','Production Total Regulatory bodies','Regulatory bodies Total','Production Total Patents','Staging Total Patents','Patents Total','Staging Total Press Releases','Production Total Press Releases','Press Releases Total','Asset Classes having diff']
    writer = csv.DictWriter(csvfile, fieldnames=feild_names)
    if not file_exists :
      writer.writeheader()
    writer.writerow(author_info)

def write_csv(author_name_staging,author_id_staging,position_staging,position_production,matching_flag):
  file_exists = os.path.isfile(out_filename)
  with open(out_filename, 'a') as csvfile:
      feild_names= ['Author name Staging','Staging author id','Mapping staging','Mapping Production','Matching flag']
      writer = csv.DictWriter(csvfile, fieldnames=feild_names)
      if not file_exists:
          writer.writeheader()
      writer.writerow({'Author name Staging':author_name_staging,'Staging author id':author_id_staging, 'Mapping staging':position_staging,\
                          'Mapping Production':position_production,\
                          'Matching flag':matching_flag})

def research_activity_staging(author_id):
  Header = {
      'Authorization': 'Bearer ' + AUTH_TOKEN,
      'content-type': 'application/json'
  }
  params = {
      "dataType": "Public",
      "from": 0,
      "limit": 10
  }
  url =  'url' + author_id + '/research_activity'
  res = requests.get(url, params, headers=Header)
  inno_data = res.json().get('data',[])
  return inno_data

def research_activity_production(author_id):
  Header = {
      'Authorization': 'Bearer ' + AUTH_TOKEN_producation,
      'content-type': 'application/json'
  }
  params = {
      "dataType": "Public",
      "from": 0,
      "limit": 10
  }

  url =  'url/api/v2/kols/profile/' + author_id + '/research_activity'
  res = requests.get(url, params, headers=Header)
  inno_data = res.json().get('data',[])
  return inno_data

def run_test_cases():

  list_staging,dict_staging=staging_data(TEST_CASES["Oncology"])

  list_pro,dict_production=producation_data(TEST_CASES_pro["Oncology"])

  perform_check(list_pro,list_staging)

  indication_list =[]
  dict_val  = TEST_CASES["Oncology"]
  TA = dict_val.get('TA','')

  for indication in dict_val.get('filters',{}).get('Indications',{}).get('values',[]) :
      indication_list.append(indication)

  for author_id_stagging in list_staging:
    for author_id_production in list_pro:
      if author_id_stagging == author_id_production:
        compare_score(dict_staging,dict_production,author_id_production,author_id_stagging,TA,indication_list)

if __name__ == '__main__':
  print time.time()
  run_test_cases()
  print time.time()
