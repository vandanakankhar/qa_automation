import requests
import json
import pprint
import csv
import os.path
import pprint
from elasticsearch import Elasticsearch
global_client = Elasticsearch(['elastic_Search_username_password'])
out_filename ="top20_Neurology.csv"
out_filename_1 ="top20_Oncology.csv"
out_filename_2="top20_Cardiology.csv"
out_filename_3 ="top20_Dermatology.csv"
out_filename_4 ="top20_Reproductive_Health.csv"
out_filename_5 ="top20_Metabolic_Diseases.csv"
out_filename_6 ="top20_Endocrinology.csv"
from accounts import User_details
global count
count = 0
AUTH_TOKEN = ''
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def auth_login(account):
    r = requests.post('Url_of_application', json=account)
    #print(r.status_code)
    #pprint.pprint(r.json())
    dict = r.json()
    #print(dict)
    code = dict['responseData']['callback_url']
    #print(code)
    codes = code[34:]
    #print(codes)
    #print("Granted login codes :", codes)
    return codes

def es_query(author_id):
        body={
                "query": {
                   	"match": {
                       	"author_id": author_id
                       }
                   }
             }
        return body

def auth_callback_api(account):
    global accesstoken
    payload = {
        "code": auth_login(account),
        "app":"name_of_app"
    }
    req = requests.get(
        'application_callback_url', params=payload)
    #print(r.status_code)
    accesstoken_dict = req.json()
    #print(accesstoken_dict)
    # print(accesstoken_dict['accessToken'])
    accesstoken = accesstoken_dict['accessToken']
    #print(accesstoken)
    #print("Granted access for staging")
    return accesstoken
# to make Auth token generic
def get_auth_token(account):
    AUTH_TOKEN = auth_callback_api(account)
    print AUTH_TOKEN
    return AUTH_TOKEN

def api_headers():
    headers = {
        'Authorization': 'Bearer ' + AUTH_TOKEN,
        'content-type': 'application/json'
    }
    return headers

def get_payload(TA,as_class, list_indications, filters_data,country):
    scores = filters_data['data'][as_class]['scoring']['values']
    payload = {"from":0,"limit":10,"TA":TA,"asset_class":as_class,"filters":{"kol_info":scores,"showScore":True,"kolType":"allkols","Indications":{"values":list_indications,"isAnd":False,"name":"Indications"}}}
    if len(country) != 0:
        payload['filters']['Countries'] = {'values':country,'isAnd':False}
    return payload

def get_input(count, as_class, TA, list_indications,filters_data,country=[]):
    print(as_class, TA, list_indications, country)
    url = 'url'
    payload = get_payload(TA,as_class,list_indications,filters_data,country)
    r = requests.post(url, data=json.dumps(payload), headers=api_headers())
    r = r.json()
    emerging_author_id_list = get_emerging_kol(count, as_class, TA, list_indications, country)
    pprint.pprint(emerging_author_id_list)
    top_kol_list = []
    emerging_kol_name =''
    if r :
        for i in r['data'][0]['scores']:
            count+= 1
            print(count)
            #if count <= 15:
                #continue
            #author_id=i['author_id']
            print(i['author_id'],list_indications,country,as_class)
            top_kol_list.append(i['author_id'])
            #writer.writerow({'AUTHOR_ID' : i['author_id'], "TA" : TA, "INDICATION" : list_indications, "COUNTRY" : country, "RESOUCES" : as_class})
            if count == 100:
                break
    if len(top_kol_list) >= len(emerging_author_id_list) :
        for i in range(0,len(top_kol_list)) :
            #import ipdb; ipdb.set_trace()
            try :
                if top_kol_list[i] :
                    top_kol_body = es_query(top_kol_list[i])
                    response = global_client.search(index="authors_alias", body = top_kol_body, request_timeout=10)
                    for doc in response['hits']['hits']:
                        top_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try :
                if emerging_author_id_list[i] :
                    emerging_kol_body = es_query(emerging_author_id_list[i])
                    response_emerging = global_client.search(index="authors_alias", body = emerging_kol_body, request_timeout=10)
                    for doc in response_emerging['hits']['hits'] :
                        emerging_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try:
                if not emerging_kol_name :
                    emerging_kol_name = ''
                if len(emerging_author_id_list[i]) == 32:
                    write_csv( top_kol_list[i], top_kol_name, emerging_author_id_list[i], emerging_kol_name , TA, list_indications, country, as_class)
                else:
                    write_csv( top_kol_list[i], top_kol_name, 'Not Available For '+as_class, emerging_kol_name , TA, list_indications, country, as_class)
            except IndexError:
                pass
    elif len(emerging_author_id_list) > len(top_kol_list) :
        for i in range(0,len(emerging_author_id_list)) :
            try :
                if top_kol_list[i] :
                    top_kol_body = es_query(top_kol_list[i])
                    response = global_client.search(index="authors_alias", body = top_kol_body, request_timeout=10)
                    for doc in response['hits']['hits']:
                        top_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try :
                if emerging_author_id_list[i] :
                    emerging_kol_body = es_query(emerging_author_id_list[i])
                    response_emerging = global_client.search(index="authors_alias", body = emerging_kol_body, request_timeout=10)
                    for doc in response_emerging['hits']['hits'] :
                        emerging_kol_name = doc.get('_source',{}).get('name','').encode('utf-8')
            except IndexError:
                pass
            try:
                if not emerging_kol_name :
                    emerging_kol_name = ''
                if len(emerging_author_id_list[i]) == 32:
                    write_csv( top_kol_list[i], top_kol_name, emerging_author_id_list[i], emerging_kol_name , TA, list_indications, country, as_class)
                else:
                    write_csv( top_kol_list[i], top_kol_name, 'Not Available For '+as_class, emerging_kol_name , TA, list_indications, country, as_class)
            except IndexError:
                pass
def get_emerging_kol(count, as_class, TA, list_indications, country):
    #print(as_class, TA, list_indications, country)
    url = 'url'
    payload = {}
    payload = {"from": 0, "limit": 10, "TA": TA, "asset_class": as_class, "filters": {"Indications": {"values": list_indications, "isAnd": False}, "kol_info": [
        {"name": as_class, "value": 100, "showDate": False, "startDate": 0, "endDate": 0}], "showScore": False, "kolType": "emerging", "Countries": {"values": country, "isAnd": False}}}
    #print(json.dumps(payload))
    #import ipdb; ipdb.set_trace()
    r = requests.post(url, data=json.dumps(payload), headers=api_headers())
    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        # Whoops it wasn't a 200
        return "Error: " + str(e)
    r = r.json()
    emerging_author_id_list = []
    for i in r['data'][0]['scores']:
        count+= 1
        print(count)
        #if count <= 15:
            #continue
        #author_id=i['author_id']
        print("emerging :"+i['author_id'],list_indications,country,as_class)
        if i['author_id'] :
            emerging_author_id_list.append(i['author_id'])
        if count == 100:
            break
    return emerging_author_id_list
    
def write_csv(author_id,top_kol_name,author_id_emerging,emerging_kol_name,TA, list_indications, country, as_class):
    if TA == "Neurology":
        file_exists = os.path.isfile(out_filename)
        with open(out_filename, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Oncology":
        file_exists = os.path.isfile(out_filename_1)
        with open(out_filename_1, 'a') as csvfile:
            feild_names= ['Author_id','Top kol name','Author_id_Emerging','Emerging kol name','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Top kol name':top_kol_name,'Author_id_Emerging':author_id_emerging,'Emerging kol name':emerging_kol_name,\
                                                'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Cardiology":
        file_exists = os.path.isfile(out_filename_2)
        with open(out_filename_2, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Dermatology":
        file_exists = os.path.isfile(out_filename_3)
        with open(out_filename_3, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Reproductive Health":
        file_exists = os.path.isfile(out_filename_4)
        with open(out_filename_4, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Metabolic Diseases":
        file_exists = os.path.isfile(out_filename_5)
        with open(out_filename_5, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})
    elif TA == "Gastroenterology":
        file_exists = os.path.isfile(out_filename_6)
        with open(out_filename_6, 'a') as csvfile:
            feild_names= ['Author_id','Author_id_Emerging','TA','INDICATION','COUNTRY','RESOUCES']
            writer = csv.DictWriter(csvfile, fieldnames=feild_names)
            if not file_exists:
                writer.writeheader()
            writer.writerow({'Author_id':author_id,'Author_id_Emerging':author_id_emerging,'TA':TA,'INDICATION':list_indications,'COUNTRY':country,'RESOUCES':as_class})

def main_group_api():
    url = "url/api/v2/filters/main?group=org&view=allkols"
    payload = {}
    r = requests.get(url, data=json.dumps(payload), headers=api_headers())
    r = r.json()
    return r

def filters_api(ta_name,indication):
    url = "url/api/v2/filters?Indications="+indication+"&group=org&isAnd=false&tas="+ta_name+"&view=allkols"
    payload = {}
    r = requests.get(url, data=json.dumps(payload), headers=api_headers())
    r = r.json()
    return r

AUTH_TOKEN = get_auth_token(User_details)

if __name__ == '__main__':
    main_data = main_group_api()
    TA_value = sys.argv[1]
    if TA_value == "All":
        TA = main_data['data']['TA']['values']
    else :
        TA = [TA_value]

    INDICATION_DCT = {}
    for ta_value in TA:
        INDICATION_DCT[ta_value] = main_data['data'][ta_value]['Indications']['values']

    count = 0
    for ta_name in TA:
        for indication in INDICATION_DCT[ta_name]:
            filters_data = filters_api(ta_name,indication)
            Resources = filters_data['data']['Resources']['values']
            for asset_class in Resources:
                if sys.argv[2].lower() != 'without country':
                    # countries = filters_data['data'][asset_class]['filters'][0]['values']
                    countries = sys.argv[2].split(',')
                    for country in countries:
                        get_input(count, asset_class, ta_name, [indication], filters_data, [country.strip(' ')])
                elif sys.argv[2].lower() == 'without country':
                    get_input(count, asset_class, ta_name, [indication], filters_data)

# if __name__ == '__main__':
#     country(TA,INDICATION_DCT,LIST_ASSET_CLASS)
#
#
#
    # LIST_ASSET_CLASS = ["Top KOLs", "Publications"]
    # # list_country=["Costa Rica", "Chile", "Panama", "Ecuador", "Mexico", "Peru", "Argentina", "Brazil", "Columbia", ]
    # TA = ["Neurology"]
    # INDICATION_DCT = {"Neurology": ["Multiple Sclerosis","Alzheimer","Parkinson"]
    #                     ""}
    # COUNTRY_DCT = {"Neurology": ["Panama", "Germany"]}
    # author_ids = set()
    # for as_class in list_asset_class:
    #     list_authors_as_class=get_input(as_class,"Neurology",["Multiple Sclerosis"])
    #     for ids in list_authors_as_class:
    #         print author_ids
    #         author_ids.add(ids)
    # for ta_name in INDICATION_DCT:
    #     for resource in LIST_ASSET_CLASS:
    #         for indication in INDICATION_DCT[ta_name]:
    #             get_input(count, resource, ta_name, [indication])
    #             count = 0
    # for as_class in list_asset_class:
    #     for countries in list_country:
    #         list_authors_as_class = get_input(
    #             as_class, "Endocrinology", ["Obesity"], [countries])
    #         for ids in list_authors_as_class:
    #             author_ids.add(ids)
